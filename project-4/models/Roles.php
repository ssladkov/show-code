<?php

/**
 * This is the model class for table "Roles".
 *
 *
 */
class Roles extends CActiveRecord
{
    const ROLE_GUEST = 'guest';
    const ROLE_USER = 'user';
	const ROLE_ADMIN_ID = 1;

	private static $_items=array();
	public $privs_arr = array();
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Roles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('RoleName', 'required', 'message' => 'Необходимо заполнить поле'),
			array('RoleName', 'length', 'max'=>64, 'min' => 5, 'tooLong'=>'Не более 64 символов', 'tooShort'=>'Не менее 5 символов'),
			array('RoleName', 'match', 'pattern'=>'/^[a-zA-Z0-9._]+$/', 'message'=>'Разрешены только латинские символы, цифры и символ нижнего подчеркивания'),
			array('RoleName', 'unique'),
			array('RoleDesc', 'length', 'max'=>512, 'min' => 5, 'tooLong'=>'Не более 512 символов', 'tooShort'=>'Не менее 5 символов'),
		);
	}
    public function beforeSave() {
        $this->RoleName = strtoupper($this->RoleName);
		$this->UpdateDateTime = date('Y-m-d H:i:s');
		/*if ($this->isNewRecord) {
            $this->date_created = date('Y-m-d H:i:s');
            $this->sess_id_created = Yii::app()->user->id;
         }
		 else {
			$this->date_modified = date('Y-m-d H:i:s');
            $this->sess_id_modified = Yii::app()->user->id;			
		 }*/
        return parent::beforeSave();
    }
	public function afterSave() {
		if (!$this->isNewRecord) {
            $this->_deleteAllRoleLinks($this->RoleId);
         }
		if( is_array($this->privs_arr) ) $this->_linkPrivWRoles($this->privs_arr,$this->RoleId);
		unlink(Yii::getPathOfAlias('application.config.auth').'.php' );
        return parent::afterSave();
    }
	
	//удалим все связи ролей с этой привилегией
	private function _deleteAllRoleLinks($role_id) {
		Yii::app()->db->createCommand("delete from RolePrivileges where RoleId=?")->bindValue(1, $role_id)->execute();
	}
	//привяжем привилегию к отмеченым ролям
	private function _linkPrivWRoles($privs_id_arr,$role_id) {
		//$transaction = Yii::app()->db->beginTransaction();
		//try {
			foreach( $privs_id_arr as $priv_id ) 
				Yii::app()->db->createCommand("insert into RolePrivileges(RoleId,PrivilegeId,RolePrivilegeEnable,UpdateUserId,UpdateDateTime) values(:role_id,:priv_id,1,:us_id,:date)")->bindValues(array("role_id"=>$role_id,"priv_id"=>$priv_id,"us_id"=>Yii::app()->user->id,"date"=>date('Y-m-d H:i:s')))->execute();
		//	}
		//	$transaction->commit();
		//}
		//catch(Exception $e){
		//  $transaction->rollback();
		//}
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		//return array();
		return array(
			'privs' => array(self::MANY_MANY, 'Privileges', 'RolePrivileges(RoleId,PrivilegeId)'),
		);
	}
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('RoleId',$this->RoleId);
		$criteria->compare('RoleName',$this->RoleName);
		$criteria->compare('RoleDesc',$this->RoleDesc,true);

        $sort = new CSort();
			$sort->attributes = array(
				'RoleId'=>array(
					'asc'=>'RoleId',
					'desc'=>'RoleId DESC',    
				),
				'RoleName'=>array(
					'asc'=>'RoleName',
					'desc'=>'RoleName DESC',
				),
				'*',
			);

		return new ActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			 'pagination' => array(
                'pageSize' => 20
            )
		));
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'RoleId' => 'ID',
			'RoleName' => 'Название роли',
			'RoleDesc' => 'Описание роли',
		);
	}
	public static function items()
	{
		if(count(self::$_items)==0)
			self::_loadItems();
		return self::$_items;
	}
	/**подгрузить все роли
	 * @return array customized attribute labels (name=>label)
	 */	
	private static function _loadItems()
	{
		self::$_items=array();
		$models=self::model()->findAll(array(
			'order'=>'RoleId',
		));
		foreach($models as $model)
			self::$_items[$model->RoleId]=$model->RoleName;
	}
}