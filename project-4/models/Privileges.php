<?php

/**
 * This is the model class for table "privileges".
 *
 *
 */
class Privileges extends CActiveRecord
{
	public $roles_arr = array();
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Privileges';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('PrivilegeName', 'required', 'message' => 'Необходимо заполнить поле'),
			array('PrivilegeName', 'length', 'max'=>64, 'min' => 5, 'tooLong'=>'Не более 64 символов', 'tooShort'=>'Не менее 5 символов'),
			array('PrivilegeName', 'match', 'pattern'=>'/^[a-zA-Z0-9._]+$/', 'message'=>'Разрешены только латинские символы, цифры и символ нижнего подчеркивания'),
			array('PrivilegeName', 'unique'),
			array('PrivilegeDesc', 'length', 'max'=>512, 'min' => 5, 'tooLong'=>'Не более 512 символов', 'tooShort'=>'Не менее 5 символов'),
		);
	}
    public function beforeSave() {
        $this->PrivilegeName = strtoupper($this->PrivilegeName);
        $this->UpdateUserId = Yii::app()->user->id;
        $this->UpdateDateTime = date('Y-m-d H:i:s');
        return parent::beforeSave();
    }    
	public function afterSave() {
		if (!$this->isNewRecord) {
            $this->_deleteAllPrivLinks($this->PrivilegeId);
         }
		if( is_array($this->roles_arr) ) $this->_linkPrivWRoles($this->roles_arr,$this->PrivilegeId);
		unlink(Yii::getPathOfAlias('application.config.auth').'.php' );
        return parent::afterSave();
    }
	
	//удалим все связи ролей с этой привилегией
	private function _deleteAllPrivLinks($priv_id) {
		Yii::app()->db->createCommand("delete from RolePrivileges where PrivilegeId=?")->bindValue(1, $priv_id)->execute();
	}
	//привяжем привилегию к отмеченым ролям
	private function _linkPrivWRoles($roles_id_arr,$priv_id) {
		//$transaction = Yii::app()->db->beginTransaction();
		//try {
			foreach( $roles_id_arr as $role_id ) {
				Yii::app()->db->createCommand("insert into RolePrivileges(RoleId,PrivilegeId,RolePrivilegeEnable,UpdateUserId,UpdateDateTime) values(:role_id,:priv_id,1,:us_id,:date)")->bindValues(array("role_id"=>$role_id,"priv_id"=>$priv_id,"us_id"=>Yii::app()->user->id,"date"=>date('Y-m-d H:i:s')))->execute();
			}
			//$transaction->commit();
		//}
		/*catch(Exception $e){
		   $transaction->rollback();
		}*/
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'roles' => array(self::MANY_MANY, 'Roles', 'RolePrivileges(RoleId,PrivilegeId)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'PrivilegeId' => 'ID',
			'PrivilegeName' => 'Название привилегии',
			'PrivilegeDesc' => 'Описание привилегии',
			'roles_arr' => 'Доступна ролям',
		);
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('PrivilegeId',$this->PrivilegeId);
		$criteria->compare('PrivilegeName',$this->PrivilegeName);
		$criteria->compare('PrivilegeDesc',$this->PrivilegeDesc,true);

        $sort = new CSort();
			$sort->attributes = array(
				'PrivilegeId'=>array(
					'asc'=>'PrivilegeId',
					'desc'=>'PrivilegeId DESC',    
				),
				'PrivilegeName'=>array(
					'asc'=>'PrivilegeName',
					'desc'=>'PrivilegeName DESC',
				),				
				'PrivilegeDesc'=>array(
					'asc'=>'PrivilegeDesc',
					'desc'=>'PrivilegeDesc DESC',
				),
				'*',
			);

		return new ActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			 'pagination' => array(
                'pageSize' => 20
            )
		));
	}
}