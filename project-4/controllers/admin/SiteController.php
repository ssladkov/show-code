<?php

/**
 * SiteController методы для работы с админской частью приложения
 */
class SiteController extends AdminController
{
    public function beforeAction($action){
       if ( !Yii::app()->user->isGuest && $action->id != "logout" && $action->id != "login")  {
            $return_val;
            $command = Yii::app()->db->createCommand("EXEC :return_value = CheckSessionActivity @p_session_id = :p_session_id");
            $sess_id = Yii::app()->user->SessId;
            $command->prepare();
            $command->bindParam(':return_value', $return_val, PDO::PARAM_INT|PDO::PARAM_INPUT_OUTPUT, 1);
            $command->bindParam(':p_session_id', $sess_id, null, 50);
            $row = $command->queryRow();
            if( !$row[''] || $row[''] == 0 ) {
                Yii::app()->user->logout();
                Header( "Location: /admin.php" );
                return false;
            }
        }
        return true;
    }
	/**
	 * Index action is the default action in a controller.
	 */
	public function actionIndex()
	{
		$this->render('index');
	}	
	public function actionLogin()
	{
		$model = new LoginForm();

		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
		{
			echo CActiveForm::validate($model, array('username', 'password'));
			Yii::app()->end();
		}

		if (isset($_POST['LoginForm']))
		{
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate(array('username', 'password')) && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		$this->render('login', array(
			'model' => $model,
		));
	}
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	/*[USER] РАБОТАЕМ С ПОЛЬЗОВАТЕЛЯМИ */
	
    public function loadUserModel($id)
    {
        $model=User::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
	//Получение списка пользователей
	public function actionUsers() {
		$model = new User('Reg');
		$dataProvider = $model->search();
		$this->render('userslist',array(
			'userslist' => $dataProvider,
			'model' => $model
		));
	}
	//Работа с новым пользователем
	public function actionUserAdd() {
		$model = new User('Reg');
        if(isset($_POST['ajax']) && $_POST['ajax']==='userForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			if($model->save()) {
				Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Пользователь <strong>{$_POST['User']['UserLogin']}</strong> успешно создан");
				$this->redirect(Yii::app()->createAbsoluteUrl('admin/users'));
				Yii::app()->end();
			}
		}	
		$this->renderPartial('_form_include',array(
			'name' => '_userform',
			'model' => $model
		), false, true);
	}	
	//Редактирование пользователя
	public function actionUserEdit($id) {
		$model = $this->loadUserModel($id);
		$model->setScenario('Edit');
        if(isset($_POST['ajax']) && $_POST['ajax']==='userForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			if($model->save()) {
				$usname = $model->UserLogin;
				Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Пользователь <strong>$usname</strong> успешно отредактирован");
				$this->redirect(Yii::app()->createAbsoluteUrl('admin/users'));
				Yii::app()->end();
			}
		}	
		$this->renderPartial('_form_include',array(
			'name' => '_userform',
			'model' => $model,
			'id' => $id
		), false, true);
	}	
	//Удаление пользователя
	public function actionUserDelete($id) {
		$model = $this->loadUserModel($id);
		$usname = $model->UserLogin;
		$model->deleteByPk($id);
		echo json_encode(array("usname"=>$usname));
	}	
	//Установка нового пароля
	public function actionUserSetPass($id) {
		$model = $this->loadUserModel($id);
		$model->setScenario('Passchange');
        if(isset($_POST['ajax']) && $_POST['ajax']==='passForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			$usname = $model->UserLogin;
			if($model->save()) {
				Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Пароль для пользователя <strong>$usname</strong> успешно изменен");
				$this->redirect(Yii::app()->createAbsoluteUrl('admin/users'));
				Yii::app()->end();
			}
		}	
		$this->renderPartial('_form_include',array(
			'name' => '_userpassform',
			'model' => $model
		), false, true);
	}	
	
	/*[PRIVELEGES] РАБОТАЕМ С ПРИВИЛЕГИЯМИ */
    public function loadPrivModel($id)
    {
        $model=Privileges::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
	//Получение списка привилегий
	public function actionPrivileges() {
		$model = new Privileges();
		$dataProvider = $model->search();
		$this->render('privslist',array(
			'privslist' => $dataProvider,
			'model' => $model
		));
	}
	//Работа с новой привилегией
	public function actionPrivAdd() {
		$model = new Privileges();
        if(isset($_POST['ajax']) && $_POST['ajax']==='privForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Privileges'])) {
			$model->attributes = $_POST['Privileges'];
			$model->roles_arr = $_POST['Privileges']['roles_arr'];
			if($model->save()) {
				Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Привилегия <strong>".(strtoupper($_POST['Privileges']['PrivilegeName']))."</strong> успешно создана");
				$this->redirect(Yii::app()->createAbsoluteUrl('admin/privileges'));
			}
			Yii::app()->end();
		}	
		$this->renderPartial('_form_include',array(
			'name' => '_privform',
			'model' => $model
		), false, true);
	}
	//Редактирование привилегии
	public function actionPrivEdit($id) {
		$model = $this->loadPrivModel($id);

        if(isset($_POST['ajax']) && $_POST['ajax']==='privForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Privileges'])) {
			$model->attributes = $_POST['Privileges'];
			$model->roles_arr = $_POST['Privileges']['roles_arr'];
			if($model->save(true,array("PrivilegeDesc"))) {
				Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Привилегия <strong>".($model->PrivilegeName)."</strong> успешно отредактирована");
				$this->redirect(Yii::app()->createAbsoluteUrl('admin/privileges'));
				Yii::app()->end();
			}
		}	
		$this->renderPartial('_form_include',array(
			'name' => '_privform',
			'model' => $model
		), false, true);
	}
	//Удаление привилегии
	public function actionPrivDelete($id,$privname) {
		$model = $this->loadPrivModel($id);
		$model->deleteByPk($id);
		echo json_encode(array("privname"=>$privname));
	}		
	
	/*[ROLES] РАБОТАЕМ С РОЛЯМИ */
    public function loadRoleModel($id)
    {
        $model=Roles::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
	//Получение списка ролей
	public function actionRoles() {
		$model = new Roles();
		$dataProvider = $model->search();
		$this->render('roleslist',array(
			'roleslist' => $dataProvider,
			'model' => $model
		));
	}
	//Работа с новой ролью
	public function actionRoleAdd() {
		$model = new Roles();
		$priv_model = new Privileges();
        if(isset($_POST['ajax']) && $_POST['ajax']==='roleForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Roles'])) {
			$model->attributes = $_POST['Roles'];
			$model->privs_arr = $_POST['Roles']['privs_arr'];
			if($model->save()) {
				Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Роль <strong>".(strtoupper($_POST['Roles']['RoleName']))."</strong> успешно создана");
				$this->redirect(Yii::app()->createAbsoluteUrl('admin/roles'));
				Yii::app()->end();
			}
		}	
		$this->renderPartial('_form_include',array(
			'name' => '_roleform',
			'model' => $model,
			'privs' => $priv_model->findAll()
		), false, true);
	}
	//Редактирование роли
	public function actionRoleEdit($id) {
		$model = $this->loadRoleModel($id);

        if(isset($_POST['ajax']) && $_POST['ajax']==='roleForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Roles'])) {
			$model->attributes = $_POST['Roles'];
			$model->privs_arr = $_POST['Roles']['privs_arr'];
			if($model->save()) {
				Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Роль <strong>".(strtoupper($_POST['Roles']['RoleName']))."</strong> успешно отредактирована");
				$this->redirect(Yii::app()->createAbsoluteUrl('admin/roles'));
				Yii::app()->end();
			}
		}	
		$this->renderPartial('_form_include',array(
			'name' => '_roleform',
			'model' => $model,
			'id' => $id
		), false, true);
	}
	//Удаление роли
	public function actionRoleDelete($id) {
		$model = $this->loadRoleModel($id);
		$rolename = $model->RoleName;
		$model->deleteByPk($id);
		echo json_encode(array("rolename"=>$rolename));
	}
	
	//[USERLOG] Вывод лога авторизации пользователей
	public function actionUserLog() {
		$model = new UserLog();
		$dataProvider = $model->search();
		$this->render('userlog',array(
			'loglist' => $dataProvider,
			'model' => $model
		));
	}	
	
	//[SYSCONFIG] Вывод настроек системы
	public function actionSysConfig() {
		if( isset($_POST["pk"]) ) {
			$model=SysConfig::model()->findByPk($_POST["pk"]);
			$data = array(
				array( "name" => "p_session_id", "type" => "in", "value" => Yii::app()->user->SessId, "length" => 50 ),
				array( "name" => "p_config_name", "type" => "in", "value" => $_POST["pk"], "length" => 64 ),
				array( "name" => "p_config_desc", "type" => "in", "value" => $model->ConfigDesc, "length" => 512 ),
				array( "name" => "p_config_value", "type" => "in", "value" => $_POST["value"], "length" => 512 ),
				array( "name" => "p_out_error", "type" => "out", "length" => 5 ),
			);
			$res_arr = $this->_storedProcedureCall("Config_Update",$data);
		}
		$model = new SysConfig();
		$dataProvider = $model->search();
		$this->render('sysconfig',array(
			'configlist' => $dataProvider,
			'model' => $model
		));
	}
	
	//[CARTYPES] Работаем с категорями а/м
	public function actionCarTypes() {	
		$model = new CarTypes();
		if (isset($_POST['CarTypes'])) {
			Yii::app()->session->add("userSets[onlyEnabled]", $_POST['CarTypes']['onlyEnabled']);
		}
		$model->onlyEnabled = Yii::app()->session->get("userSets[onlyEnabled]");
		$dataProvider = $model->search();
		$this->render('cartypeslist',array(
			'dataset' => $dataProvider,
			'model' => $model
		));
	}
	public function actionCarTypeAdd() {
		$model = new CarTypes();
        if(isset($_POST['ajax']) && $_POST['ajax']==='cartypeForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['CarTypes'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "CarTypeID", "type" => "out", "length" => 5 ),
				array( "name" => "CarTypeName", "type" => "in", "value" => $_POST['CarTypes']['Name'], "length" => 5 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['CarTypes']['Enabled'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("CarTypes_Create",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новая категория успешно создана");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении категории");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/cartypes'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_cartypeform',
			'model' => $model
		), false, true);
	}	
	public function actionCarTypeEdit($id) {
		$model = CarTypes::model()->findByPk($id);
		$model->setScenario('Edit');
        if(isset($_POST['ajax']) && $_POST['ajax']==='cartypeForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['CarTypes'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "CarTypeID", "type" => "in", "value" => $id, "length" => 5 ),
				array( "name" => "CarTypeName", "type" => "in", "value" => $_POST['CarTypes']['Name'], "length" => 5 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['CarTypes']['Enabled'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("CarTypes_Update",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Категория успешно обновлена");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении категории");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/cartypes'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_cartypeform',
			'model' => $model
		), false, true);
	}		
	
	//[SERVICEGROUPS] Работаем с группами услуг
	public function actionServiceGroups() {
		$model = new ServiceGroups();
		if (isset($_POST['ServiceGroups'])) {
			Yii::app()->session->add("userSets[onlyEnabled]", $_POST['ServiceGroups']['onlyEnabled']);
		}
		$model->onlyEnabled = Yii::app()->session->get("userSets[onlyEnabled]");
		$dataProvider = $model->search();
		$this->render('servicegroupslist',array(
			'dataset' => $dataProvider,
			'model' => $model
		));
	}
	public function actionServiceGroupAdd() {
		$model = new ServiceGroups();
        if(isset($_POST['ajax']) && $_POST['ajax']==='serviceGroupForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['ServiceGroups'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "ServiceGroupID", "type" => "out", "length" => 5 ),
				array( "name" => "ServiceGroupName", "type" => "in", "value" => $_POST['ServiceGroups']['Name'], "length" => 500 ),
				array( "name" => "ServiceGroupTypeID", "type" => "in", "value" => $_POST['ServiceGroups']['ServiceGroupTypeID'], "length" => 5 ),
				array( "name" => "SortOrder", "type" => "in", "value" => $_POST['ServiceGroups']['SortOrder'], "length" => 5 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['ServiceGroups']['Enabled'], "length" => 1 ),
				array( "name" => "IsNew", "type" => "in", "value" => $_POST['ServiceGroups']['IsNew'], "length" => 1 ),
				array( "name" => "IsAction", "type" => "in", "value" => $_POST['ServiceGroups']['IsAction'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("ServiceGroups_Create",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новая группа успешно создана");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении группы");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/servicegroups'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_servicegroupform',
			'model' => $model
		), false, true);
	}	
	public function actionServiceGroupEdit($id) {
		$model = ServiceGroups::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='serviceGroupForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['ServiceGroups'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "ServiceGroupID", "type" => "in", "value" => $id, "length" => 5 ),
				array( "name" => "ServiceGroupName", "type" => "in", "value" => $_POST['ServiceGroups']['Name'], "length" => 500 ),
				array( "name" => "ServiceGroupTypeID", "type" => "in", "value" => $_POST['ServiceGroups']['ServiceGroupTypeID'], "length" => 5 ),
				array( "name" => "SortOrder", "type" => "in", "value" => $_POST['ServiceGroups']['SortOrder'], "length" => 5 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['ServiceGroups']['Enabled'], "length" => 1 ),
				array( "name" => "IsNew", "type" => "in", "value" => $_POST['ServiceGroups']['IsNew'], "length" => 1 ),
				array( "name" => "IsAction", "type" => "in", "value" => $_POST['ServiceGroups']['IsAction'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("ServiceGroups_Update",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Группа успешно обновлена");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении группы");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/servicegroups'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_servicegroupform',
			'model' => $model
		), false, true);
	}		
	
	//[SERVICES] Работаем с услугами
	public function actionServices() {
		$model = new Services();
		if (isset($_POST['Services'])) {
			Yii::app()->session->add("userSets[onlyEnabled]", $_POST['Services']['onlyEnabled']);
		}
		$model->onlyEnabled = Yii::app()->session->get("userSets[onlyEnabled]");
		$dataProvider = $model->search();
		$this->render('serviceslist',array(
			'dataset' => $dataProvider,
			'model' => $model
		));
	}
	public function actionServiceAdd() {
		$model = new Services();
        if(isset($_POST['ajax']) && $_POST['ajax']==='serviceForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Services'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "ServiceGroupID", "type" => "in", "value" => $_POST['Services']['ServiceGroupID'], "length" => 5 ),
				array( "name" => "ServiceID", "type" => "out", "length" => 5 ),
				array( "name" => "ServiceName", "type" => "in", "value" => $_POST['Services']['Name'], "length" => 500 ),
				array( "name" => "SortOrder", "type" => "in", "value" => $_POST['Services']['SortOrder'], "length" => 5 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['Services']['Enabled'], "length" => 1 ),
				array( "name" => "IsNew", "type" => "in", "value" => $_POST['Services']['IsNew'], "length" => 1 ),
				array( "name" => "IsAction", "type" => "in", "value" => $_POST['Services']['IsAction'], "length" => 1 ),
				array( "name" => "IsComplex", "type" => "in", "value" => $_POST['Services']['IsComplex'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("Services_Create",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новая услуга успешно добавлена");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении услуги");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/services'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_serviceform',
			'model' => $model
		), false, true);
	}	
	public function actionServiceEdit($id) {
		$model = Services::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='serviceForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Services'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "ServiceGroupID", "type" => "in", "value" => $_POST['Services']['ServiceGroupID'], "length" => 5 ),
				array( "name" => "ServiceID", "type" => "in", "length" => 5, "value" => $id ),
				array( "name" => "ServiceName", "type" => "in", "value" => $_POST['Services']['Name'], "length" => 500 ),
				array( "name" => "SortOrder", "type" => "in", "value" => $_POST['Services']['SortOrder'], "length" => 5 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['Services']['Enabled'], "length" => 1 ),
				array( "name" => "IsNew", "type" => "in", "value" => $_POST['Services']['IsNew'], "length" => 1 ),
				array( "name" => "IsAction", "type" => "in", "value" => $_POST['Services']['IsAction'], "length" => 1 ),
				array( "name" => "IsComplex", "type" => "in", "value" => $_POST['Services']['IsComplex'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("Services_Update",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Услуга успешно обновлена");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении услуги");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/services'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_serviceform',
			'model' => $model
		), false, true);
	}

	//[PRICES] Работаем с прайс-листом
	public function actionPriceList() {
		$services_model = new Services();
		$this->render('pricelist',array(
			'prices' => Prices::items(),
			'cartypes' => CarTypes::items(),
			'services' => $services_model->items(),
		));
	}	
	public function actionPriceAdd($serviceid,$ctypeid) {
		$model = new Prices();
        if(isset($_POST['ajax']) && $_POST['ajax']==='priceForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Prices'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "CarTypeID", "type" => "in", "value" => $ctypeid, "length" => 5 ),
				array( "name" => "ServiceID", "type" => "in", "value" => $serviceid, "length" => 10 ),
				array( "name" => "Value", "type" => "in", "value" => $_POST['Prices']['Value'], "length" => 10 ),
				array( "name" => "WorkerProfit", "type" => "in", "value" => $_POST['Prices']['WorkerProfit'], "length" => 10 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['Prices']['Enabled'], "length" => 1 ),
				array( "name" => "WorkerProfitIsFixed", "type" => "in", "value" => $_POST['Prices']['WorkerProfitIsFixed'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("Prices_Create",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новая цена успешно добавлена");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении цены");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/pricelist'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_priceform',
			'model' => $model
		), false, true);
	}		
	public function actionPriceEdit($serviceid,$ctypeid) {
		$model = Prices::model()->findByPk(array("ServiceID"=>$serviceid,"CarTypeID"=>$ctypeid));
        if(isset($_POST['ajax']) && $_POST['ajax']==='priceForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Prices'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "CarTypeID", "type" => "in", "value" => $ctypeid, "length" => 5 ),
				array( "name" => "ServiceID", "type" => "in", "value" => $serviceid, "length" => 10 ),
				array( "name" => "Value", "type" => "in", "value" => $_POST['Prices']['Value'], "length" => 10 ),
				array( "name" => "WorkerProfit", "type" => "in", "value" => $_POST['Prices']['WorkerProfit'], "length" => 10 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['Prices']['Enabled'], "length" => 1 ),
				array( "name" => "WorkerProfitIsFixed", "type" => "in", "value" => $_POST['Prices']['WorkerProfitIsFixed'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("Prices_Update",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Цена успешно обновлена");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении цены");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/pricelist'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_priceform',
			'model' => $model
		), false, true);
	}	
	
	//[POSTS] Работаем с постами
	public function actionPostsList() {
		$model = new Posts();
		if (isset($_POST['Posts'])) {
			Yii::app()->session->add("userSets[onlyEnabled]", $_POST['Posts']['onlyEnabled']);
		}
		$model->onlyEnabled = Yii::app()->session->get("userSets[onlyEnabled]");
		$dataProvider = $model->search();
		$this->render('postslist',array(
			'dataset' => $dataProvider,
			'model' => $model
		));
	}	
	public function actionPostAdd() {
		$model = new Posts();
        if(isset($_POST['ajax']) && $_POST['ajax']==='postForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Posts'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "ServiceGroupTypeID", "type" => "in", "value" => $_POST['Posts']['ServiceGroupTypeID'], "length" => 5 ),
				array( "name" => "PostID", "type" => "out", "length" => 5 ),
				array( "name" => "PostName", "type" => "in", "value" => $_POST['Posts']['Name'], "length" => 100 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['Posts']['Enabled'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("Posts_Create",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новый пост успешно добавлен");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении поста");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/postslist'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_postform',
			'model' => $model
		), false, true);
	}		
	public function actionPostEdit($id) {
		$model = Posts::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='postForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Posts'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "ServiceGroupTypeID", "type" => "in", "value" => $_POST['Posts']['ServiceGroupTypeID'], "length" => 5 ),
				array( "name" => "PostID", "type" => "in", "value" => $id, "length" => 5 ),
				array( "name" => "PostName", "type" => "in", "value" => $_POST['Posts']['Name'], "length" => 100 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['Posts']['Enabled'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("Posts_Update",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Пост успешно обновлен");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении поста");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/postslist'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_postform',
			'model' => $model
		), false, true);
	}	
	
	//[WORKERS] Работаем с рабочими
	public function actionWorkersList() {
		$model = new Workers();
		if (isset($_POST['Workers'])) {
			Yii::app()->session->add("userSets[onlyEnabled]", $_POST['Workers']['onlyEnabled']);
		}
		$model->onlyEnabled = Yii::app()->session->get("userSets[onlyEnabled]");
		$dataProvider = $model->search();
		$this->render('workerslist',array(
			'dataset' => $dataProvider,
			'model' => $model
		));
	}	
	public function actionWorkerAdd() {
		$model = new Workers();
        if(isset($_POST['ajax']) && $_POST['ajax']==='workerForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Workers'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "WorkerID", "type" => "out", "length" => 10 ),
				array( "name" => "WorkerName", "type" => "in", "value" => $_POST['Workers']['Name'], "length" => 255 ),
				array( "name" => "Phone", "type" => "in", "value" => ($_POST['Workers']['Phone']?:false), "length" => 20 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['Workers']['Enabled'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("Workers_Create",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новый рабочий успешно добавлен");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении рабочего");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/workerslist'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_workerform',
			'model' => $model
		), false, true);
	}		
	public function actionWorkerEdit($id) {
		$model = Workers::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='workerForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Workers'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "WorkerID", "type" => "in", "value" => $id, "length" => 10 ),
				array( "name" => "WorkerName", "type" => "in", "value" => $_POST['Workers']['Name'], "length" => 255 ),
				array( "name" => "Phone", "type" => "in", "value" => ($_POST['Workers']['Phone']?:null), "length" => 20 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['Workers']['Enabled'], "length" => 1 ),
				array( "name" => "IsOnVacation", "type" => "in", "value" => $_POST['Workers']['IsOnVacation'], "length" => 1 ),
			);
			$res_arr = $this->_storedProcedureCall("Workers_Update",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Данные рабочего успешно обновлены");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении данных рабочего");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/workerslist'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_workerform',
			'model' => $model
		), false, true);
	}
    public function actionWorkersSalary() {
        if( isset($_POST["Workers"]["salaryDateRange"])) {
            $dates_arr = explode(" - ", $_POST["Workers"]["salaryDateRange"]);
            $data = array(
                array( "name" => "DateFrom", "type" => "in", "value" => $dates_arr[0], "length" => 10 ),
                array( "name" => "DateTo", "type" => "in", "value" => $dates_arr[1], "length" => 10 ),
            );
            $res_arr = $this->_storedProcedureCall("Report_WorkersSalary",$data,true);
        }
        $this->render('workerssalary',array(
            'model' => Workers::model(),
            'result' => (isset($res_arr)?$res_arr:0),
            'sel_date_range' => (isset($_POST["Workers"]["salaryDateRange"])?$_POST["Workers"]["salaryDateRange"]:null)
        ));
    }


    // ИНТЕРФЕЙС КЛАДОВЩИКА //
	
	//[GOODS] Работаем с товарами
	public function actionGoods() {
		$model = new Goods();
		if (isset($_POST['Goods'])) {
			Yii::app()->session->add("userSets[onlyEnabled]", $_POST['Goods']['onlyEnabled']);
		}
		$model->onlyEnabled = Yii::app()->session->get("userSets[onlyEnabled]");
		$dataProvider = $model->search();
		$this->render('goodslist',array(
			'dataset' => $dataProvider,
			'model' => $model
		));
	}
	public function actionGoodsAdd() {
		$model = new Goods();
        if(isset($_POST['ajax']) && $_POST['ajax']==='goodsForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Goods'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "GoodID", "type" => "out", "length" => 10 ),
				array( "name" => "Name", "type" => "in", "value" => $_POST['Goods']['Name'], "length" => 500 ),
				array( "name" => "Price", "type" => "in", "value" => $_POST['Goods']['Price'], "length" => 10 ),
			);
			$res_arr = $this->_storedProcedureCall("Goods_Create",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новый товар успешно добавлен");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении товара");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/goods'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_goodsform',
			'model' => $model
		), false, true);
	}	
	public function actionGoodsEdit($id) {
		$model = Goods::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='goodsForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Goods'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "GoodID", "type" => "in", "value" => $id, "length" => 10 ),
				array( "name" => "Name", "type" => "in", "value" => $_POST['Goods']['Name'], "length" => 500 ),
				array( "name" => "Enabled", "type" => "in", "value" => $_POST['Goods']['Enabled'], "length" => 1 ),
                array( "name" => "Price", "type" => "in", "value" => $_POST['Goods']['Price'], "length" => 10 ),
			);
			$res_arr = $this->_storedProcedureCall("Goods_Update",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Данные товара успешно обновлены");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении данных товара");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/goods'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_goodsform',
			'model' => $model
		), false, true);
	}	
	public function actionGoodsMove($id) {
		$model = Goods::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='goodsMoveForm') {
            if( !$_POST['Goods']['IsIncome'] )
				$model->setScenario("outcome");
			echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['Goods'])) {
			$data = array(
				array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
				array( "name" => "GoodID", "type" => "in", "value" => $id, "length" => 10 ),
				array( "name" => "IsIncome", "type" => "in", "value" => $_POST['Goods']['IsIncome'], "length" => 1 ),
				array( "name" => "Count", "type" => "in", "value" => $_POST['Goods']['Quantity'], "length" => 5 ),
			);
			$res_arr = $this->_storedProcedureCall("Goods_Move",$data);
			if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> {$_POST['Goods']['Quantity']} единиц(ы) <strong>{$model->Name}</strong> успешно ".($_POST['Goods']['IsIncome']?"приняты":"списаны")."" );
			elseif( $res_arr["result"] == 9 ) Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Попытка списать товара больше, чем есть в наличии, либо несуществующий товар");
			else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении данных товара");
			$this->redirect(Yii::app()->createAbsoluteUrl('admin/goods'));
			Yii::app()->end();

		}	
		$this->renderPartial('_form_include',array(
			'name' => '_goodsmoveform',
			'model' => $model
		), false, true);
	}	
	public function actionGoodsLog() {
		$model = new GoodsMovement();
		$dataProvider = $model->search();
		$this->render('goodslog',array(
			'dataset' => $dataProvider,
			'model' => $model
		));
	}	
	
	// ИНТЕРФЕЙС КАССИРА //
	
	//[ORDERS] Работаем с заказами
	public function actionOrders() {
		$model = new Orders();
		$this->render('orderslist',array(
			'model' => $model
		));
	}
    //Добавляем к заказу товары из магазина
    public function actionAddFromShop($orderid) {
        if (isset($_POST['items_to_add'])) {
            $errors = array();
            if($_POST['items_to_add'] != "") {
                $items_arr = explode(",",$_POST['items_to_add']);
                foreach($items_arr as $item_id) {
                    $data = array(
                        array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                        array( "name" => "OrderID", "type" => "in", "value" => $orderid+0, "length" => 5 ),
                        array( "name" => "GoodID", "type" => "in", "value" => $item_id+0, "length" => 10 ),
                        array( "name" => "IsAdd", "type" => "in", "value" => 1, "length" => 1 ),
                    );
                    $res_arr = $this->_storedProcedureCall("SalesGoods_Link",$data);
                    if( $res_arr["result"] ) $errors[] = $res_arr["result"];
                }
            }
            if($_POST['items_to_remove'] != "") {
                $items_arr = explode(",",$_POST['items_to_remove']);
                foreach($items_arr as $item_id) {
                    $data = array(
                        array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                        array( "name" => "OrderID", "type" => "in", "value" => $orderid+0, "length" => 5 ),
                        array( "name" => "GoodID", "type" => "in", "value" => $item_id+0, "length" => 10 ),
                        array( "name" => "IsAdd", "type" => "in", "value" => 0, "length" => 1 ),
                    );
                    $res_arr = $this->_storedProcedureCall("SalesGoods_Link",$data);
                    if( $res_arr["result"] ) $errors[] = $res_arr["result"];
                }
            }
            if( count($errors) == 0 ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Список товаров в заказе был успешно обновлен!" );
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Не все товары были обновлены у заказа ".(implode("::",$errors)));
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/orders'));
            Yii::app()->end();

        }
        $model_goods = new Goods();
        $model_sales = new Sales();
        $dataProvider = $model_goods->findAll("Enabled=1");
        $dataProveider_sales = $model_sales->findAll("OrderID = :ordID AND Enabled = 1", array("ordID"=>$orderid));
        $order_data = Orders::model()->findByPk($orderid);
        $this->renderPartial('_form_include',array(
            'name' => '_addfromshop',
            'goods_data' => $dataProvider,
            'sales_data' => $dataProveider_sales,
            'can_remove_goods' => ($order_data["PayStatus"]==PayStatuses::FULL_PAID?0:1),
            'model' => $model_goods
        ), false, true);
    }
	
	//[TASKS] Показать задачи
	public function actionTasks($orderid) {
		$model = new Tasks();
		$dataProvider = $model->findAll("OrderID = :ordID", array("ordID"=>$orderid));
		$this->renderPartial('taskslist',array(
			'dataset' => $dataProvider,
			//'model' => $model
		));
	}

    //Оплата заказа
    public function actionPayOrder($orderid) {
        $model = new Orders();

        //валидация формы
        if(isset($_POST['ajax']) && $_POST['ajax']==='payOrderForm' && $_POST['Orders']['PayStatus'] != PayStatuses::FULL_PAID) {
            if( $_POST['Orders']['new_PayTypeID'] == PayTypes::CARD)
                $model->setScenario("paybycard");
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        //получаем данные заказа
        $dataProvider = $model->findByPk($orderid);
        $price = $this->priceFormat($dataProvider["TotalAmount"] - $dataProvider["DiscountAmount"] - $dataProvider["PayedAmount"]);
        $discount_off = 0;
        $claim_off = 0;
        $client_off = 0;

        //получаем данные из магазина
        $model_sales = new Sales();
        $sales_data = $model_sales->findAll("OrderID = :ordID AND Enabled = 1", array("ordID"=>$orderid));
        $sales_count = $model_sales->count("OrderID = :ordID AND Enabled = 1", array("ordID"=>$orderid));

        //проверим возможность оплаты по договору
        $actpay = false;
        if( $dataProvider->CarID ) {
            $actcars_model = new ActCars();
            $actcars_data = $actcars_model->findAll("Enabled = 1 AND CarID = :carID",array("carID"=>$dataProvider->CarID));
            foreach($actcars_data as $row) {
                if( $row->act->Enabled ) {
                    $expr_date = strtotime($row->act->ExpireDate);
                    if( $expr_date > strtotime("now") ) {
                        $actpay = true;
                        break;
                    }
                }
            }
        }
        //пересчет с учетом скидки
        if( isset($_POST['action']) && $_POST['action'] == "apply_discount") {
            $model->new_PayTypeID = $_POST["new_PayTypeID"];
            $model->new_DiscountCard = $_POST["new_DiscountCard"];
            $model->new_ClaimID = $_POST["new_ClaimID"];
            $model->BankID = $_POST["BankID"];
            if( $_POST["new_DiscountCard"] != "" ) {
                $cards_model = new Cards();
                $data_cards = $cards_model->findByPk($_POST["new_DiscountCard"]);
                if( $data_cards["ID"] == "" || !$data_cards["Enabled"] )  $model->addError("new_DiscountCard","Карта не найдена в системе");
                else {
                    $discount = $data_cards["Discount"];
                    $discount_off = $this->priceFormat(round($data_cards["Discount"]/100*$price,2));
                }
            }
            if( $_POST["new_ClaimID"] != "" ) {
                $claim_model = new Claims();
                $data_claims = $claim_model->findByPk($_POST["new_ClaimID"]);
                if( $data_claims["ID"] == "" )
                    $model->addError("new_ClaimID","Карта не найдена в системе");
                elseif( $data_claims["IsUsed"] )
                    $model->addError("new_ClaimID","Эта карта уже использована");
                else
                    $claim_off = $this->priceFormat($data_claims["Amount"]);
            }
        }

        //смотрим, если у заказа есть ClientID, то смотрим клиента на возможность расплатиться предоплачеными счетом
        if($dataProvider->ClientID) {
            $card_data = Cards::model()->find("ClientID = :ClientID", array(":ClientID"=>$dataProvider->ClientID));
            if($card_data["ID"]) {
                $discount = $card_data["Discount"];
                $discount_off = $this->priceFormat(round($card_data["Discount"]/100*$price,2));
                $model->new_DiscountCard = $card_data["ID"];
            }
            if( $dataProvider->client->Balance > 0 )
                $client_off = $this->priceFormat($dataProvider->client->Balance);
        }
        //смотрим, если у заказа есть номер скидочной карты, автоматически его дергаем и считаем скидку
        /*if($dataProvider->DiscountCardID) {
            $model->new_DiscountCard = $dataProvider->DiscountCardID;
            $data_cards = Cards::model()->findByPk($dataProvider->DiscountCardID);
            $discount = $data_cards["Discount"];
            $discount_off = $this->priceFormat(round($data_cards["Discount"]/100*$price,2));
        }*/
        $new_price = $this->priceFormat($dataProvider["TotalAmount"] - $dataProvider["DiscountAmount"] - $dataProvider["PayedAmount"] - $discount_off - $claim_off);
        $hidden_price = $this->priceFormat($new_price - $client_off);
        //оплата
        if (isset($_POST['Orders'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5,  ),
                array( "name" => "OrderID", "type" => "in", "value" => $orderid, "length" => 10,   ),
                array( "name" => "PayTypeID", "type" => "in", "value" => $_POST['Orders']['new_PayTypeID'], "length" => 2,   ),
                array( "name" => "BankID", "type" => "in", "value" => ($_POST['Orders']['new_PayTypeID']==2&&$_POST['Orders']['BankID']!=0?$_POST['Orders']['BankID']:null), "length" => 10,   ),
                array( "name" => "DiscountCardID", "type" => "in", "value" => (isset($_POST['Orders']['new_DiscountCard'])?$_POST['Orders']['new_DiscountCard']:null), "length" => 5,   ),
                array( "name" => "ClaimID", "type" => "in", "value" => (isset($_POST['Orders']['new_ClaimID'])?$_POST['Orders']['new_ClaimID']:null), "length" => 10,  ),
                array( "name" => "UseClientBalance", "type" => "in", "value" => (isset($_POST['Orders']['UseClientBalance'])?$_POST['Orders']['UseClientBalance']:0), "length" => 1,  ),
                array( "name" => "ManualDiscount", "type" => "in", "value" => (isset($_POST['Orders']['special_discount'])&&isset($_POST['Orders']['sd_amount'])?$_POST['Orders']['sd_amount']:null), "length" => 5,  )
            );
            $res_arr = $this->_storedProcedureCall("Orders_Pay",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Статус заказа успешно обновлен" );
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Не удалось обновить статус заказа ");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/orders'));
            Yii::app()->end();
        }
        $this->renderPartial('_form_include',array(
            'name' => ($dataProvider["PayStatus"]==PayStatuses::FULL_PAID?'_completeorder':'_payorder'),
            'order_data' => $dataProvider,
            'pay_type4js' => ($model->new_PayTypeID==""?PayTypes::CASH:$model->new_PayTypeID),
            'tasks_list' => Tasks::model()->findAll("OrderID = :ordID", array("ordID"=>$orderid)),
            'model' => $model,
            'discount' => (isset($discount)?$discount:false),
            'discount_off' => $discount_off,
            'claim_off' => $claim_off,
            'client_off' => $client_off,
            'new_price' => ($new_price<0?0:$new_price),
            'hidden_price' => ($hidden_price<0?0:$hidden_price),
            'error' => (isset($error)?$error:false),
            'actpay' => $actpay,
            'price' => $price,
            'sales_data' => $sales_data,
            'sales_count' => $sales_count
        ), false, true);
    }

    // ИНТЕРФЕЙС РАБОТЫ С ДОГОВОРАМИ //

    /** [ACTS] Вывод всех договоров
     */
    function actionActs(){
        $model = new Acts();
        $dataProvider = $model->search();
        $this->render('actslist',array(
            'dataset' => $dataProvider,
            //'model' => $model
        ));
    }

    /** добавление договора
     */
    function actionActCreate(){
        $model = new Acts();
        if(isset($_POST['ajax']) && $_POST['ajax']==='actForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Acts'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "ActID", "type" => "out", "length" => 10 ),
                array( "name" => "ActName", "type" => "in", "value" => $_POST['Acts']['Name'], "length" => 500 ),
                array( "name" => "ExpireDate", "type" => "in", "value" => $_POST['Acts']['ExpireDate'], "length" => 10 ),
                array( "name" => "Enabled", "type" => "in", "value" => 1, "length" => 1 ),
            );
            $res_arr = $this->_storedProcedureCall("Acts_Create",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новый договор успешно добавлен");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении договора");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/acts'));
            Yii::app()->end();

        }
        $this->renderPartial('_form_include',array(
            'name' => '_actform',
            'model' => $model
        ), false, true);
    }
    /** редактирование договора
     * @param integer $id ID договора, который редактируем
     */
    public function actionActEdit($id) {
        $model = Acts::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='actForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Acts'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "ActID", "type" => "in", "value" => $id, "length" => 10 ),
                array( "name" => "ActName", "type" => "in", "value" => $_POST['Acts']['Name'], "length" => 500 ),
                array( "name" => "ExpireDate", "type" => "in", "value" => $_POST['Acts']['ExpireDate'], "length" => 10 ),
                array( "name" => "Enabled", "type" => "in", "value" => $_POST['Acts']['Enabled'], "length" => 1 ),
            );
            $res_arr = $this->_storedProcedureCall("Acts_Update",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Данные договора успешно обновлены");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении данных договора");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/acts'));
            Yii::app()->end();

        }
        $this->renderPartial('_form_include',array(
            'name' => '_actform',
            'model' => $model
        ), false, true);
    }
    /** привязка/отвязка машин к/от договору(а)
     * @param integer $id ID договора, к которому привязываем или от которого отвязываем тачки
     */
    public function actionActCars($id) {
        //если выбрали CSV-файл, надо с ним поработать
        if( isset($_FILES) && isset($_FILES["csvupload"]) ) {
            $cars_arr = explode("\r\n",$this->_WinToUTF8Encode(file_get_contents($_FILES["csvupload"]["tmp_name"])));
            $old_links_arr = array();
            $registered_cars = array();
            $linked_cars = array();
            foreach( ActCars::model()->findAll("ActID = :actid and Enabled = 1",array(":actid"=>$id)) as $car ) {
                $old_links_arr[] = $car["CarID"];
            }
            foreach( Cars::model()->findAll() as $car ) {
                $registered_cars[] = strtoupper($car["ID"]);
            }
            $err_arr = array();
            foreach( $cars_arr as $car ) {
                if( $car != "" ) {
                    $car_arr = explode(";",$car);
                    //проверим, если машины нет в системе, надо её завести
                    if( !in_array($car_arr[0], $registered_cars) ) {
                        //получим model_id по названию
                        $model_data = CarModels::model()->find("Name = :name",array(":name"=>$car_arr[2]));
                        if($model_data["ID"]=="") {
                            $err_arr[] = "Не удалось добавить машину с номером <strong>{$car_arr[0]}</strong> - неизвестная модель: <strong>{$car_arr[2]}</strong>";
                            continue;
                        }
                        $res_add = $this->_createCar($car_arr[0],$model_data["ID"]);
                        if( $res_add["result"] ) {
                            $err_arr[] = "Не удалось добавить машину с номером <strong>{$car_arr[0]}</strong>";
                            continue;
                        }
                    }
                    //проверим, есть ли машина уже в старых связях с текущим договором, если нет, тогда пробуем связать
                    if( !in_array($car_arr[0], $old_links_arr) ) {
                        $res_link = $this->_actCarLink($car_arr[0],$id);
                        if( $res_link["result"] ) {
                            $err_arr[] = "Не удалось связать машину с номером <strong>{$car_arr[0]}</strong> с текущим договором";
                            continue;
                        }
                    }
                    $linked_cars[] = $car_arr[0];
                }
            }
            if( !isset($_POST["keep_old"]) || !$_POST["keep_old"] ) {
                //с учетом старых связей, смотрим, какие связи убрались и убираем их из базы
                $remove_links = array_diff($old_links_arr,$linked_cars);
                foreach($remove_links as $carid) {
                    $res_link = $this->_actCarLink($carid,$id,'0');
                    if( $res_link["result"] )
                        $err_arr[] = "Не удалось убрать связь машины с номером <strong>{$carid}</strong> с текущим договором";
                }
            }
            if( count($err_arr) == 0 ) $res = array("mess_class" => 'success',"text" => "<strong>Успех!</strong> Связи договора и машин были успешно обновлены");
            else $res = array("mess_class" => 'error', "text" => "<strong>Внимание!</strong> Во время связывания машин и договора произошли следующие ошибки:<br/>".implode("<br/>",$err_arr));
            echo json_encode($res);
            Yii::app()->end();
        }

        $act_data = Acts::model()->findByPk($id);
        $acmodel = new ActCars();
        $actcars = $acmodel->search($id);

        //получаем список всех машин
        $cars_model = new Cars();
        $cars = $cars_model->findAll();
        $cars_list = array();
        foreach($cars as $row){
            $cars_list[$row->ID] = $row->ID;
        }
        $this->render('actcarslist',array(
            'act_data' => $act_data,
            'actcars_list' => $actcars,
            'allcars' => $cars_list,
            'ActID' => $id,
        ));
    }
    function actionActCarsCreate($actid) {
        $model = new ActCars();
        if(isset($_POST['ajax']) && $_POST['ajax']==='actForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ActCars'])) {
            $form = $_POST['ActCars'];
            $car_data = Cars::model()->findByPk($form["CarID"]);
            if( $car_data["ID"] == "" ) {
                $add_result = $this->_createCar($form["CarID"],$form["modelID"]);
                if( $add_result["result"] ) {
                    Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении нового автомобиля");
                    $this->redirect(Yii::app()->createAbsoluteUrl('admin/actcars',array("id"=>$actid)));
                    Yii::app()->end();
                }

            }
            $link_res = $this->_actCarLink($form["CarID"],$actid);
            if( !$link_res["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Связи договора и машин были успешно обновлены");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении связей машин и договора");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/actcars',array("id"=>$actid)));
            Yii::app()->end();
        }
        $all_brands_data = CHtml::listData(CarBrands::model()->findAll(array("order"=>"Name")), 'ID', 'Name');
        $curr_models_data = CHtml::listData(CarModels::model()->findAll("CarBrandID = :cbid",array(":cbid"=>key($all_brands_data))), 'ID', 'Name');
        $this->renderPartial('_form_include',array(
            'name' => '_actcarsform',
            'actcars_model' => $model,
            'all_brands_data' => $all_brands_data,
            'curr_models_data' => $curr_models_data
        ), false, true);
    }
    function actionActCarDelete_ajax($carid,$actid){
        $res = $this->_actCarLink($carid,$actid,'0');
        if( $res["result"] > 0 ) $result = array("mess_class" => 'error', "text" => "<strong>Ошибка!</strong> Не удалось убрать машину <strong>$carid</strong> из договора");
        else $result = array("mess_class" => 'success',"text" => "<strong>Успех!</strong> Машина <strong>$carid</strong> успешна убрана из договора");
        echo json_encode($result);
        Yii::app()->end();
    }
    private function _createCar($numb, $model) {
        $data = array(
            array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
            array( "name" => "CarID", "type" => "in", "value" => $numb, "length" => 9 ),
            array( "name" => "CarModelID", "type" => "in", "value" => $model, "length" => 5 ),
        );
        return $this->_storedProcedureCall("Cars_Create",$data);
    }

    private function _actCarLink( $carid, $actid, $link=1 ) {
        $data = array(
            array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
            array( "name" => "CarID", "type" => "in", "value" => $carid, "length" => 5 ),
            array( "name" => "ActID", "type" => "in", "value" => $actid, "length" => 10 ),
            array( "name" => "IsLink", "type" => "in", "value" => $link, "length" => 1 ),
        );
        return $this->_storedProcedureCall("ActCars_Link",$data);
    }

    /** [CLAIMS] Вывод всех карт рекламации
     */
    function actionClaims(){
        $model = new Claims();
        $dataProvider = $model->search();
        $this->render('claimslist',array(
            'dataset' => $dataProvider,
        ));
    }
    /** добавление карты
     */
    function actionClaimCreate(){
        $model = new Claims();
        $all_claims_arr = CHtml::listData($model->findAll(), 'ID', 'OrderID');
        $orders_model = new Orders();
        //$orders_data = $orders_model->findAll("Enabled = 1  and PayStatus in(:ps1,:ps2)", array(
        $orders_data = $orders_model->findAll("Enabled = 1");
        $orders_list = array();
        foreach($orders_data as $row){
            if( !in_array($row->ID,$all_claims_arr) )
                $orders_list[$row->ID] = $row->CarID;
        }
        if(isset($_POST['ajax']) && $_POST['ajax']==='actForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Claims'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "ClaimID", "type" => "in", "value" => $_POST['Claims']['ID'], "length" => 19 ),
                array( "name" => "Name", "type" => "in", "value" => $_POST['Claims']['Name'], "length" => 500 ),
                array( "name" => "Amount", "type" => "in", "value" => $_POST['Claims']['Amount'], "length" => 10 ),
                array( "name" => "OrderID", "type" => "in", "value" => $_POST['Claims']['OrderID'], "length" => 10 ),
            );
            $res_arr = $this->_storedProcedureCall("Claims_Create",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новая карта успешно заведена");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении карты");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/claims'));
            Yii::app()->end();

        }
        $this->renderPartial('_form_include',array(
            'name' => '_claimform',
            'model' => $model,
            'orders_list' => $orders_list
        ), false, true);
    }
    /** редактирование использованой карты
     */
    function actionClaimEdit($id){
        $model = Claims::model()->findByPk($id);
        $all_claims_arr = CHtml::listData(Claims::model()->findAll(), 'ID', 'OrderID');
        $orders_model = new Orders();
        //$orders_data = $orders_model->findAll("Enabled = 1  and PayStatus in(:ps1,:ps2)", array(
        $orders_data = $orders_model->findAll("Enabled = 1", array(
            //"ps1"=>PayStatuses::NOT_PAID,
            //"ps2"=>PayStatuses::PART_PAID
        ));
        $orders_list = array();
        foreach($orders_data as $row){
            if( !in_array($row->ID,$all_claims_arr) )
                $orders_list[$row->ID] = $row->CarID;
        }
        if(isset($_POST['ajax']) && $_POST['ajax']==='actForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Claims'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "ClaimID", "type" => "in", "value" => $id ),
                array( "name" => "Name", "type" => "in", "value" => $_POST['Claims']['Name'], "length" => 500 ),
                array( "name" => "Amount", "type" => "in", "value" => $_POST['Claims']['Amount'], "length" => 10 ),
                array( "name" => "OrderID", "type" => "in", "value" => $_POST['Claims']['OrderID'], "length" => 10 ),
            );
            $res_arr = $this->_storedProcedureCall("Claims_Update",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Карта заведена повторно");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при возобновлении карты");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/claims'));
            Yii::app()->end();

        }
        $this->renderPartial('_form_include',array(
            'name' => '_claimform',
            'model' => $model,
            'orders_list' => $orders_list
        ), false, true);
    }
    // ИНТЕРФЕЙС РАБОТЫ С БАНКАМИ //

    /** [Banks] Вывод всех банков
     */
    function actionBanks(){
        $model = new Banks();
        $dataProvider = $model->search();
        $this->render('bankslist',array(
            'dataset' => $dataProvider,
        ));
    }
    /** добавление банка
     */
    function actionBankCreate(){
        $model = new Banks();
        if(isset($_POST['ajax']) && $_POST['ajax']==='bankForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Banks'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "BankID", "type" => "out", "length" => 10 ),
                array( "name" => "Name", "type" => "in", "value" => $_POST['Banks']['Name'], "length" => 500 ),
                array( "name" => "Discount", "type" => "in", "value" => $_POST['Banks']['Discount'], "length" => 10 ),
                array( "name" => "Enabled", "type" => "in", "value" => 1, "length" => 1 ),
            );
            $res_arr = $this->_storedProcedureCall("Banks_Create",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новый банк успешно добавлен");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении банка");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/banks'));
            Yii::app()->end();

        }
        $this->renderPartial('_form_include',array(
            'name' => '_bankform',
            'model' => $model
        ), false, true);
    }
    /** редактирование банка
     * @param integer $id ID банка, который редактируем
     */
    public function actionBankEdit($id) {
        $model = Banks::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='bankForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Banks'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "BankID", "type" => "in", "value" => $id, "length" => 10 ),
                array( "name" => "Name", "type" => "in", "value" => $_POST['Banks']['Name'], "length" => 500 ),
                array( "name" => "Discount", "type" => "in", "value" => $_POST['Banks']['Discount'], "length" => 10 ),
                array( "name" => "Enabled", "type" => "in", "value" => 1, "length" => 1 ),
            );
            $res_arr = $this->_storedProcedureCall("Banks_Update",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Данные банка успешно обновлены");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении данных банка");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/banks'));
            Yii::app()->end();

        }
        $this->renderPartial('_form_include',array(
            'name' => '_bankform',
            'model' => $model
        ), false, true);
    }

    // ИНТЕРФЕЙС РАБОТЫ С КЛИЕНТАМИ //

    /** [Clients] Вывод всех клиентов
     */
    function actionClients(){
        $model = new Clients();
        $dataProvider = $model->search();
        $this->render('clientslist',array(
            'dataset' => $dataProvider,
        ));
    }
    /** добавление клиента
     */
    function actionClientCreate(){
        $model = new Clients();
        $model_card = new Cards();
        $model_cars = new Cars();
        if(isset($_POST['ajax']) && $_POST['ajax']==='clientForm') {
            echo CActiveForm::validate(array($model,$model_card,$model_cars));
            Yii::app()->end();
        }
        if (isset($_POST['Clients'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "ClientID", "type" => "out", "length" => 10 ),
                array( "name" => "Name", "type" => "in", "value" => $_POST['Clients']['Name'], "length" => 500 ),
                array( "name" => "IsMale", "type" => "in", "value" => $_POST['Clients']['IsMale'], "length" => 1 ),
                array( "name" => "PhoneNumber", "type" => "in", "value" => $_POST['Clients']['PhoneNumber'], "length" => 18 ),
                array( "name" => "Balance", "type" => "in", "value" => ($_POST['Clients']['Balance']!=""?$_POST['Clients']['Balance']:'0'), "length" => 18 ),
                array( "name" => "Age", "type" => "in", "value" => $_POST['Clients']['Age'], "length" => 2 ),
            );
            $res_arr = $this->_storedProcedureCall("Clients_Create",$data);
            if( $client_id = $res_arr["out_data"]["ClientID"]) {
                $data = array(
                    array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                    array( "name" => "CardID", "type" => "in", "value" => $_POST['Cards']['ID'], "length" => 9 ),
                    array( "name" => "ClientID", "type" => "in", "value" => $client_id, "length" => 10 ),
                    array( "name" => "Discount", "type" => "in", "value" => $_POST['Cards']['Discount'], "length" => 10 ),
                    array( "name" => "ExpireDate", "type" => "in", "value" => $_POST['Cards']['ExpireDate'], "length" => 10 ),
                    array( "name" => "Enabled", "type" => "in", "value" => 1, "length" => 1 ),
                );
                $res_arr_card = $this->_storedProcedureCall("Cards_Create",$data);
                $data = array(
                    array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                    array( "name" => "CarID", "type" => "in", "value" => $_POST['Cars']['ID'], "length" => 10 ),
                    array( "name" => "ClientID", "type" => "in", "value" => $client_id, "length" => 10 ),
                    array( "name" => "CarModelID", "type" => "in", "value" => $_POST['Cars']['CarModelID'], "length" => 7 ),
                );
                $res_arr_car = $this->_storedProcedureCall("Cars_Create",$data);
            }
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новый клиент успешно добавлен");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении клиента");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/clients'));
            Yii::app()->end();

        }
        $all_brands_data = CHtml::listData(CarBrands::model()->findAll(array("order"=>"Name")), 'ID', 'Name');
        $curr_models_data = CHtml::listData(CarModels::model()->findAll("CarBrandID = :cbid",array(":cbid"=>key($all_brands_data))), 'ID', 'Name');
        $this->renderPartial('_form_include',array(
            'name' => '_clientformadd',
            'model' => $model,
            'model_card' => $model_card,
            'model_car' => $model_cars,
            'all_brands_data' => $all_brands_data,
            'curr_models_data' => $curr_models_data
        ), false, true);
    }
    /** редактирование клиента
     * @param integer $id ID клиента, который редактируем
     */
    public function actionClientEdit($id) {
        $model = Clients::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='clientForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Clients'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "ClientID", "type" => "in", "value" => $id, "length" => 10 ),
                array( "name" => "Name", "type" => "in", "value" => $_POST['Clients']['Name'], "length" => 500 ),
                array( "name" => "IsMale", "type" => "in", "value" => $_POST['Clients']['IsMale'], "length" => 1 ),
                array( "name" => "PhoneNumber", "type" => "in", "value" => $_POST['Clients']['PhoneNumber'], "length" => 18 ),
                array( "name" => "Balance", "type" => "in", "value" => $_POST['Clients']['Balance'], "length" => 18 ),
                array( "name" => "Age", "type" => "in", "value" => $_POST['Clients']['Age'], "length" => 2 ),
            );
            $res_arr = $this->_storedProcedureCall("Clients_Update",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Данные клиента успешно обновлены");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении данных клиента");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/clients'));
            Yii::app()->end();

        }
        $this->renderPartial('_form_include',array(
            'name' => '_clientformedit',
            'model' => $model
        ), false, true);
    }

    // ИНТЕРФЕЙС РАБОТЫ С КАРТАМИ //

    /** [Cards] Вывод всех карт
     */
    function actionCards(){
        $model = new Cards();
        $dataProvider = $model->search();
        $this->render('cardslist',array(
            'dataset' => $dataProvider,
        ));
    }
    /** добавление карты
     */
    function actionCardCreate(){
        $model = new Cards();
        if(isset($_POST['ajax']) && $_POST['ajax']==='cardForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Cards'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "CardID", "type" => "in", "value" => $_POST['Cards']['ID'], "length" => 9 ),
                array( "name" => "ClientID", "type" => "in", "value" => $_POST['Cards']['ClientID'], "length" => 10 ),
                array( "name" => "Discount", "type" => "in", "value" => $_POST['Cards']['Discount'], "length" => 10 ),
                array( "name" => "ExpireDate", "type" => "in", "value" => $_POST['Cards']['ExpireDate'], "length" => 10 ),
                array( "name" => "Enabled", "type" => "in", "value" => 1, "length" => 1 ),
            );
            $res_arr = $this->_storedProcedureCall("Cards_Create",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новая карта успешно добавлена");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении карты");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/cards'));
            Yii::app()->end();

        }
        $this->renderPartial('_form_include',array(
            'name' => '_cardform',
            'model' => $model
        ), false, true);
    }
    /** редактирование карты
     * @param integer $id ID карты, которую редактируем
     */
    public function actionCardEdit($id) {
        $model = Cards::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='cardForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Cards'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "CardID", "type" => "in", "value" => $id, "length" => 10 ),
                array( "name" => "ClientID", "type" => "in", "value" => $_POST['Cards']['ClientID'], "length" => 10 ),
                array( "name" => "Discount", "type" => "in", "value" => $_POST['Cards']['Discount'], "length" => 10 ),
                array( "name" => "ExpireDate", "type" => "in", "value" => $_POST['Cards']['ExpireDate'], "length" => 10 ),
                array( "name" => "Enabled", "type" => "in", "value" => $_POST['Cards']['Enabled'], "length" => 1 ),
            );
            $res_arr = $this->_storedProcedureCall("Cards_Update",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Данные карты успешно обновлены");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении данных карты");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/cards'));
            Yii::app()->end();

        }
        $this->renderPartial('_form_include',array(
            'name' => '_cardform',
            'model' => $model
        ), false, true);
    }
    /** список всех машин
     */
    public function actionCars() {
        $model = new Cars();
        $this->render('carslist',array(
            'model' => $model,
        ));
    }
    /** добавление машины
     */
    function actionCarCreate(){
        $model = new Cars();
        if(isset($_POST['ajax']) && $_POST['ajax']==='carForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Cars'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "CarID", "type" => "in", "value" => $_POST['Cars']['ID'], "length" => 10 ),
                array( "name" => "ClientID", "type" => "in", "value" => $_POST['Cars']['ClientID'], "length" => 7 ),
                array( "name" => "CarModelID", "type" => "in", "value" => $_POST['Cars']['CarModelID'], "length" => 7 ),
            );
            $res_arr = $this->_storedProcedureCall("Cars_Create",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Новая машина успешно добавлена");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при добавлении машины");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/cars'));
            Yii::app()->end();

        }
        $all_brands_data = CHtml::listData(CarBrands::model()->findAll(array("order"=>"Name")), 'ID', 'Name');
        $curr_models_data = CHtml::listData(CarModels::model()->findAll("CarBrandID = :cbid",array(":cbid"=>key($all_brands_data))), 'ID', 'Name');
        $this->renderPartial('_form_include',array(
            'name' => '_carform',
            'model' => $model,
            'all_brands_data' => $all_brands_data,
            'curr_models_data' => $curr_models_data
        ), false, true);
    }
    /** редактирование машины
     * @param integer $id ID номер машины, которую редактируем
     */
    public function actionCarEdit($id) {
        $model = Cars::model()->findByPk($id);
        $model->CarBrandID = $model->carModel->carBrand->ID;
        if(isset($_POST['ajax']) && $_POST['ajax']==='carForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Cars'])) {
            $data = array(
                array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 5 ),
                array( "name" => "CarID", "type" => "in", "value" => $id, "length" => 10 ),
                array( "name" => "ClientID", "type" => "in", "value" => $_POST['Cars']['ClientID'], "length" => 7 ),
                array( "name" => "CarModelID", "type" => "in", "value" => $_POST['Cars']['CarModelID'], "length" => 7 ),
            );
            $res_arr = $this->_storedProcedureCall("Cars_Update",$data);
            if( !$res_arr["result"] ) Yii::app()->user->setFlash('success',"<strong>Успех!</strong> Данные машины успешно обновлены");
            else Yii::app()->user->setFlash('error',"<strong>Ошибка!</strong> Произошла ошибка при обновлении данных машины");
            $this->redirect(Yii::app()->createAbsoluteUrl('admin/cars'));
            Yii::app()->end();

        }
        $all_brands_data = CHtml::listData(CarBrands::model()->findAll(array("order"=>"Name")), 'ID', 'Name');
        $curr_models_data = CHtml::listData(CarModels::model()->findAll("CarBrandID = :cbid",array(":cbid"=>$model->CarBrandID)), 'ID', 'Name');
        $this->renderPartial('_form_include',array(
            'name' => '_carform',
            'model' => $model,
            'all_brands_data' => $all_brands_data,
            'curr_models_data' => $curr_models_data
        ), false, true);
    }
    /** получение списка моделей машин по id марки
     * @param integer $id ID марки
     */
    function actionCarBrandChange($id) {
        echo json_encode(array("models"=>CHtml::listData(CarModels::model()->findAll("CarBrandID = :cbid",array(":cbid"=>$id)), 'ID', 'Name')));
        Yii::app()->end();
    }
}