<?php
return array (
  'PAD_LOGIN' => 
  array (
    'type' => 1,
    'description' => 'PAD_LOGIN',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'POSTS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'POSTS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'GOODS_MOVEMENT' => 
  array (
    'type' => 1,
    'description' => 'GOODS_MOVEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'GOODS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'GOODS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'ORDERS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'ORDERS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'CLAIMS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'CLAIMS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'ACTS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'ACTS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'BANKS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'BANKS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'CLIENTS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'CLIENTS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'CARDS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'CARDS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'CARS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'CARS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'WORKERS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'WORKERS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'SALARY_REPORT' => 
  array (
    'type' => 1,
    'description' => 'SALARY_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'CARSCAT_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'CARSCAT_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'SERVICES_GROUPS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'SERVICES_GROUPS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'SERVICES_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'SERVICES_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'PRICELIST_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'PRICELIST_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'ORDERS_REPORT' => 
  array (
    'type' => 1,
    'description' => 'ORDERS_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'FUELCARDS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'FUELCARDS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'TASKS_REPORT' => 
  array (
    'type' => 1,
    'description' => 'TASKS_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'DEBTORS_REPORT' => 
  array (
    'type' => 1,
    'description' => 'DEBTORS_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'INTERNET_LOGIN' => 
  array (
    'type' => 1,
    'description' => 'INTERNET_LOGIN',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'ROUNDINGDIFF_REPORT' => 
  array (
    'type' => 1,
    'description' => 'ROUNDINGDIFF_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'CASHIERS_REPORT' => 
  array (
    'type' => 1,
    'description' => 'CASHIERS_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'ADMIN_REPORT' => 
  array (
    'type' => 1,
    'description' => 'ADMIN_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'ACTS_REPORT' => 
  array (
    'type' => 1,
    'description' => 'ACTS_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'AVERAGECHECK_REPORT' => 
  array (
    'type' => 1,
    'description' => 'AVERAGECHECK_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'CARSCOUNT_REPORT' => 
  array (
    'type' => 1,
    'description' => 'CARSCOUNT_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'TURNOVER_REPORT' => 
  array (
    'type' => 1,
    'description' => 'TURNOVER_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'CARSTYPESMODELS_REPORT' => 
  array (
    'type' => 1,
    'description' => 'CARSTYPESMODELS_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'CONTACTLESSCARD_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'CONTACTLESSCARD_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'SPECIALCLIENT_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'SPECIALCLIENT_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'TURNOVERSALES_REPORT' => 
  array (
    'type' => 1,
    'description' => 'TURNOVERSALES_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'SPECIALCLIENT_REPORT' => 
  array (
    'type' => 1,
    'description' => 'SPECIALCLIENT_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'PARKING_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'PARKING_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'CELLS_MANAGER' => 
  array (
    'type' => 1,
    'description' => 'CELLS_MANAGER',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'PARKING_REPORT' => 
  array (
    'type' => 1,
    'description' => 'PARKING_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'PARKING_TURNOVER_SALES_REPORT' => 
  array (
    'type' => 1,
    'description' => 'PARKING_TURNOVER_SALES_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'PAYMENTS_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'PAYMENTS_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'PARKING_BEBEFITSCARS' => 
  array (
    'type' => 1,
    'description' => 'PARKING_BEBEFITSCARS',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'PARKING_PREPAIDCARS' => 
  array (
    'type' => 1,
    'description' => 'PARKING_PREPAIDCARS',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'UNIVERSALSUMMARY_REPORT' => 
  array (
    'type' => 1,
    'description' => 'UNIVERSALSUMMARY_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'UNIQUE_CAR_STAT_REPORT' => 
  array (
    'type' => 1,
    'description' => 'UNIQUE_CAR_STAT_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'ADMINISTRATOR_TASKS_REPORT' => 
  array (
    'type' => 1,
    'description' => 'ADMINISTRATOR_TASKS_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'SPECIAL_ORDERS_REPORT' => 
  array (
    'type' => 1,
    'description' => 'SPECIAL_ORDERS_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'SIMPLE_MANUAL_DISCOUNT_ORDERS_REPORT' => 
  array (
    'type' => 1,
    'description' => 'SIMPLE_MANUAL_DISCOUNT_ORDERS_REPORT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'STAFF_WORKPLACES_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'STAFF_WORKPLACES_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'STAFF_MANAGEMENT' => 
  array (
    'type' => 1,
    'description' => 'STAFF_MANAGEMENT',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'STAFF_VIEW' => 
  array (
    'type' => 1,
    'description' => 'STAFF_VIEW',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'CASHIER' => 
  array (
    'type' => 2,
    'description' => 'Кассир',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'ORDERS_MANAGEMENT',
      1 => 'ACTS_MANAGEMENT',
      2 => 'CLIENTS_MANAGEMENT',
      3 => 'CARS_MANAGEMENT',
      4 => 'ORDERS_REPORT',
      5 => 'DEBTORS_REPORT',
      6 => 'CONTACTLESSCARD_MANAGEMENT',
      7 => 'SPECIALCLIENT_MANAGEMENT',
      8 => 'UNIVERSALSUMMARY_REPORT',
    ),
  ),
  'TOPCLEANADMIN' => 
  array (
    'type' => 2,
    'description' => 'Администратор мойки',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'PAD_LOGIN',
      1 => 'POSTS_MANAGEMENT',
      2 => 'CLIENTS_MANAGEMENT',
      3 => 'CARDS_MANAGEMENT',
      4 => 'CARS_MANAGEMENT',
      5 => 'WORKERS_MANAGEMENT',
      6 => 'CELLS_MANAGER',
    ),
  ),
  'REPORTER' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'SALARY_REPORT',
      1 => 'ORDERS_REPORT',
      2 => 'TASKS_REPORT',
      3 => 'DEBTORS_REPORT',
      4 => 'INTERNET_LOGIN',
      5 => 'ROUNDINGDIFF_REPORT',
      6 => 'CASHIERS_REPORT',
      7 => 'ADMIN_REPORT',
      8 => 'ACTS_REPORT',
      9 => 'AVERAGECHECK_REPORT',
      10 => 'CARSCOUNT_REPORT',
      11 => 'TURNOVER_REPORT',
      12 => 'CARSTYPESMODELS_REPORT',
      13 => 'TURNOVERSALES_REPORT',
      14 => 'SPECIALCLIENT_REPORT',
      15 => 'PARKING_REPORT',
      16 => 'PARKING_TURNOVER_SALES_REPORT',
      17 => 'UNIVERSALSUMMARY_REPORT',
    ),
  ),
  'MANAGER' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'ORDERS_MANAGEMENT',
      1 => 'ACTS_MANAGEMENT',
      2 => 'BANKS_MANAGEMENT',
      3 => 'CLIENTS_MANAGEMENT',
      4 => 'CARDS_MANAGEMENT',
      5 => 'CARS_MANAGEMENT',
      6 => 'SALARY_REPORT',
      7 => 'CARSCAT_MANAGEMENT',
      8 => 'SERVICES_GROUPS_MANAGEMENT',
      9 => 'SERVICES_MANAGEMENT',
      10 => 'PRICELIST_MANAGEMENT',
      11 => 'ORDERS_REPORT',
      12 => 'FUELCARDS_MANAGEMENT',
      13 => 'TASKS_REPORT',
      14 => 'DEBTORS_REPORT',
      15 => 'ROUNDINGDIFF_REPORT',
      16 => 'CASHIERS_REPORT',
      17 => 'ADMIN_REPORT',
      18 => 'ACTS_REPORT',
      19 => 'AVERAGECHECK_REPORT',
      20 => 'CARSCOUNT_REPORT',
      21 => 'TURNOVER_REPORT',
      22 => 'CARSTYPESMODELS_REPORT',
      23 => 'CONTACTLESSCARD_MANAGEMENT',
      24 => 'SPECIALCLIENT_MANAGEMENT',
      25 => 'TURNOVERSALES_REPORT',
      26 => 'SPECIALCLIENT_REPORT',
      27 => 'PARKING_MANAGEMENT',
      28 => 'CELLS_MANAGER',
      29 => 'PARKING_REPORT',
      30 => 'PARKING_TURNOVER_SALES_REPORT',
      31 => 'PAYMENTS_MANAGEMENT',
    ),
  ),
  'ANALITIKA' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'POSTS_MANAGEMENT',
      1 => 'GOODS_MOVEMENT',
      2 => 'GOODS_MANAGEMENT',
      3 => 'ORDERS_MANAGEMENT',
      4 => 'CLAIMS_MANAGEMENT',
      5 => 'ACTS_MANAGEMENT',
      6 => 'BANKS_MANAGEMENT',
      7 => 'CLIENTS_MANAGEMENT',
      8 => 'CARDS_MANAGEMENT',
      9 => 'CARS_MANAGEMENT',
      10 => 'WORKERS_MANAGEMENT',
      11 => 'SALARY_REPORT',
      12 => 'CARSCAT_MANAGEMENT',
      13 => 'SERVICES_GROUPS_MANAGEMENT',
      14 => 'SERVICES_MANAGEMENT',
      15 => 'PRICELIST_MANAGEMENT',
      16 => 'ORDERS_REPORT',
      17 => 'FUELCARDS_MANAGEMENT',
      18 => 'TASKS_REPORT',
      19 => 'DEBTORS_REPORT',
      20 => 'ROUNDINGDIFF_REPORT',
      21 => 'CASHIERS_REPORT',
      22 => 'ADMIN_REPORT',
      23 => 'ACTS_REPORT',
      24 => 'AVERAGECHECK_REPORT',
      25 => 'CARSCOUNT_REPORT',
      26 => 'TURNOVER_REPORT',
      27 => 'CARSTYPESMODELS_REPORT',
      28 => 'CONTACTLESSCARD_MANAGEMENT',
      29 => 'SPECIALCLIENT_MANAGEMENT',
      30 => 'TURNOVERSALES_REPORT',
      31 => 'SPECIALCLIENT_REPORT',
      32 => 'PARKING_MANAGEMENT',
      33 => 'CELLS_MANAGER',
      34 => 'PARKING_REPORT',
      35 => 'PARKING_TURNOVER_SALES_REPORT',
      36 => 'PAYMENTS_MANAGEMENT',
      37 => 'PARKING_BEBEFITSCARS',
      38 => 'PARKING_PREPAIDCARS',
      39 => 'UNIVERSALSUMMARY_REPORT',
      40 => 'UNIQUE_CAR_STAT_REPORT',
      41 => 'ADMINISTRATOR_TASKS_REPORT',
    ),
  ),
  'TEST1' => 
  array (
    'type' => 2,
    'description' => 'test1',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'PAD_LOGIN',
      1 => 'POSTS_MANAGEMENT',
      2 => 'GOODS_MOVEMENT',
      3 => 'GOODS_MANAGEMENT',
      4 => 'ORDERS_MANAGEMENT',
      5 => 'CLAIMS_MANAGEMENT',
      6 => 'ACTS_MANAGEMENT',
      7 => 'BANKS_MANAGEMENT',
      8 => 'CLIENTS_MANAGEMENT',
      9 => 'CARDS_MANAGEMENT',
      10 => 'CARS_MANAGEMENT',
      11 => 'WORKERS_MANAGEMENT',
      12 => 'SALARY_REPORT',
      13 => 'CARSCAT_MANAGEMENT',
      14 => 'SERVICES_GROUPS_MANAGEMENT',
      15 => 'SERVICES_MANAGEMENT',
      16 => 'PRICELIST_MANAGEMENT',
      17 => 'ORDERS_REPORT',
      18 => 'FUELCARDS_MANAGEMENT',
      19 => 'TASKS_REPORT',
      20 => 'DEBTORS_REPORT',
      21 => 'INTERNET_LOGIN',
      22 => 'ROUNDINGDIFF_REPORT',
      23 => 'CASHIERS_REPORT',
      24 => 'ADMIN_REPORT',
      25 => 'ACTS_REPORT',
      26 => 'AVERAGECHECK_REPORT',
      27 => 'CARSCOUNT_REPORT',
      28 => 'TURNOVER_REPORT',
      29 => 'CARSTYPESMODELS_REPORT',
      30 => 'CONTACTLESSCARD_MANAGEMENT',
      31 => 'SPECIALCLIENT_MANAGEMENT',
      32 => 'TURNOVERSALES_REPORT',
      33 => 'SPECIALCLIENT_REPORT',
      34 => 'PARKING_MANAGEMENT',
      35 => 'CELLS_MANAGER',
      36 => 'PARKING_REPORT',
      37 => 'PARKING_TURNOVER_SALES_REPORT',
      38 => 'PAYMENTS_MANAGEMENT',
      39 => 'STAFF_WORKPLACES_MANAGEMENT',
      40 => 'STAFF_VIEW',
    ),
  ),
  'Administrator' => 
  array (
    'type' => 2,
    'description' => NULL,
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'CASHIER',
      1 => 'TOPCLEANADMIN',
      2 => 'REPORTER',
      3 => 'MANAGER',
      4 => 'ANALITIKA',
      5 => 'TEST1',
      6 => 'SPECIAL_ORDERS_REPORT',
      7 => 'SIMPLE_MANUAL_DISCOUNT_ORDERS_REPORT',
      8 => 'STAFF_MANAGEMENT',
    ),
  ),
);
