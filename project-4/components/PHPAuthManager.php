<?php

/**
 * @file класс расширяющий CPhpAuthManager, необходим для RBAC контроля
 * 
 */
class PHPAuthManager extends CPhpAuthManager{
    public function init(){
		//Загрузим из файла две роли по-умолчанию: guest и user, вторая наследуется от первого
        if( file_exists( Yii::getPathOfAlias('application.config.auth').'.php' ) ) {
			if( count($this->authItems) == 0 ) {
				$this->authFile=Yii::getPathOfAlias('application.config.auth').'.php';
				$this->load();
			}
		}
		else {
           $this->buildAuthTree();
		}	
		// Для гостей у нас и так роль по умолчанию guest.
		if(!Yii::app()->user->isGuest){
			// Связываем роль, заданную в БД с идентификатором пользователя,
			// возвращаемым UserIdentity.getId().
			$this->assign(Yii::app()->user->roleName, Yii::app()->user->id);
		}
	}
	public function buildAuthTree() {
		$this->authFile=Yii::getPathOfAlias('application.config.auth').'.php';
		$roles_arr = array(); //массив ключ - id роли, значение
		//Вытащим все роли из базы кроме админской (её мы создадим в конце отдельно)
		$roles_data = Roles::model()->findAll( array( 'select' => array('RoleId','RoleName','RoleDesc'), 'condition' => 'RoleId != 1') );
		$role_privs_arr = array();
		$not_assigned_privs = array(); //для тех привилегий, которые к ролям не привязаны
		$privs_data = Privileges::model()->findAll();
		if( $privs_data != null ) {
			foreach( $privs_data as $priv ) {
				$this->createAuthItem($priv->PrivilegeName,CAuthItem::TYPE_TASK,$priv->PrivilegeName);
				if( count( $priv->roles ) < 1 ) 
					$not_assigned_privs[] = $priv->PrivilegeName;
				else {
					foreach( $priv->roles as $role ) {
						$role_privs_arr[$role->RoleId][] = $priv->PrivilegeName;
					}
				}
			}
		
			foreach( $roles_data as $role ) {
				$roles_arr[$role->RoleId] = $role->RoleName;
				//Добавим каждую роль в менеджер
				$this->createAuthItem($role->RoleName,CAuthItem::TYPE_ROLE,$role->RoleDesc);
				//каждая роль наследуется от роли User
				if( isset( $role_privs_arr[$role->RoleId] ) ) {
					foreach( $role_privs_arr[$role->RoleId] as $priv_name ) {
						$this->addItemChild($role->RoleName,$priv_name);
					}
				}
			}
		}
		
		//Создадим наконец админскую роль, у неё будут дети все роли
		$role_admin_obj = Roles::model()->find( array( 'select' => array('RoleId','RoleName','RoleDesc'), 'condition' => 'RoleId = 1') );
		$this->createAuthItem($role_admin_obj->RoleName,CAuthItem::TYPE_ROLE,$role_admin_obj->RoleDesc);
		//echo $this->getAuthItem($role_admin_obj->role_name); die;
		foreach( $roles_arr as $role ) {
			$this->addItemChild($role_admin_obj->RoleName,$role);
		}
		//привяжем к админской роли непривязанные привилегии, если таковые есть
		if( count( $not_assigned_privs ) > 0 ) {
			foreach( $not_assigned_privs as $priv ) {
				$this->addItemChild($role_admin_obj->RoleName,$priv);
			}			
		}
		$this->save();
	}
}
