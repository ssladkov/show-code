<?php

class DefaultController extends CController
{
    private function _IPInRange( $ips, $ip ) {
        $ips_arr = explode( "-", $ips );
        $ip_num_min = ip2long( $ips_arr[0] );
        $ip_num_max =  ip2long( $ips_arr[1] );
        $ip_num_checking = ip2long( $ip );
        $result = $ip_num_checking >= $ip_num_min && $ip_num_checking <= $ip_num_max ? true : false;
        return $result;
    }
    public function actionIndex($id=null)
    {
        $config = $this->getModule()->config;
        $globalAccessRules = isset(Yii::app()->params["reports_global_access"]) ? Yii::app()->params["reports_global_access"] : false;

        /** Проверки, указан ли в конфиге id и есть ли у нас права на построение отчетов */
        if(!isset($config[$id]))
            throw new CHttpException(404);
        if( $globalAccessRules ) {
            $access = false;
            if( isset($globalAccessRules['priv']) && Yii::app()->user->checkAccess($globalAccessRules['priv']) )
                $access = true;
            if( isset($globalAccessRules['ipRange']) && $this->_IPInRange($globalAccessRules['ipRange'], $_SERVER["REMOTE_ADDR"] ) )
                $access = true;

            if(!$access)
                throw new CHttpException(403);
        }
        if(!Yii::app()->user->checkAccess($config[$id]["access"]))
            throw new CHttpException(403);

        /** @var  $model создаем объект, spName название хранимки, без него будет эксепшн, также при инициализации создается уникальный ключ для кеша */
        $model = new Report($config[$id]["spName"]);
        /** @var  $viewName имя вьюхи по-умолчанию для фильтра и вывода результатов */
        $viewName = "index";
        /** @var  $wFilter флаг строить ли фильтр для отчета */
        $wFilter = isset($config[$id]["withFilter"]) ? $config[$id]["withFilter"] : true;

        /** Чистим весь кеш */
        if( isset($_GET['clearcache']) ) {
            $model->clearAllCache();
            die('Report cache has cleared');
        }

        /** Валидация формы фильтра (пока активно не используется) */
        if(isset($_POST['ajax']) && $_POST['ajax']==='reportFilter') {
            /** Соберем поля для модели из кеша */
            $model->initFilterFields();
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		
		$publisher = Yii::app()->assetManager;
        $assets = $publisher->publish(GLOBAL_EXT_DIR.'/YiiOldPackage/modules/ReportSystem/assets');
		$registry = Yii::app()->clientScript;
		$registry->registerScriptFile("{$assets}/js/common.js", CClientScript::POS_END);
		
        /** Сюда мы попадаем после сабмита формы фильтра */
        if( isset($_POST["Report"]["values"]) ) {
			/** Соберем поля для модели из кеша */
            $model->initFilterFields();
            /** Утсновим выбранные значения фильтра соответствующим аттрибутам модели и положим их в кеш */
            $model->setFilterValues($_POST["Report"]["values"]);
            /** Данные самого отчета сохраняются в кеше для экспорта, но здесь нам их надо почистить */
            $model->clearDataCache();
            /** Генерируем отчет с учетом фильтра и заносим эти данные в кеш */
            $model->generateReport();
        }
        elseif( isset($_POST["Report"]["export"]) && isset($config[$id]["export"])) {
            if( $wFilter ) {
                /** Соберем поля для модели из кеша */
                $model->initFilterFields();
                /** Если используется фильтр, берем его значения из кеша (чтобы вывести в отчете) */
                $model->setFilterValues();
                /** И строим отчет, данные при этом беруется также из кеша (для единообразия) */
                $model->generateReport();
            }
            else {
                /** Если отчет без фильтра, строим его с данными из кеша, которые были получены ранее */
                $model->generateReportWithoutFilter();
            }

            if( $_POST["Report"]["export"] == "xls" && in_array("xls",$config[$id]["export"]))
                $type = "xls";
            elseif( $_POST["Report"]["export"] == "print" && in_array("print",$config[$id]["export"]))
                $type = "print";
            else
                $type = $config[$id]["export"][0];

            switch($type) {
                case "xls"  : $viewName = "exportxls"; break;
                case "print": $viewName = "print"; break;
            }
        }
        /** Первая загрузка страницы */
        else {
            if( $wFilter ) {
                /** Если фильтр предусмотрен для данного отчета, мы сгенерируем для него поля, которые вернет хранимка и положим их в кеш */
                $model->initFilterFields();
            }
            else {
                /** Если фильтр не предусмотрен, мы сразу запустим хранимку на построение отчета без него, предварительно почистив старые данные */
                $model->clearDataCache();
                $model->generateReportWithoutFilter();
            }
        }

        if( $viewName == "print") {
            $this->renderPartial($viewName, array(
                'id' => $id,
                'moduleConfig' => $config[$id],
                'model' => $model,
            ), false, true);
        }
        else {
            $this->render($viewName, array(
                'id' => $id,
                'moduleConfig' => $config[$id],
                'model' => $model,
            ));
        }
    }
}