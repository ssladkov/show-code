<?php
class DateTimePicker extends CInputWidget
{
    /**
     * @var TbActiveForm when created via TbActiveForm.
     * This attribute is set to the form that renders the widget
     * @see TbActionForm->inputRow
     */
    public $form;

    /**
     * @var array the options for the Bootstrap JavaScript plugin.
     */
    public $options = array();

    /**
     * @var string[] the JavaScript event handlers.
     */
    public $events = array();

    public $append;
    /**
     *### .init()
     *
     * Initializes the widget.
     */
    public function init()
    {
        $this->htmlOptions['type'] = 'text';
        $this->htmlOptions['autocomplete'] = 'off';

        /*if (!isset($this->options['language']))
            $this->options['language'] = substr(Yii::app()->getLanguage(), 0, 2);

        if (!isset($this->options['format']))
            $this->options['format'] = 'mm/dd/yyyy';

        if (!isset($this->options['weekStart']))
            $this->options['weekStart'] = 0; // Sunday*/
    }

    /**
     *### .run()
     *
     * Runs the widget.
     */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

       /* if ($this->hasModel())
        {
            if ($this->form)
                echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
            else
                echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);

        } else
            echo CHtml::textField($name, $this->value, $this->htmlOptions);*/

        echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions).
            ($this->append?'<span class="add-on">'.$this->append.'</span>':'');

        $this->registerAssets();

        $options = !empty($this->options) ? CJavaScript::encode($this->options) : '';

        ob_start();
        echo "jQuery('#{$id}').datetimepicker({$options})";
        /*foreach ($this->events as $event => $handler)
            echo ".on('{$event}', " . CJavaScript::encode($handler) . ")";*/

        Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $this->getId(), ob_get_clean() . ';');

    }

    public function registerAssets()
    {
		if( !defined('GLOBAL_EXT_DIR') )
			define( 'GLOBAL_EXT_DIR',( getenv('YII_EXT_PATH') ? getenv('YII_EXT_PATH') : dirname(__FILE__) . '/../extensions' ) );
        $publisher = Yii::app()->assetManager;
        //$assets = $publisher->publish(Yii::app()->modulePath.'/ReportSystem/assets');
        $assets = $publisher->publish(GLOBAL_EXT_DIR.'/YiiOldPackage/modules/ReportSystem/assets');

        $registry = Yii::app()->clientScript;
        $registry
            ->registerCssFile("{$assets}/css/jquery.datetimepicker.css")
            ->registerCssFile("{$assets}/css/style.css")
            ->registerScriptFile("{$assets}/js/jquery.datetimepicker.js", CClientScript::POS_END);
    }

    /*public function registerLanguageScript()
    {
        if (isset($this->options['language']) && $this->options['language'] != 'en')
        {
            $file = 'locales/bootstrap-datepicker.'.$this->options['language'].'.js';
            if (@file_exists(Yii::getPathOfAlias('bootstrap.assets').'/js/'.$file))
                Yii::app()->bootstrap->registerAssetJs('locales/bootstrap-datepicker.'.$this->options['language'].'.js', CClientScript::POS_END);
        }
    }*/
}
