<?php
/**
 * Report
 */

class Report extends CFormModel {

    public $values = array();
    public $dataXML;

    private $_labels = array();
    private $_rules = array();
    private $_fieldsData = array();
    private $_tableRowsFormat = array();
    private $_cacheId;

    public function rules() {
        return array(
            array('values', 'validateValues'),
        );
    }

    public function validateValues($attribute,$params) {
		foreach($this->values as $key => $val){
            if (empty($val) && isset($this->_rules[$key]) && in_array("requried",$this->_rules[$key])) {
                $this->addError("values[$key]", "Поле должно быть заполнено");
            }
            if( !empty($val) && isset($this->_rules[$key]) && in_array("numerical",$this->_rules[$key]) && !is_numeric($val) )
                $this->addError("values[$key]", "Допускаются только цифровые значения");
            if( !empty($val) && isset($this->_rules[$key]) && in_array("int",$this->_rules[$key]) && round($val) != $val )
                $this->addError("values[$key]", "Допускаются только целочисленные значения");
        }
    }

    public function attributeLabels() {
        return $this->_labels;
    }

    public function init() {
        if( !$this->scenario ) throw new CHttpException(400,"Report: scenario can't be empty");
        $this->_setCacheId(); //составление ID для кеша
    }

    public function initFilterFields() {
        $this->_setFieldsData(); //вытаскиваем из XML хранимой процедуры все данные по полям, сохраняем в массиве $this->_fieldsData
        $this->_setModelData(); //формируем из $this->_fieldsData данные для модели: $this->values (поля со значениями), $this->_labels (названия полей) и $this->_rules (правила)
    }

    private function _setCacheId() {
        $this->_cacheId = "Report_".$this->scenario."_".Yii::app()->user->id;
    }

    private function _setFieldsData() {
        $filterFieldsXMLStr = $this->_getFilterFieldsXML();
        $simpleXML = simplexml_load_string($filterFieldsXMLStr);
        foreach( $simpleXML->param as $node ) {
            $tmpArr = (array) $node->attributes();
            $this->_fieldsData[str_replace("@","",$node["id"])] = $tmpArr["@attributes"];
        }
        if( count($this->_fieldsData) == 0 )
            throw new CHttpException(500,"FieldsData: no fields data was got (or couldn't parse) from stored procedure");
    }

    private function _setModelData() {
        foreach($this->_fieldsData as $id => $field ) {

            if( $field["type"] == "date" ) {
                $defaultValue = date( (isset($field["dateFormat"])?$field["dateFormat"]:"d.m.Y H:i"), (isset($field["default"])?strtotime($field["default"]):strtotime("now")));
            }
            else {
                $defaultValue = isset($field["default"]) ? $field["default"] : "";
            }
            $this->values[$id] = $defaultValue;
            $this->_labels["values[$id]"] = isset($field["label"]) ? $field["label"] : $id;

            //описываем правила
            if( isset($field["needle"]) && (bool) $field["needle"] )
                $this->_rules[$id][] = 'requried';
            if( $field["type"] == "number" )
                $this->_rules[$id][] = 'numerical';
            if( isset($field["format"]) && $field["format"] == "int" )
                $this->_rules[$id][] = 'int';
        }
    }

    private function _getFilterFieldsXML() {
        $cacheKey = $this->_cacheId."_fieldsDataXML";
        //Yii::app()->cache->delete($cacheKey);
        $cacheData = Yii::app()->cache->get($cacheKey);
        if( $cacheData === false ) {
            $cacheData = $this->_storedProcedureCall();
            Yii::app()->cache->set($cacheKey, $cacheData);
        }
        return $cacheData;
    }

    private function _storedProcedureCall($type="fields") {
        $spName = $this->scenario;
        $timezone = new DateTimeZone("UTC");
        $return_value;
        if( $type == "fields" ) {
            $command = Yii::app()->db->createCommand("EXEC :return_value = [$spName] @RequestSettings = 1 ");
            $command->prepare();
            $command->bindParam(':return_value', $return_value, PDO::PARAM_INT|PDO::PARAM_INPUT_OUTPUT, 1);
            $data = $command->queryColumn();
            $command->reset();
        }
        elseif( $type == "data" ) {
            $procParams = array();
            foreach($this->_fieldsData as $id => $field ) {
                $procParams[] = "@$id = :$id";
            }
            $command = Yii::app()->db->createCommand("EXEC :return_value = [$spName] @RequestSettings = 0 ".(count($procParams)>0?",".implode(",",$procParams):""));
            $command->prepare();
            $command->bindParam(':return_value', $return_value, PDO::PARAM_INT|PDO::PARAM_INPUT_OUTPUT, 1);
            //$tmp = "";

			foreach($this->_fieldsData as $id => $field ) {
                if( $field["type"] == "date" ) {
                    $tmpDate = new DateTime($this->values[$id], $timezone);
                    $value[$id] = $tmpDate->format("Y-m-d\TH:i:s"); //такой формат понимает бд
                }
                else
                    $value[$id] = $this->values[$id];
                $value[$id] = !isset($value[$id]) || $value[$id] == '' ? null : $value[$id];
				$command->bindParam(":$id", $value[$id], null, 10);
            }
            $data = $command->queryColumn();
            $command->reset();
        }
        //print_r($data);
		//die;
		return implode("",$data);
    }

    public function getFilterFieldsData() {
        return $this->_fieldsData;
    }

    public function clearFilterCache() {
        $filterCacheKey = $this->_cacheId."_fieldsDataXML";
        Yii::app()->cache->delete($filterCacheKey);
    }

    public function clearFilterValuesCache() {
        $filterCacheKey = $this->_cacheId."_fieldsValuesXML";
        Yii::app()->cache->delete($filterCacheKey);
    }

    public function clearDataCache() {
        $dataCacheKey = $this->_cacheId."_DataXML";
        Yii::app()->cache->delete($dataCacheKey);
    }

    public function clearAllCache() {
        $this->clearFilterCache();
        $this->clearFilterValuesCache();
        $this->clearDataCache();
    }

    public function setFilterValues($data=false) {
        $cacheKey = $this->_cacheId."_fieldsValuesXML";
        if(!$data) {
            $cacheData = Yii::app()->cache->get($cacheKey);
            if( $cacheData === false )
                throw new CHttpException(500, "Report: Filter values cache is empty");
            $data = $cacheData;
        }
        else {
            $this->clearFilterValuesCache();
            Yii::app()->cache->set($cacheKey, $data);
        }

        foreach($data as $key => $val ) {
            if( isset($this->values[$key]) )
                $this->values[$key] = $val;
        }
    }

    public function generateReport() {
        $cacheKey = $this->_cacheId."_DataXML";
        $cacheData = Yii::app()->cache->get($cacheKey);
        if( $cacheData === false ) {
            $cacheData = $this->_storedProcedureCall("data");
            Yii::app()->cache->set($cacheKey, $cacheData);
        }
        $this->dataXML = simplexml_load_string("<r>".$cacheData."</r>");
        /*echo $cacheData;
        die;*/
        /*$json = json_encode($this->dataXML);
        $array = json_decode($json,TRUE);
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        die;*/
    }

    public function generateReportWithoutFilter() {
        $cacheKey = $this->_cacheId."_DataXML";
        $cacheData = Yii::app()->cache->get($cacheKey);
        if( $cacheData === false ) {
            $cacheData = $this->_storedProcedureCall("data");
            Yii::app()->cache->set($cacheKey, $cacheData);
        }
        $this->dataXML = simplexml_load_string("<r>".$cacheData."</r>");
    }

    public function setRowFormat($rowKey, $type, $format) {
        $this->_tableRowsFormat[$rowKey] = array( "type" => $type, "format" => $format);
    }

    public function formatData($rowKey, $value, $wTags=true) {
        $formattedValue = $value;
        if( isset($this->_tableRowsFormat[$rowKey]) && $value != "" && $value != " " && $value != "&#x20;") {
            $rowFormat = $this->_tableRowsFormat[$rowKey];
            if( $rowFormat["type"] == "date" )
                $formattedValue = date( (isset($rowFormat["format"])?$rowFormat["format"]:"d.m.Y H:i"), strtotime($value) );
            elseif( $rowFormat["type"] == "number" ) {
				$preFormatted = !isset( $rowFormat["format"] ) || $rowFormat["format"] != "int" ? number_format((float) $value, 2, '.', ' ') : $value;
				$formattedValue = $wTags ? '<span class="pull-right">'.$preFormatted.'</span>' : $value;
            }
        }
        return $formattedValue;
    }
}
