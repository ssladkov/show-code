<?php
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.date("Y-m-d").'-'.$id.'-report.xlsx"');
header('Cache-Control: max-age=0');

$objPHPExcel = new PHPExcel();

$workSheet = $objPHPExcel->setActiveSheetIndex(0);

$workSheet->setCellValue('A1', $moduleConfig['title']);
$rCount = 2;

foreach( $model->FilterFieldsData as $key => $val ) {
    $workSheet->setCellValue("A$rCount",$model->getAttributeLabel("values[$key]"));
    if( $val["type"] == "list" ) {
        $listModel = CActiveRecord::model($val["listTable"])->findByPk($model->values[$key]);
        $value = $listModel?$listModel->$val["listValue"]:$val['empty'];
    }
    elseif( $val["type"] == "bool" ) {
        $value = $model->values[$key] ? "да" : "нет";
    }
    else
        $value = $model->values[$key];
    $workSheet->setCellValue("B$rCount",$value);
    $rCount++;
}

$rCount++;

/** выводим шапочку */
$cCount = 'A';
foreach($model->dataXML->head->item as $val) {
    $model->setRowFormat($cCount, $val["type"], $val["format"]);
    $workSheet->setCellValue(($cCount.$rCount),$val["label"]);
    $cCount++;
}

$rCount++;

/** выводим summary */
if($model->dataXML->summary) {
	$cCount = 'A';
	foreach($model->dataXML->summary->value as $val) {
		$workSheet->setCellValue(($cCount.$rCount),$val);
		$cCount++;
	}
}

$rCount++;

/** выводим данные */
foreach($model->dataXML->data->row as $row) {
    $cCount = 'A';
    foreach($row->value as $val) {
        $workSheet->setCellValue(($cCount.$rCount),$model->formatData($cCount,$val,false));
        $cCount++;
    }
    $rCount++;
}

/** выводим summary */
if($model->dataXML->summary) {
	$cCount = 'A';
	foreach($model->dataXML->summary->value as $val) {
		$workSheet->setCellValue(($cCount.$rCount),$val);
		$cCount++;
	}
}

$objPHPExcel->setActiveSheetIndex(0);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;


