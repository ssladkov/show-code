<!doctype html>
<html itemscope="" itemtype="http://schema.org/WebPage" lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>


    <title><?php echo $this->pageTitle=Yii::app()->name ." - ".$moduleConfig['title'] ?></title>
</head>

<body>
<div class="container" id="page">
    <h3><?=$moduleConfig['title'];?></h3>

    <?php if( $model->FilterFieldsData ) : ?>
    <div class="well" style="margin-bottom:10px;">
        <div>
            <?php
            foreach( $model->FilterFieldsData as $key => $val ) {
                echo $model->getAttributeLabel("values[$key]");
                echo ":&nbsp;&nbsp;";
                if( $val["type"] == "list" ) {
                    $listModel = null;
                    if ($model->values[$key] != '')
                        $listModel = CActiveRecord::model($val["listTable"])->findByPk($model->values[$key]);
                    $value = $listModel?$listModel->$val["listValue"]:$val['empty'];
                }
                elseif( $val["type"] == "bool" ) {
                    $value = $model->values[$key] ? "да" : "нет";
                }
                else
                    $value = $model->values[$key];
                echo "<b>$value</b>";
                echo "<br/>";
            }
            ?>
        </div>
    </div>
    <?php endif; ?>

    <table class="items table table-bordered table-striped" id="data-table">
        <tr>
            <?php
            $i=0;
            foreach($model->dataXML->head->item as $val) {
                $model->setRowFormat($i, $val["type"], $val["format"]);
                echo "<th>".$val["label"]."</th>";
                $i++;
            }
            ?>
        </tr>
        <?php if($model->dataXML->data->row): ?>
            <tr class="bold">
                <?php /** выводим summary */
                if($model->dataXML->summary) {
					$i = 0;
					foreach($model->dataXML->summary->value as $val) {
						echo "<td>".$model->formatData($i,$val)."</td>";
						$i++;
					}
				}
                ?>
            </tr>
            <?php /** выводим данные */
            $i = 0;
            foreach($model->dataXML->data->row as $row) {
                echo "<tr>";
                $i = 0;
                foreach($row->value as $val) {
                    echo "<td>".$model->formatData($i,$val)."</td>";
                    $i++;
                }
                echo "</tr>";
            }
            ?>
            <tr class="bold">             
			   <?php /** выводим summary */
                if($model->dataXML->summary) {
					$i = 0;
					foreach($model->dataXML->summary->value as $val) {
						echo "<td>".$model->formatData($i,$val)."</td>";
						$i++;
					}
                }
                ?>
            </tr>
        <?php else: ?>
            <tr>
                <td colspan="<?=(int)$i;?>"><i>Нет результатов</i></td>
            </tr>
        <?php endif; ?>
    </table>
</div>
<script>
	$(document).ready( function() {
		mergeRows( 'data-table' );
		window.print();
	});
</script>
</body>
</html>