<?php
/* @var $this DefaultController */

$this->pageTitle=Yii::app()->name ." - ".$moduleConfig['title'];
?>

<h3><?=$moduleConfig['title'];?></h3>

<?php if( $model->FilterFieldsData ) : ?>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'reportFilter',
        'type'=>'horizontal',
        'htmlOptions' => array('class' => 'well'),
        'enableAjaxValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        )
    ));
	echo "<table>";
    foreach( $model->FilterFieldsData as $key => $val ) {
        echo "<tr>";
		$needle = isset($val["needle"]) ? (bool) $val["needle"] : false;
		echo "<td>".$form->labelEx($model,"values[$key]")."</td>";
		//echo "&nbsp;&nbsp;";
        if( $val["type"] == "list" ) {
            $emptyArr =  !$needle ? array('empty' =>(isset($val["empty"])?$val["empty"]:'-всё-')) : array();
			echo "<td>".$form->dropDownList($model,"values[$key]", CHtml::listData(CActiveRecord::model($val["listTable"])->findAll(array("order"=>(isset($val["listOrder"])?$val["listOrder"]:$val["listValue"]))),$val["listKey"],$val["listValue"]),$emptyArr)."</td>";
        }
        elseif( $val["type"] == "date" ) {
            $withTime = isset($val["withTime"]) ? (bool) $val["withTime"] : false;
            $format = isset($val["dateFormat"]) ? $val["dateFormat"] : "d.m.Y H:i";
            $maxDate =  isset($val["maxDate"]) ? date("Y/m/d", strtotime($val["maxDate"]) ) : date("Y/m/d", strtotime((isset($val["default"])?$val["default"]:"now")) );
			echo '<td><div class="input-append">';
            $this->widget('ReportSystem.widgets.DateTimePicker', array(
                'model' => $model,
                'attribute' => "values[$key]",
                'append' => '<i class="icon-calendar from"></i>',
				'htmlOptions' => array("style" => "width:93%"),
                'options' => array(
                    'format' => $format,
                    'timepicker' => $withTime,
                    'lang' => 'ru',
                    'todayButton' => false,
                    'dayOfWeekStart' => 1,
                    'value' => $model->values[$key],
                    'maxDate' => $maxDate
                )
            ));
            echo '</div></td>';
        }
		elseif( $val["type"] == "bool" ) {
			echo "<td>";
			$this->widget(
				'bootstrap.widgets.TbToggleButton',
				array(
					'model' => $model,
					'attribute' => "values[$key]",
					'enabledLabel' => 'ДА',
					'disabledLabel' => 'НЕТ',
				)
			);
			echo "</td>";
		}
        else {
            echo "<td>".$form->textField($model, "values[$key]")."</td>";
        }
		//echo "&nbsp;&nbsp;";
        echo "<td>".$form->error($model,"values[$key]")."</td>";
		echo "</tr>";
    }

	echo "<tr><td></td><td>";
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'label'=>'Применить',
        'htmlOptions' => (isset($moduleConfig["filterOptions"]["submitClass"])?array('class' => $moduleConfig["filterOptions"]["submitClass"]):array()),
    ));
	echo "</td><td></td></tr></table>";
    echo "</div><div class='clearfix'></div>";
    $this->endWidget(); ?>
<?php endif; ?>

<?php if($model->dataXML) : ?>
    <?php if( isset($moduleConfig["export"]) && $model->dataXML->data->row): ?>
        <div class="pull-right">
            <form method="POST" target="_blank" id="exportForm">
                <input type="hidden" name="Report[export]" value="" id="type"/>
                <?php if( in_array('xls',$moduleConfig["export"])): ?>
                    <a class="btn btn-success" href="javascript:;void(0)" onClick="$('#type').val('xls');$('#exportForm').submit();"><i class="icon-list-alt icon-white"></i> Экспорт в Excel</a>
                <?php endif; ?>
                &nbsp;&nbsp;&nbsp;
                <?php if( in_array('print',$moduleConfig["export"])): ?>
                    <a class="btn btn-info" href="javascript:;void(0)" onClick="$('#type').val('print');$('#exportForm').submit();"><i class="icon-print icon-white"></i> Распечатать</a>
                <?php endif; ?>
            </form>
        </div>
        <div class='clearfix'></div><br/>
    <?php endif; ?>
	<?php
	/** Если надо сделать таблицу во всю ширину, делаем такой хитрый трюк */
	if( isset($moduleConfig['wide']) && $moduleConfig['wide']) 
		echo "</div>"; ?>
		
    <table class="table table-bordered" id="data-table" style="overflow-x: auto">
        <tr class="bold info">
            <?php /** выводим шапку */
            $i = 0;
            foreach($model->dataXML->head->item as $val) {
                $model->setRowFormat($i, $val["type"], $val["format"]);
                echo "<td>".$val["label"]."</td>";
                $i++;
            }
            ?>
        </tr>
        <?php if($model->dataXML->data->row): ?>
            <tr class="bold success">
                <?php /** выводим summary */
                if($model->dataXML->summary) {
                    $i = 0;
                    foreach($model->dataXML->summary->value as $val) {
                        echo "<td nowrap='nowrap'>".$model->formatData($i,$val)."</td>";
                        $i++;
                    }
                }
                ?>
            </tr>
            <?php /** выводим данные */
            $i = 0;
            foreach($model->dataXML->data->row as $row) {
                echo "<tr>";
                $i = 0;
                foreach($row->value as $val) {
                    echo "<td>".$model->formatData($i,$val)."</td>";
                    $i++;
                }
                echo "</tr>";
            }
            ?>
            <tr class="bold success">
                <?php /** выводим summary */
                if($model->dataXML->summary) {
                    $i = 0;
                    foreach($model->dataXML->summary->value as $val) {
                        echo "<td>".$model->formatData($i,$val)."</td>";
                        $i++;
                    }
                }
                ?>
            </tr>
        <?php else: ?>
			<tr class="bold success">
                <?php /** выводим summary */
                if($model->dataXML->summary) {
                    $i = 0;
                    foreach($model->dataXML->summary->value as $val) {
                        echo "<td>".$model->formatData($i,$val)."</td>";
                        $i++;
                    }
                }
                ?>
            </tr>
			<tr>
                <td colspan="<?=(int)$i;?>"><i>Нет результатов</i></td>
            </tr>
			<tr class="bold success">
                <?php /** выводим summary */
                if($model->dataXML->summary) {
                    $i = 0;
                    foreach($model->dataXML->summary->value as $val) {
                        echo "<td>".$model->formatData($i,$val)."</td>";
                        $i++;
                    }
                }
                ?>
            </tr>
        <?php endif; ?>
    </table>
    <?php if( isset($moduleConfig["export"]) && $model->dataXML->data->row): ?>
        <div class="pull-right">
            <form method="POST" target="_blank" id="exportForm">
                <input type="hidden" name="Report[export]" value="" id="type"/>
                <?php if( in_array('xls',$moduleConfig["export"])): ?>
                    <a class="btn btn-success" href="javascript:;void(0)" onClick="$('#type').val('xls');$('#exportForm').submit();"><i class="icon-list-alt icon-white"></i> Экспорт в Excel</a>
                <?php endif; ?>
                &nbsp;&nbsp;&nbsp;
                <?php if( in_array('print',$moduleConfig["export"])): ?>
                    <a class="btn btn-info" href="javascript:;void(0)" onClick="$('#type').val('print');$('#exportForm').submit();"><i class="icon-print icon-white"></i> Распечатать</a>
                <?php endif; ?>
            </form>
        </div>
        <div class="clearfix"></div>
    <?php endif; ?>
	<script>
	$(document).ready( function() {
		mergeRows( 'data-table' );
	});
	</script>
<?php endif; ?>