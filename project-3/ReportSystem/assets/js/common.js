function mergeRows( tableID ) {
	var tObj = $("#"+tableID);
	
	if( tObj.length < 1 ) return false;
	
	var first_instances = new Array();
	var rowsCount = 0;
	
	$(tObj).find("tr").each(function () {
		var colsCount = 0;
		$(this).find('td:first').each(function () {

			if (typeof first_instances[colsCount] == "undefined" ) {
				first_instances[colsCount] = $(this);
			} else if (first_instances[colsCount].text() == $(this).text()) {
				$(this).remove();
				first_instances[colsCount].attr('rowspan', parseInt((first_instances[colsCount].attr('rowspan')?first_instances[colsCount].attr('rowspan'):1)) + 1);

			} else {
				first_instances[colsCount] = $(this);
			}
			colsCount++;
		});
		rowsCount++;
	});
}