<?php

// this contains the application parameters that can be maintained via GUI
return array(
    'main_layout' => 'main',
    'main_rules' => array(
        array('allow', 'roles' => array('Administrator') ),
        array('allow', 'actions'=>array('index','account'), 'users'=>array('@') ),
        array('allow', 'actions'=>array('parkingCalculate','account'), 'users'=>array('*') ),
        array('allow', 'actions'=>array('login'), 'users'=>array('?') ),
        array('allow', 'actions'=>array('logout'), 'users'=>array('@') ),
        array('allow', 'actions'=>array('acts','actcreate','carbrandchange','actedit','actcars','actcarscreate','actcardelete_ajax','ActCarsCheck'), 'roles'=>array('ACTS_MANAGEMENT') ),
        array('allow', 'actions'=>array('SpecialClients','SpecialClientCreate','carbrandchange','SpecialClientEdit','SpecialClientCars','SpecialClientCarsCreate','SpecialClientCarDelete_ajax','SpecialClientCarsCheck'), 'roles'=>array('SPECIALCLIENT_MANAGEMENT') ),
        array('allow', 'actions'=>array('banks','bankcreate','bankedit','bankdeleteajax'), 'roles'=>array('BANKS_MANAGEMENT') ),
        array('allow', 'actions'=>array('fuelcards','fuelcardcreate','fuelcardedit','fuelcarddeleteajax'), 'roles'=>array('FUELCARDS_MANAGEMENT') ),
        array('allow', 'actions'=>array('Clients','ClientCreate','ClientEdit','carbrandchange'), 'roles'=>array('CLIENTS_MANAGEMENT') ),
        array('allow', 'actions'=>array('Cards','CardCreate','CardEdit','carbrandchange','GetClientsByTerm_ajax'), 'roles'=>array('CARDS_MANAGEMENT') ),
        array('allow', 'actions'=>array('Cars','CarCreate','CarEdit','CarBrands','CarBrandAdd','CarBrandEdit','CarBrandLogo','CarModels','CarModelAdd','CarModelEdit','CarCheck','GetCarsByTerm_ajax'), 'roles'=>array('CARS_MANAGEMENT') ),
        array('allow', 'actions'=>array('Goods','GoodsAdd','GoodsEdit'), 'roles'=>array('GOODS_MANAGEMENT') ),
        array('allow', 'actions'=>array('GoodsMove','GoodsLog'), 'roles'=>array('GOODS_MOVEMENT') ),
        array('allow', 'actions'=>array('Orders','AddFromShop','OrderComments','Tasks','PayOrder','PayTypesReport','SoldGoodsReport','SearchOrderByCardNum','CheckPrint', 'SearchContactlessCard', 'ReleaseCell', 'ContactLessCards'), 'roles'=>array('ORDERS_MANAGEMENT') ),
        array('allow', 'actions'=>array('Claims','ClaimCreate','ClaimEdit'), 'roles'=>array('CLAIMS_MANAGEMENT') ),
        array('allow', 'actions'=>array('CarTypes','CarTypeAdd','CarTypeEdit'), 'roles'=>array('CARSCAT_MANAGEMENT') ),
        array('allow', 'actions'=>array('ServiceGroups','ServiceGroupAdd','ServiceGroupEdit'), 'roles'=>array('SERVICES_GROUPS_MANAGEMENT') ),
        array('allow', 'actions'=>array('Services','ServiceAdd','ServiceEdit'), 'roles'=>array('SERVICES_MANAGEMENT') ),
        array('allow', 'actions'=>array('PriceList','PriceAdd','PriceEdit'), 'roles'=>array('PRICELIST_MANAGEMENT') ),
        array('allow', 'actions'=>array('PostsList','PostAdd','PostEdit'), 'roles'=>array('POSTS_MANAGEMENT') ),
        array('allow', 'actions'=>array('WorkersList','WorkerAdd','WorkerEdit'), 'roles'=>array('WORKERS_MANAGEMENT') ),
        array('allow', 'actions'=>array('WorkersSalary'), 'roles'=>array('SALARY_REPORT') ),
        array('allow', 'actions'=>array('OrdersReport', 'OrderComments', 'ReportOrder', 'CalculateOrdersReportSumm','CheckPrint'), 'roles'=>array('ORDERS_REPORT') ),
        array('allow', 'actions'=>array('TasksReport'), 'roles'=>array('TASKS_REPORT') ),
        array('allow', 'actions'=>array('CashiersReport'), 'roles'=>array('CASHIERS_REPORT') ),
        array('allow', 'actions'=>array('AdminsReport'), 'roles'=>array('ADMIN_REPORT') ),
        array('allow', 'actions'=>array('ActsReport'), 'roles'=>array('ACTS_REPORT') ),
        array('allow', 'actions'=>array('SpecialClientsReport'), 'roles'=>array('SPECIALCLIENT_REPORT') ),
        array('allow', 'actions'=>array('AverageCheckReport'), 'roles'=>array('AVERAGECHECK_REPORT') ),
        array('allow', 'actions'=>array('Debtors','AddFromShop','OrderComments','Tasks','PayOrder','PayTypesReport','SoldGoodsReport','SearchOrderByCardNum','CheckPrint', 'SearchContactlessCard', 'ReleaseCell', 'ContactLessCards'), 'roles'=>array('DEBTORS_REPORT') ),
        array('allow', 'actions'=>array('CarsCountReport'), 'roles'=>array('CARSCOUNT_REPORT') ),
        array('allow', 'actions'=>array('RoundDiffReport'), 'roles'=>array('ROUNDINGDIFF_REPORT') ),
        array('allow', 'actions'=>array('TurnoverReport'), 'roles'=>array('TURNOVER_REPORT') ),
        array('allow', 'actions'=>array('TurnoverSalesReport'), 'roles'=>array('TURNOVERSALES_REPORT') ),
        array('allow', 'actions'=>array('CarsTypesModelsReport'), 'roles'=>array('CARSTYPESMODELS_REPORT') ),
        array('allow', 'actions'=>array('contactlesscards'), 'roles'=>array('CONTACTLESSCARD_MANAGEMENT') ),
        array('allow', 'actions'=>array('CardOrderPage','CardOrderData','CellsTable', 'UpdateCarKeyStatus', 'Queue'), 'users'=>array('*') ),
        array('allow', 'actions'=>array('Parking', 'ParkingData', 'CarCheck', 'GetCarsByTerm_ajax', 'ParkingClose', 'carbrandchange'), 'roles'=>array('PARKING_MANAGEMENT') ),
        array('allow', 'actions'=>array('CellsManage', 'ReleaseCellByID'), 'roles'=>array('CELLS_MANAGER') ),
        array('allow', 'actions'=>array('ParkingReport'), 'roles'=>array('PARKING_REPORT') ),
        array('allow', 'actions'=>array('ParkingTurnoverReport'), 'roles'=>array('PARKING_TURNOVER_SALES_REPORT') ),
        array('allow', 'actions'=>array('BenefitsCars','BenefitsCarAdd','BenefitsCarDelete', 'GetCarsByTerm_ajax', 'GetClientsByTerm_ajax'), 'roles'=>array('PARKING_BEBEFITSCARS') ),
        array('allow', 'actions'=>array('PrepaidParkingCars','PrepaidParkingCarAdd','PrepaidParkingCarDelete', 'GetCarsByTerm_ajax', 'GetClientsByTerm_ajax'), 'roles'=>array('PARKING_PREPAIDCARS') ),
        array('allow', 'actions'=>array('PaysList', 'PayCancel'), 'roles'=>array('PAYMENTS_MANAGEMENT') ),
        array('allow', 'actions'=>array('UniqeCarStats'), 'roles'=>array('UNIQUE_CAR_STAT_REPORT') ),
        array('allow', 'actions'=>array('StaffView','GetImageById','GetImageThumbById','Staff'), 'roles'=>array('STAFF_VIEW','STAFF_MANAGEMENT') ),
        array('allow', 'actions'=>array('StaffDocDeleteImage','StaffDocAddImage','StaffEdit','StaffAdd'), 'roles'=>array('STAFF_MANAGEMENT') ),
        array('allow', 'actions'=>array('StaffWorkPlaces','StaffWorkPlaceAdd','StaffWorkPlaceDelete','StaffWorkPlaceEdit'), 'roles'=>array('STAFF_WORKPLACES_MANAGEMENT') ),
        array('deny', 'users' => array('*' ) )
    ),
    /** глобальные настройки видимости отчетов. Каждый элемент массива проверяется с другим по принципу "ИЛИ".
     * Например, если отсутствует указанная привилегия у прользователя, смотрим, попадает ли его айпи в указанный диапазон
    */
    'reports_global_access' => array(
        'priv' => 'INTERNET_LOGIN',
        'ipRange' => '127.0.0.2-127.0.0.255',
    ),
    'reports_config' => array(
        /** ключ массива - часть адреса (напр.: test - admin/report/test)
         * title - что будет выведено на странице в заголовке
         * spName - имя хранимки,
         * access - кому доступно,
         * export - экспорт (print - печать, xls - в Excel)
         * wide - широкая таблица с результатами (вылезет за рабочую область с прокрутками и отцентрируется)
         **/
        //"test" => array( "title" => "Отчет тестовый", "spName" => "!!__Report_Test", "access" => "Administrator", "export" => array("print", "xls") ),
        "acts" => array( "title" => "Отчет по договорам", "spName" => "Report_Acts_New", "access" => "ACTS_REPORT", "export" => array("print", "xls") ),
        "workerssalary" => array( "title" => "Отчет по зарплатам мойщиков", "spName" => "Report_Workers_New", "access" => "SALARY_REPORT", "export" => array("print", "xls") ),
        "orderssummary" => array( "title" => "Сводный отчет по заказам", "spName" => "Report_Orders_New", "access" => "ORDERS_REPORT", "export" => array("print", "xls"), "wide" => true ),
        "debtorsssummary" => array( "title" => "Сводный отчет по должникам", "spName" => "Report_Deptors_New", 'withFilter' => false, "access" => "DEBTORS_REPORT", "export" => array("print", "xls") ),
        "master" => array( "title" => "Отчет по работе мастера констультанта", "spName" => "Report_Administrators_New", "access" => "ADMIN_REPORT", "export" => array("print", "xls") ),
        "tasks" => array( "title" => "Отчет по услугам", "spName" => "Report_DailyTasks_New", "access" => "TASKS_REPORT", "export" => array("print", "xls") ),
        "specialclients" => array( "title" => "Отчет по клиентам со специальными условиями", "spName" => "Report_SpecialClients_New", "access" => "SPECIALCLIENT_REPORT", "export" => array("print", "xls") ),
        "statsbycars" => array( "title" => "Отчет по машинам", "spName" => "Report_CarStatistic_New", "access" => "CARSTYPESMODELS_REPORT", "export" => array("print", "xls") ),
        "universalsummary" => array( "title" => "Универсальный отчет по услугам", "spName" => "Report_UniversalSummary_New", "access" => "UNIVERSALSUMMARY_REPORT", "export" => array("print", "xls") ),
        "uniqecarstats" => array( "title" => "Количество уникальных машин", "spName" => "Report_Unique_CarStatistic_New", "access" => "UNIQUE_CAR_STAT_REPORT", "export" => array("print", "xls") ),
        "admintasks" => array( "title" => "Отчет по услугам администратора", "spName" => "Report_AdministratorTasks_New", "access" => "ADMINISTRATOR_TASKS_REPORT", "export" => array("print", "xls") ),
        "specialorders" => array( "title" => "Отчет по специальным заказам", "spName" => "Report_Spec_Orders_New", "access" => "SPECIAL_ORDERS_REPORT", "export" => array("print", "xls") ),
        "simplemanualdiscount" => array( "title" => "Отчет по рекламным акциям", "spName" => "Report_Simple_Manual_Discount_Orders_New", "access" => "SIMPLE_MANUAL_DISCOUNT_ORDERS_REPORT", "export" => array("print", "xls") ),

        /*"admins" => array( "title" => "Отчет по администраторам", "spName" => "Report_Administrators_New", "access" => "ADMIN_REPORT", "export" => array("print", "xls") ),
        "carscount" => array( "title" => "Отчет по количеству автомобилей", "spName" => "Report_Cars_New", "access" => "CARSCOUNT_REPORT", "export" => array("print", "xls") ),
        "cashiers" => array( "title" => "Отчет по кассирам", "spName" => "Report_Cashiers_New", "access" => "CASHIERS_REPORT", "export" => array("print", "xls") ),
        "parkingturnover" => array( "title" => "Отчет по продажам парковки", "spName" => "Report_ParkingTurnoverSales_New", "access" => "PARKING_TURNOVER_SALES_REPORT", "export" => array("print", "xls") ),
        "paytypes" => array( "title" => "Отчет по типам оплаты", "spName" => "Report_PayTypes_New", "access" => "ORDERS_MANAGEMENT", "export" => array("print", "xls") ),
        "soldgoods" => array( "title" => "Отчет по проданным товарам", "spName" => "Report_SaleTypes_New", "access" => "ORDERS_MANAGEMENT", "export" => array("print", "xls") ),
        "turnover" => array( "title" => "Отчет по оборотам", "spName" => "Report_Turnover_New", "access" => "TURNOVER_REPORT", "export" => array("print", "xls") ),
        "turnoversales" => array( "title" => "Отчет по продажам", "spName" => "Report_TurnoverSales_New", "access" => "TURNOVERSALES_REPORT", "export" => array("print", "xls") ),*/
    )
);
