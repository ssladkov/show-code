<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 9/22/2017
 * Time: 5:41 PM
 */
require_once 'Autoloader.php';

$trader = new Crypto\Service\Exchange([
    //new Crypto\Service\Exchange\Bitfinex(),
    new Crypto\Service\Exchange\Bittrex(),
    //new Crypto\Service\Exchange\Poloniex(),
    //new Crypto\Service\Exchange\CoinExchange(),
    //new Crypto\Service\Exchange\Cryptopia(),
    //new Crypto\Service\Exchange\Exmo(),
    new Crypto\Service\Exchange\Yobit(),
    //new Crypto\Service\Exchange\Bithumb(),
    //new Crypto\Service\Exchange\HitBTC(),
    new Crypto\Service\Exchange\Binance(),
]);

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <title>CryptoTrader.RU</title>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">CryptoTrader.RU</a>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php foreach($trader->getAllNotExclusivePairs() as $name => $pair_data) : ?>
                <h1><?=$name;?></h1>
                <table class="table">
                    <tr>
                        <th>Биржа</th>
                        <th>Смогу продать за</th>
                        <th>Оредров на покупку</th>
                        <th>Смогу купить за</th>
                        <th>Оредров на продажу</th>
                        <th>Объём пары</th>
                        <th>Макс. цена</th>
                        <th>Мин. цена</th>
                        <th>Посл. цена</th>
                    </tr>
                    <?php foreach($pair_data as $exchange_name => $market) : ?>
                        <tr>
                            <td><?=$exchange_name;?></td>
                            <td><?=$market->buyPrice;?></td>
                            <td><?=$market->buyOpenOrders;?></td>
                            <td><?=$market->sellPrice;?></td>
                            <td><?=$market->sellOpenOrders;?></td>
                            <td><?=$market->currentVolume;?></td>
                            <td><?=$market->maxPrice;?></td>
                            <td><?=$market->minPrice;?></td>
                            <td><?=$market->lastPrice;?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('table').each( function() {
            var min_price = 0;
            var max_price = 0;
            var max_price_cell = "";
            var min_price_cell = "";
            $(this).find('tr').each( function(i){
                if(i>0) {
                    var temp_max_price_cell = $(this).find('td').eq(1);
                    var temp_min_price_cell = $(this).find('td').eq(3);
                    var temp_max_price = $(temp_max_price_cell).text();
                    var temp_min_price = $(temp_min_price_cell).text();
                    if($.isNumeric(temp_max_price) && $.isNumeric(temp_min_price)) {
                        temp_min_price = parseFloat(temp_min_price);
                        temp_max_price = parseFloat(temp_max_price);
                        if(i == 1) {
                            min_price = temp_min_price;
                            min_price_cell = temp_min_price_cell;
                        }
                        if(temp_max_price > max_price) {
                            max_price = temp_max_price;
                            max_price_cell = temp_max_price_cell;
                        }
                        if(temp_min_price < min_price) {
                            min_price = temp_min_price;
                            min_price_cell = temp_min_price_cell;
                        }
                    }
                }
            });
            if(max_price > min_price) {
                var profit = 100 - Math.round(min_price * 100 / max_price);
                $(max_price_cell).prop("title", "Profit: " + profit.toString() + "%");
                $(max_price_cell).css("background-color", "#7eff00");
                $(min_price_cell).prop("title", "Profit: " + profit.toString() + "%");
                $(min_price_cell).css("background-color", "#7eff00");
            }
        });
    });
</script>

</body>
</html>