<?php

/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 9/22/2017
 * Time: 5:04 PM
 */
namespace Crypto\Service\Exchange

class Bittrex extends Exchange
{
	public $api_uri = "https://bittrex.com/api/v1.1/public/";
	public $api_pairs_endpoint = "getmarketsummaries";
	public $name = "Bittrex";
	public $code = "bittrex";
	public $pairs_key_field = "result/MarketName";
	public $pair_delimiter = "-";
	public $pair_reverse_order = true;

	public $ticker_fields = [
		"buy_price" => "Bid",
		"sell_price" => "Ask",
		"buy_orders" => "OpenBuyOrders",
		"sell_orders" => "OpenSellOrders",
		"volume" => "BaseVolume",
		"max_price" => "High",
		"min_price" => "Low",
		"last_price" => "Last",
	];
}