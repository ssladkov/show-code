<?php

/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 9/22/2017
 * Time: 5:04 PM
 */
namespace Crypto\Service\Exchange 

class Yobit extends Exchange
{
	public $api_uri = "https://yobit.net/api/3/";
	public $api_pairs_endpoint = "info";
	public $name = "Yobit";
	public $code = "yobit";
	public $pairs_key_field = "";
	public $pair_delimiter = "_";

	public $ticker_fields = [
		"buy_price" => "buy",
		"sell_price" => "sell",
		"buy_orders" => "",
		"sell_orders" => "",
		"volume" => "vol",
		"max_price" => "high",
		"min_price" => "low",
		"last_price" => "last",
	];

	/** Кастоманая функция для Yobit, подготавливает стандартизированный массив криптопар
	 * @param array $pairs_arr
	 * @return array
	 */
	public function preparePairsArr($pairs_arr) {
		$result = [];
		foreach($pairs_arr["pairs"] as $pair_name => $pair_data) {
			$result[] = [
				"name" => strtoupper(str_replace($this->pair_delimiter, $this->pair_delimiter_standard, $pair_name)),
				"code" => $pair_name,
				"raw_data" => null,
			];
		}
		return $result;
	}

	public function setRawData() {
		$data = [];
		$raw_data = [];
		foreach($this->all_pairs as $market) {
			if(!$market->isExclusive) {
				$data[] = $market->code;
			}
		}
		$c = 0;
		$tmp_arr = [];
		foreach($data as $code) {
			$tmp_arr[] = $code;
			//дебильный api... посылаем группами пары, все сразу нельзя - не обработает
			if($c > 50) {
				$json_data = $this->_getAPIData($this->api_uri . 'ticker/' . implode("-", $tmp_arr) . "?ignore_invalid=1");
				$raw_data = array_merge($raw_data, json_decode($json_data, true));
				$tmp_arr = [];
				$c = 0;
			}
			$c++;
		}

		foreach($this->all_pairs as &$market) {
			if(!$market->isExclusive && isset($raw_data[(string)$market->code])) {
				$market->rawData = $raw_data[(string)$market->code];
			}
		}
	}
}