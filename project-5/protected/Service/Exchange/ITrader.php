<?php

/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 9/22/2017
 * Time: 3:48 PM
 */
namespace Crypto\Service\Exchange
interface ITrader
{
	/** Метод для возвращения всех подключенных объектов торговых площадок */
	public function getExchanges();

	/** Метод для возвращения валютных пар для всех подключенных торговых площадок */
	public function getMarkets();

	/** Метод для возвращения информации по конкретной подключенной торговой площадке по переданному ID
	 * @param string $exchange_id
	 */
	public function getExchangeInfo($exchange_id);
}