<?php

/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 9/22/2017
 * Time: 4:11 PM
 */
namespace Crypto\Service\Exchange

class Trader implements ITrader
{
	/** @var array Массив всех возможных валютных пар со всех подключенных ТП */
	private $_all_pairs = [];

	/** @var \Exchanges\Exchange[] массив объектов-торговых площадок */
	private $_exchanges = [];

	/** @var array массив названий пар, с которыми будем работать */
	private $_pairs_workflow = [];

	private $_markets = [];

	/**
	 * @param \Exchanges\Exchange[] $exchanges_arr
	 * @throws Exception
	 */
	public function __construct($exchanges_arr) {
		if(!is_array($exchanges_arr) || count($exchanges_arr) < 2) throw new Exception('At least 2 exchanges must be specified!');
		/** @var array $pairs_found_count_arr Количество раз, которое каждая пара встречается на всех ТП (если 1, пара считается эксклюзивной) */
		$pairs_found_count_arr = [];
		foreach($exchanges_arr as $exchange) {
			if(!($exchange instanceof \Exchanges\Exchange)) throw new Exception('One ore more objects in Trader constructor are not instances of Exchange class!');
			/** @var Exchanges\Market $market */
			foreach($exchange->getAllPairs() as $market) {
				$name = $market->name;
				if(!isset($pairs_found_count_arr[$name])) $pairs_found_count_arr[$name] = 1;
				else $pairs_found_count_arr[$name] = $pairs_found_count_arr[$name] + 1;
			}
			$this->_exchanges[] = $exchange;
		}
		$this->setPairsWorkflow($pairs_found_count_arr);

		/** Теперь надо еще раз пробежаться по всем парам маркетов и пометить эксклюзивные пары */
		foreach($this->_exchanges as $exchange) {
			/** @var Exchanges\Market $market */
			foreach($exchange->getAllPairs() as &$market) {
				if(!in_array($market->name, $this->_pairs_workflow)) $market->isExclusive = true;
			}
			/** Добавляем информацию по продажам всех валютных пар */
			$exchange->setRawData();
			$exchange->addPairsTicker(true);
		}
	}

	public function setPairsWorkflow($data) {
		foreach($data as $name => $count) {
			if($count > 1) $this->_pairs_workflow[] = $name;
		}
		array_unique($this->_pairs_workflow);
	}

	/** Анализирует все криптопары на подключенных ТП и возвращает лишь те, которые присутствуют хотя бы на двух биржах */
	public function getAllPairs() {
		foreach($this->_exchanges as $exchange) {
			/** @var Exchanges\Market $market */
			foreach($exchange->getAllPairs() as $market) {
				$pair = $market->name;
				if(!isset($this->_all_pairs[$pair])) $this->_all_pairs[$pair] = [];
				$this->_all_pairs[$pair][$exchange->getName()] = $market;
			}
		}
		return $this->_all_pairs;
	}

	public function getAllNotExclusivePairs() {
		$result = [];
		foreach($this->getAllPairs() as $pair_name => $exchanges_arr) {
			if(count($exchanges_arr)>1) $result[$pair_name] = $exchanges_arr;
		}
		return $result;
	}

	public function getExchanges()
	{
		// TODO: Implement getExchanges() method.
	}

	public function getMarkets()
	{
		// TODO: Implement getMarkets() method.
	}

	public function getExchangeInfo($exchange_id)
	{
		// TODO: Implement getExchangeInfo() method.
	}
}