<?php

/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 9/22/2017
 * Time: 5:04 PM
 */
namespace Crypto\Service\Exchange 

class Binance extends Exchange
{
	public $api_uri = "https://www.binance.com/api/v1/";
	public $api_pairs_endpoint = "ticker/allBookTickers";
	public $name = "Binance";
	public $code = "binance";
	public $pairs_key_field = "";
	public $pair_delimiter = "";
	public $pair_reverse_order = true;

	public $ticker_fields = [
		"buy_price" => "bidPrice",
		"sell_price" => "askPrice",
		"buy_orders" => "",
		"sell_orders" => "",
		"volume" => "",
		"max_price" => "",
		"min_price" => "",
		"last_price" => "",
	];

	/** Кастоманая функция для Binance, подготавливает стандартизированный массив криптопар
	 * @param array $data
	 * @return array
	 */
	public function preparePairsArr($data) {
		$result = [];
		foreach($data as $item) {
			if(strlen($item["symbol"]) < 5) continue;
			$pair_base = substr($item["symbol"], -3);
			if(!in_array($pair_base,['ETH','BTC'])) continue;
			$pair_symb = str_replace($pair_base, "", $item["symbol"]);
			$result[] = [
				"name" => ($pair_symb . $this->pair_delimiter_standard . $pair_base),
				"code" => $item["symbol"],
				"raw_data" => $item,
			];
		}
		return $result;
	}
}