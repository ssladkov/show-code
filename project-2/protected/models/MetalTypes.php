<?php

/**
 * This is the model class for table "MetalTypes".
 *
 * The followings are the available columns in table 'MetalTypes':
 * @property integer $ID
 * @property string $Name
 * @property boolean $Enabled
 *
 * The followings are the available model relations:
 * @property Metals[] $metals
 */
class MetalTypes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MetalTypes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'MetalTypes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Name, Enabled', 'required'),
			array('Name', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, Name, Enabled', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'metals' => array(self::HAS_MANY, 'Metals', 'MetalTypeID'),
            'metalTypeProperties' => array(self::HAS_MANY, 'MetalTypesMetalPropertyTypes', 'MetalTypeID'),
            'metalProperties' => array(self::MANY_MANY, 'MetalPropertyTypes', 'MetalTypes_MetalPropertyTypes(MetalTypeID, MetalPropertyTypeID)'),
		);
	}
    public function scopes() {
        return array(
            'enabled'=>array(
                'condition'=>'Enabled = 1',
            ),
        );
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Name' => 'Name',
			'Enabled' => 'Enabled',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Enabled',$this->Enabled);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}