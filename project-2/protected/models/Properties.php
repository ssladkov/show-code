<?php
/**
 * Properties.php
 * базовый класс для формы свойств изделий металопроката
 * необходимые поля подгружаются динамически
 * правила создаются динамически
 */

class Properties extends CFormModel {

	public $values = array();
    public $labels = array();
    private $rules = array();
    private $varTypes = array();

	public function rules() {
		return array(
			array('values', 'validateValues'),
		);
        //при проверке свойств вида Properties.values[N] возникает ошибка отсутствия такого свойства, вероятно потому, что
        //значение в скобках не интерпретируется как индекс массива, поэтому, использовать подстановку массива с правилами нвозможно
        //return $this->rules;
	}
    public function validateValues($attribute,$params)
    {
        foreach($this->values as $key => $val){
            if (empty($val) && in_array("requried",$this->rules[$key])) {
                $this->addError("values[$key]", "Поле должно быть заполнено");
            }
            if( !empty($val) && in_array("numerical",$this->rules[$key]) && (!is_numeric($val) || !(round($val) == $val)) )
                $this->addError("values[$key]", "Допускаются только целочисленные значения");
        }
    }
    public function loadData($typeID) {
        $model=MetalTypes::model()->findByPk($typeID);
        if($model===null)
            throw new CHttpException(404,'Не найден тип метализделия с ID:'.$typeID);
        $this->loadProperties($model);
        $this->generateRules($typeID);
    }
    public function loadValues($metalModel) {
        foreach( $metalModel->metalProperties as $obj ) {
            $this->values[$obj->MetalPropertyTypeID] = $obj->Value;
        }
    }

    private function loadProperties($model) {
        $req_arr = array();
        foreach( $model->metalTypeProperties as $obj ) {
            if( $obj->Required == 1 )
                $req_arr[] = $obj->MetalPropertyTypeID;
        }
        foreach( $model->metalProperties as $obj ) {
            $property_id = $obj->ID;
            $this->values[$property_id] = "";
            $this->labels["values[$property_id]"] = $obj->Name.($obj->Unit?", ".$obj->Unit:""). ( in_array($property_id,$req_arr) ? ' <span class="required">*</span>' : "" );
            $this->varTypes[$property_id] = $obj->Type;
        }
    }

    private function generateRules($typeID) {
        $model = new MetalTypesMetalPropertyTypes('search');
        $model->MetalTypeID = $typeID;
        foreach( $model->search()->getData() as $obj ) {
            $mptypeid = $obj->MetalPropertyTypeID;
            if( $obj->Required ) {
                $this->rules[$mptypeid][] = 'requried';
            }
            if( $this->varTypes[$obj->MetalPropertyTypeID] == "int" ) $this->rules[$mptypeid][] = 'numerical';
        }
    }

    public function attributeLabels()
    {
        return $this->labels;
    }

}
