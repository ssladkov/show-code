<?php

/**
 * This is the model class for table "Metals".
 *
 * The followings are the available columns in table 'Metals':
 * @property string $ID
 * @property integer $MetalTypeID
 * @property integer $MetalMarkTypeID
 * @property integer $MetalStandartID
 * @property integer $MetalMarkTypeStandartID
 * @property integer $MetalWeightTypeID
 * @property string $Name
 * @property string $Weight
 *
 * The followings are the available model relations:
 * @property MetalMarkTypes $metalMarkType
 * @property MetalTypes $metalType
 * @property MetalProperties[] $metalProperties
 */
class Metals extends CActiveRecord
{
    const BASE_WEIGHT_TXT = "кг";

    const ERR_ADD = -1;
    const ERR_UPDATE = -4;
    const ERR_DELETE = -5;
    const OK_ADD = 1;
    const OK_UPDATE = 4;
    const OK_DELETE = 5;

    public $statuses_arr = array();
    public $status_txt = array(
        1   => "Объект %s% успешно добавлен",
        4   => "Объект %s% успешно обновлен",
        5   => "Объект %s% успешно удален",
        -1  => "При добавлении объекта %s% произошла ошибка, код %s%",
        -4  => "При обновлении объекта %s% произошла ошибка, код %s%",
        -5  => "При удалении объекта %s% произошла ошибка, код %s%",
    );

	public $pageSize = 10;
    public $Properties = array(); //свойства будут временно тут содержаться
    public $onlyEnabled;
    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Metals the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Metals';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('MetalTypeID, MetalMarkTypeID, MetalMarkTypeStandartID, MetalStandartID, MetalWeightTypeID, Name', 'required'),
			array('MetalTypeID, MetalMarkTypeID, MetalMarkTypeStandartID, MetalStandartID, MetalWeightTypeID', 'numerical', 'integerOnly'=>true),
			array('Name', 'length', 'max'=>500),
			//array('Standart', 'length', 'max'=>50),
			array('Weight', 'length', 'max'=>18),
			array('Weight', 'numerical'),
            array('Enabled', 'boolean'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, MetalTypeID, MetalMarkTypeID, MetalStandartID, MetalMarkTypeStandartID, Name, Weight', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'metalMarkType' => array(self::BELONGS_TO, 'MetalMarkTypes', 'MetalMarkTypeID'),
			'metalType' => array(self::BELONGS_TO, 'MetalTypes', 'MetalTypeID'),
			'typeStandart' => array(self::BELONGS_TO, 'MetalStandarts', 'MetalStandartID'),
			'markStandart' => array(self::BELONGS_TO, 'MetalMarkTypeStandarts', 'MetalMarkTypeStandartID'),
			'weightType' => array(self::BELONGS_TO, 'MetalWeightTypes', 'MetalWeightTypeID'),
			'metalProperties' => array(self::HAS_MANY, 'MetalProperties', 'MetalID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
            'Enabled' => 'Активен',
			'MetalTypeID' => 'Профиль',
			'MetalMarkTypeID' => 'Марка',
			'MetalStandartID' => 'Стандарт на профиль',
			'MetalMarkTypeStandartID' => 'Стандарт на марку',
			'MetalWeightTypeID' => 'Ед. распред-я веса',
			'Name' => 'Название',
			//'Standart' => 'Стандарт',
			'Weight' => 'Вес',
            'onlyEnabled' => 'Только активные',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare('ID',$this->ID,true);
		$criteria->compare('MetalTypeID',$this->MetalTypeID);
		$criteria->compare('MetalMarkTypeID',$this->MetalMarkTypeID);
		$criteria->compare('t.MetalStandartID',$this->MetalStandartID);
		$criteria->compare('t.MetalMarkTypeStandartID',$this->MetalMarkTypeStandartID);
		$criteria->compare('t.Name',$this->Name,true);
		$criteria->compare('t.Enabled',$this->Enabled);
		//$criteria->compare('Standart',$this->Standart,true);
		//$criteria->compare('Weight',$this->Weight,true);
        $criteria->with = array("metalMarkType", "metalType", "metalProperties", "metalProperties.metalPropertyTypes", "markStandart", "typeStandart","typeStandart.t_file", "markStandart.m_file", "weightType");

		return new ActiveDataProvider($this, array(
            'sort' => array(
                'defaultOrder'=>array('Name' => CSort::SORT_ASC)
            ),
			'criteria'=>$criteria,
		));
	}

    /** создание объекта */
    public function saveProc() {
        $properties_xml = "<r>";
        foreach( $this->Properties as $key => $val ) {
            $properties_xml .= "<metalProperty id='$key'>$val</metalProperty>";
        }
        $properties_xml .= "</r>";
        $xmlDoc = new DOMDocument();
        $xmlDoc->loadXML($properties_xml);

        $data = array(
            array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 64 ),
            array( "name" => "MetalTypeID", "type" => "in", "value" => $this->MetalTypeID, "length" => 5 ),
            array( "name" => "MetalMarkTypeID", "type" => "in", "value" => $this->MetalMarkTypeID, "length" => 5 ),
            array( "name" => "Name", "type" => "in", "value" => $this->Name, "length" => 500 ),
            array( "name" => "MetalStandartID", "type" => "in", "value" => $this->MetalStandartID, "length" => 5 ),
            array( "name" => "MetalMarkTypeStandartID", "type" => "in", "value" => $this->MetalMarkTypeStandartID, "length" => 5 ),
            array( "name" => "MetalWeightTypeID", "type" => "in", "value" => $this->MetalWeightTypeID, "length" => 5 ),
            array( "name" => "Weight", "type" => "in", "value" => $this->Weight, "length" => 20 ),
            array( "name" => "Properties", "type" => "in", "value" => $xmlDoc->saveXML(), "length" => 500 ),
            array( "name" => "ID", "type" => "out", "length" => 64 ),
        );
        $resArr = Controller::storedProcedureCall("Metals_Create",$data);
        if ($this->ID = $resArr['out_data']['ID']) {
            $this->statuses_arr[] = array("code"=>self::OK_ADD,"params"=>$this->Name );
        } else {
            $this->statuses_arr[] = array("code"=>self::ERR_ADD,"params"=> array($this->Name,$resArr['result']) );
        }
        return true;
    }

    /** редактирование объекта */
    public function updateProc() {
        $properties_xml = "<r>";
        foreach( $this->Properties as $key => $val ) {
            $properties_xml .= "<metalProperty id='$key'>$val</metalProperty>";
        }
        $properties_xml .= "</r>";
        $xmlDoc = new DOMDocument();
        $xmlDoc->loadXML($properties_xml);

        $data = array(
            array( "name" => "UserID", "type" => "in", "value" => Yii::app()->user->id, "length" => 64 ),
            array( "name" => "MetalTypeID", "type" => "in", "value" => $this->MetalTypeID, "length" => 5 ),
            array( "name" => "MetalMarkTypeID", "type" => "in", "value" => $this->MetalMarkTypeID, "length" => 5 ),
            array( "name" => "Name", "type" => "in", "value" => $this->Name, "length" => 500 ),
            array( "name" => "MetalStandartID", "type" => "in", "value" => $this->MetalStandartID, "length" => 5 ),
            array( "name" => "MetalMarkTypeStandartID", "type" => "in", "value" => $this->MetalMarkTypeStandartID, "length" => 5 ),
            array( "name" => "MetalWeightTypeID", "type" => "in", "value" => $this->MetalWeightTypeID, "length" => 5 ),
            array( "name" => "Weight", "type" => "in", "value" => $this->Weight, "length" => 20 ),
            array( "name" => "Properties", "type" => "in", "value" => $xmlDoc->saveXML(), "length" => 500 ),
            array( "name" => "MetalID", "type" => "in", "value" => $this->ID, "length" => 64 ),
            array( "name" => "Enabled", "type" => "in", "value" => $this->Enabled, "length" => 1 ),
        );
        $resArr = Controller::storedProcedureCall("Metals_Update",$data);
        if( !$resArr['result'] ) {
            $this->statuses_arr[] = array("code"=>self::OK_UPDATE,"params"=>$this->Name );
        } else {
            $this->statuses_arr[] = array("code"=>self::ERR_UPDATE,"params"=> array($this->Name,$resArr['result']) );
        }
        return true;
    }
    public function getSetProperties($data) {
        $output = "<ul>";
        foreach($data->metalProperties as $obj) {
            $output .= "<li>" . $obj->metalPropertyTypes->Name . ": " . $obj->Value . " " . $obj->metalPropertyTypes->Unit . "</li>";
        }
        $output .= "</ul>";
        return $output;
    }

    public function getWeight($data) {
        $output = number_format($data->Weight,3, ".", "");
        $output .= " ".self::BASE_WEIGHT_TXT."/";
        $output .= $data->weightType->Name;
        return $output;
    }
}