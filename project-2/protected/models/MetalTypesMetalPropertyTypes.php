<?php

/**
 * This is the model class for table "MetalTypes_MetalPropertyTypes".
 *
 * The followings are the available columns in table 'MetalTypes_MetalPropertyTypes':
 * @property integer $MetalTypeID
 * @property integer $MetalPropertyTypeID
 */
class MetalTypesMetalPropertyTypes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MetalTypesMetalPropertyTypes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'MetalTypes_MetalPropertyTypes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('MetalTypeID, MetalPropertyTypeID', 'required'),
			array('MetalTypeID, MetalPropertyTypeID', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('MetalTypeID, MetalPropertyTypeID', 'safe', 'on'=>'search'),
		);
	}
    public function primaryKey() {
        return array("MetalTypeID","MetalPropertyTypeID");
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            //'metalProperties' => array(self::HAS_ONE, 'MetalPropertyTypes', array('ID'=>'MetalPropertyTypeID')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'MetalTypeID' => 'Metal Type',
			'MetalPropertyTypeID' => 'Metal Property Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MetalTypeID',$this->MetalTypeID);
		$criteria->compare('MetalPropertyTypeID',$this->MetalPropertyTypeID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}