<?php

/**
 * SiteController is the default controller to handle user requests.
 */
class SiteController extends Controller
{
    public $recursiveTree_arr = array();
    /**
	 * Index action is the default action in a controller.
	 */
	public function actionIndex()
	{
		$this->render('index');
	}
	public function filters() {
        return array(
            'accessControl',
         );
    }
	public function accessRules() {
		return array(
			array('allow', 'users' => array('@') ),
			array('deny', 'users' => array('*' ) )
		);
	}

	public function actionLogin()
	{
		$model = new LoginForm();

		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
		{
			echo CActiveForm::validate($model, array('username', 'password', 'verifyCode'));
			Yii::app()->end();
		}

		if (isset($_POST['LoginForm']))
		{
			echo "<pre>".print_r($_POST)."</pre>";
			die;
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate(array('username', 'password')) && $model->login())
				$this->redirect(user()->returnUrl);
		}

		//$sent = r()->getParam('sent', 0);
		$this->render('login', array(
			'model' => $model,
			//'sent' => $sent,
		));
	}
    public function actionTreeMain() {
        $model = new Entities();
        $this->render('treemain',array(
            'title' => 'Справочник изделий',
            'model' => $model,
            'dataProvider' => $model->projects()->search()
        ));
    }

    public function actionGetTreeByID_ajax() {
        $id = !isset($_POST["id"]) || $_POST["id"] == "" ? false : $_POST["id"];
        switch( $_POST["type"] ) {
            case "groups": $model = new EntityGroup(); break;
            default: $model = new EntityTree();
        }
        $onlyEnabled = isset($_POST["onlyEnabled"])&&$_POST["onlyEnabled"];
        if( !$id )
            $data = $model->findAll("ParentID is null");
        else {
            if( isset($_POST["withSelf"])) {
                if ($onlyEnabled)
                    $data = $model->findAll("(ParentID = :pid OR ID = :id) AND Enabled = 1",array(":pid"=>$id,":id"=>$id));
                else
                    $data = $model->findAll("ParentID = :pid OR ID = :id",array(":pid"=>$id,":id"=>$id));
            }
            else {
                if ($onlyEnabled)
                    $data = $model->findAll("ParentID = :pid AND Enabled = 1",array(":pid"=>$id));
                else
                    $data = $model->findAll("ParentID = :pid",array(":pid"=>$id));
            }
        }
        $out_arr = array();
        foreach($data as $obj) {
            if ($onlyEnabled)
                $is_parent = $model->count("ParentID = :pid AND Enabled = 1", array(":pid"=>$obj->ID)) > 0 ? true : false;
            else
                $is_parent = $model->count("ParentID = :pid", array(":pid"=>$obj->ID)) > 0 ? true : false;
            if( $_POST["type"] == "default" )
                $out_arr[] = array("id" => $obj->ID, "name" => ("[".$obj->entity->entityType->ShortName."] ".$obj->entity->Name.($obj->Count>1?"[".$obj->Count."]":"")), "isParent" => $is_parent, "parentId" => $obj->ParentID );
            else {
                if (!$obj->Enabled)
                    $out_arr[] = array("id" => $obj->ID, "name" => $obj->Name, "isParent" => $is_parent, "parentId" => $obj->ParentID, "icon"=>'/images/close.png' );
                else
                    $out_arr[] = array("id" => $obj->ID, "name" => $obj->Name, "isParent" => $is_parent, "parentId" => $obj->ParentID );
            }
        }
        echo json_encode($out_arr);
        Yii::app()->end();
    }

    public function actionGetSingleNodeByID_ajax() {
        $id = !isset($_POST["id"]) || $_POST["id"] == "" ? false : $_POST["id"];
        if( !$id ) die;
        $model = Entities::model()->findByPk($id);
        $is_parent = EntityTree::model()->count("ParentID = :pid", array(":pid"=>$id)) > 0 ? true : false;
        $out_arr = array("id" => $id, "name" => ("[".$model->entityType->ShortName."] ".$model->Name), "isParent" => $is_parent);
        echo json_encode($out_arr);
        Yii::app()->end();
    }

    public function actionGetGroupRootNodesByID_ajax() {
        $id = !isset($_POST["id"]) || $_POST["id"] == "" ? false : $_POST["id"];
        if( !$id ) die;
        $onlyEnabled = isset($_POST["onlyEnabled"])&&$_POST["onlyEnabled"];
        if ($onlyEnabled)
            $model = EntityGroup::model()->findAll("TypeID = :typeID AND ParentID is NULL AND Enabled = 1",array("typeID"=>$id));
        else
            $model = EntityGroup::model()->findAll("TypeID = :typeID AND ParentID is NULL",array("typeID"=>$id));
        $out_arr = array();
        foreach( $model as $model_item ) {
            $curr_id = $model_item->ID;
            if ($onlyEnabled)
                $is_parent = EntityGroup::model()->count("ParentID = :pid AND Enabled = 1", array(":pid"=>$curr_id)) > 0 ? true : false;
            else
                $is_parent = EntityGroup::model()->count("ParentID = :pid", array(":pid"=>$curr_id)) > 0 ? true : false;
            if (!$model_item->Enabled)
                $out_arr[] = array("id" => $curr_id, "name" => $model_item->Name, "isParent" => $is_parent, "icon"=>'/images/close.png');
            else
                $out_arr[] = array("id" => $curr_id, "name" => $model_item->Name, "isParent" => $is_parent);
        }
        echo json_encode($out_arr);
        Yii::app()->end();
    }

    public function actionBuildTreeRecursiveByID($id) {
        $data = EntityTree::model()->find("ID = :id",array(":id"=>$id));
        if( $pid = $data->ParentID ) {
            $this->_getTreeBranchByID($pid);
            $this->actionBuildTreeRecursiveByID($pid);
        }
        else {
            $this->_getTreeRootBranch();
        }
        echo json_encode($this->recursiveTree_arr);
        Yii::app()->end();
    }
    private function _getTreeBranchByID($pid) {
        $data = EntityTree::model()->findAll("ParentID = :pid", array(":pid"=>$pid));
        foreach($data as $obj) {
            $this->recursiveTree_arr[] = array(
                "id" => $obj->ID,
                "name" => ($obj->entity->entityType->Name.": ".$obj->entity->Name),
                "isParent" => (EntityTree::model()->count("ParentID = :pid", array(":pid"=>$obj->ID)) > 0 ? true : false),
                "parentId" => $pid
            );
        }
        return $pid;
    }
    private function _getTreeRootBranch() {
        $data = EntityTree::model()->findAll("ParentID is NULL");
        foreach($data as $obj) {
            $this->recursiveTree_arr[] = array(
                "id" => $obj->ID,
                "name" => ($obj->entity->entityType->Name.": ".$obj->entity->Name),
                "isParent" => true,
                "parentId" => null
            );
        }
    }

    /** Добавление сущности без привязки к дереву */
    public function actionEntityOnlyAdd($typeID=false) {
        $model = new Entities();
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Entities'])) {
            $model->attributes = $_POST['Entities'];
            $model->FullName = !$model->FullName ? $model->Name : $model->FullName;
            $back_url = Yii::app()->createAbsoluteUrl("site/".EntityTypes::model()->paths_parts_arr[$model->EntityTypeID]);
            //$back_url = isset($_POST["back_url"]) && $_POST["back_url"] != "" ? Yii::app()->createAbsoluteUrl($_POST["back_url"]) : Yii::app()->createAbsoluteUrl('site/contragents');
            if($model->saveProc()) {
                $this->_outputStatuses($model);
                $this->redirect($back_url);
                Yii::app()->end();
            }
        }
        $model->EntityTypeID = $typeID;
        $model->IsSameNames = true;
        $groups_model = new EntityGroup();
        $groups_model->TypeID = $typeID;
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_entityonlyaddform',
            'title' => 'Добавление объекта "'.EntityTypes::model()->findByPk($typeID)->Name.'"',
            'model' => $model,
            'groups_model' => $groups_model,
        ), false, true);
    }

    /**
     * Добавляет сущность и привязывает её к родителю с переданным ID
     * @param integer $parentid EntityID родителя.
     * @throws 500 Exception, если хотябы один параметр не передан
     */
    public function actionEntityAddUnderParent($parentid=0) {
        if( $parentid==0 )  throw new CHttpException(500,"EntityID is NULL or not transfered");
        $model = new Entities();
        $tree_model = new EntityTree();
        $parent_model = Entities::model()->findByPk($parentid);
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate(array($model,$tree_model));
            Yii::app()->end();
        }
        if (isset($_POST['Entities'])) {
            $model->attributes = $_POST['Entities'];
            $tree_model->attributes = $_POST['EntityTree'];
            if($model->saveProc()) {
                $tree_model->ID = $model->ID;
                $tree_model->EntityName = $model->Name;
                $tree_model->ParentName = $parent_model->Name;
                $tree_model->saveProc();
                $add_result = $this->_outputStatuses(array($model,$tree_model));
                echo json_encode(array(
                    "node" => array(
                        "id"=>$model->ID,
                        "pid"=>$tree_model->ParentID,
                        "name"=>("[".$model->entityType->ShortName."] ".$model->Name)
                    ),
                    "result" => $add_result
                ));
                //$this->redirect(Yii::app()->createAbsoluteUrl('site/treemain'));
                Yii::app()->end();
            }
        }
        $model->parentName = $parent_model->Name;
        $tree_model->ParentID  = $parent_model->ID;
        $model->Enabled = 1;
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_entityform',
            'title' => 'Добавление объекта',
            'model' => $model,
            'tree_model' => $tree_model,
            'parent_model' => $parent_model,
            'js_button_func' => 'ifCheckAddEntityFormOkSubmit',
        ), false, true);
    }

    /**
     * Редактирует привязку сущности
     * @param integer $id ID сущности.
     * @param integer $pid ID родителя сущности.
     * @throws 500 Exception, если хотябы один параметр не передан
     */
    public function actionEntityTreeEdit($id=0,$pid=0) {
        if( $id==0 || $pid==0 )  throw new CHttpException(500,"EntityID is NULL or not transfered");
        $tree_model = EntityTree::model()->findByPk(array("ID"=>$id,"ParentID"=>$pid));
        $parent_model = Entities::model()->findByPk($pid);
        $tree_model->EntityName = $tree_model->entity->Name;
        $tree_model->ParentName = $parent_model->Name;
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($tree_model);
            Yii::app()->end();
        }
        if (isset($_POST['EntityTree'])) {
            $tree_model->attributes = $_POST['EntityTree'];
            if($tree_model->updateProc()) {
                $add_result = $this->_outputStatuses($tree_model);
                echo json_encode(array(
                    "node" => array(
                        "id"=>$id,
                        "pid"=>$pid,
                        "name"=>""
                    ),
                    "result" => $add_result
                ));
                //$this->redirect(Yii::app()->createAbsoluteUrl('site/treemain'));
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_entitytreeform',
            'title' => 'Редактирование связи',
            'tree_model' => $tree_model,
            'js_button_func' => 'ifCheckAddEntityFormOkSubmit',
        ), false, true);
    }

    /**
     * Редактирует сущность
     * @param integer $id ID сущности.
     * @throws 500 Exception, если не передан EntityId или невозможно найти сущность по указанному EntityID
     */
    public function actionEntityEdit($id=0) {
        if( $id==0 )  throw new CHttpException(500,"EntityID is NULL or not transfered");
        $model = Entities::model()->findByPk($id);
        if( $model===null )  throw new CHttpException(500,"Can't find Entity with ID #$id");

        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Entities'])) {
            $model->attributes = $_POST['Entities'];
            $model->FullName = !$model->FullName ? $model->Name : $model->FullName;
            if($model->updateProc()) {
                $res = $this->_outputStatuses($model);
                echo json_encode(array(
                    "node" => array(
                        "id"=>$id,
                    ),
                    'result'=>$res
                ));
                //$this->redirect(Yii::app()->createAbsoluteUrl("site/".EntityTypes::model()->paths_parts_arr[$model->EntityTypeID]));
                Yii::app()->end();
            }
        }
        $groups_model = new EntityGroup();
        $groups_model->TypeID = $model->EntityTypeID;
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_entityeditform',
            'title' => 'Редактирование объекта',
            'model' => $model,
            'groups_model' => $groups_model,
            'js_button_func' => 'ifCheckAddEntityFormOkSubmit',
        ), false, true);
    }

    /**
     * Создание связи
     * @param integer $id ID сущности.
     */
    public function actionEntityRelation($id) {
        $model=Entities::model()->findByPk($id);
        $typeModel=new EntityTypeTree();
        $typeModel->ParentID = $model->EntityTypeID;
        $typeModel->Enabled = 1;
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_entityrelationform',
            'title' => 'Создание связи',
            'model' => $model,
            'typeModel' => $typeModel,
            'js_button_func' => 'ifCheckAddEntityFormOkSubmit',
            //'wideModal2' => true,
            'modalWidth' => 1200,
        ), false, true);
    }

    public function actionEntitiesByType($id) {
        $model=new Entities();
        $group_model = new EntityGroup();
        $group_model->TypeID = $id;
        $model->Enabled = 1;
        $model->EntityTypeID=$id;
        if(isset($_GET['Entities'])) {
            $model->attributes=$_GET['Entities'];
        }
        $this->renderPartial('_entitieslist', array(
            'model'=>$model,
            'group_model'=>$group_model,
        ), false, true);
    }

    public function actionEntityRelationForm($id, $pid) {
        $model = EntityTree::model()->findByPk(array("ID"=>$id,"ParentID"=>$pid));
        if ($hasntRelation = count($model)==0) {
            $model = new EntityTree();
            $model->Count = null;
        }

        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        $entityModel = Entities::model()->findByPk($id);
        $entityModel_parent = Entities::model()->findByPk($pid);
        $model->EntityName = !empty($entityModel->FullName)?$entityModel->FullName:$entityModel->Name;
        $model->ParentName = $entityModel_parent->Name;

        if(isset($_POST['EntityTree'])) {
            $model->Count = $_POST['EntityTree']['Count'];
            $model->ParentID = $pid;
            $model->ID = $id;
            if ($hasntRelation)
                $model->saveProc();
            else
                $model->updateProc();

            $add_result = $this->_outputStatuses(array($model));
            echo json_encode(array(
                "node" => array(
                    "id"=>$id,
                    "pid"=>$pid,
                    "name"=>("[".$entityModel->entityType->ShortName."] ".$entityModel->Name)
                    ),
                "result" => $add_result
            ));
            Yii::app()->end();
        }

        $this->renderPartial('_entityrelationsubform', array(
            'model'=>$model,
        ), false, true);
    }

    public function actionEntityDeleteRelation($id, $pid) {
        $model = EntityTree::model()->findByAttributes(array('ID'=>$id,'ParentID'=>$pid));
        $entityModel = Entities::model()->findByPk($id);
        $entityModel_parent = Entities::model()->findByPk($pid);
        $model->EntityName = $entityModel->Name;
        $model->ParentName = $entityModel_parent->Name;

        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_POST['EntityTree'])) {
            $model->deleteProc();
            $add_result = $this->_outputStatuses(array($model));
            echo json_encode(array(
                "node" => array(
                    "pid"=>$pid,
                ),
                "result" => $add_result
            ));
            Yii::app()->end();
        }

        $this->renderPartial('_entitydeleterelation', array(
            'model'=>$model,
            'js_button_func' => 'ifCheckAddEntityFormOkSubmit',
        ), false, true);
    }

    /**
     * Возвращает содержимое сущности: файлы и металлопрокат
     * @param integer $id ID сущности.
     * @throws 500 Exception, если хотябы один параметр не передан
     */
    public function actionEntityContent($id=0) {
        if($id==0) throw new CHttpException(500,"EntityID is NULL or not transfered");
        $model = Entities::model()->findByPk($id);
        if($model==null) throw new CHttpException(404,"Entity with ID=$id not found");

        //Возьмем контент для списка файлов
        $files_model = new Files();
        $files_model->onlyEnabled = 1;
        $dataSource = new CActiveDataProvider('Files');
        $dataSource->setData($model->enabledFiles);
        $files_content = $this->renderPartial('//files/fileslist',array(
            'model' => $files_model,
            'dataSource' => $dataSource,
            'type' => 'entity',
            'id'=>$id,
        ), true, false);

        //Возьмем контент для списка металопроката
        $metals_model = new EntitiesMetals();
        $metals_model->EntityID = $id;
        $metalsTwoDims = array();
        foreach( Metals::model()->findAll() as $obj ) {
            if( $obj->MetalWeightTypeID == MetalWeightTypes::SQUARE_METER )
                $metalsTwoDims[$obj->ID] = 1;
        }
        $metals_content = $this->renderPartial('EntitiesMetalsList',array(
            'model' => $metals_model,
            'entity_model' => $model,
            'isAll' => false,
            'dp' => $metals_model->getEntitiesAsDataProvider(),
            'metalsTwoDims' => $metalsTwoDims
        ), true, false);
        $this->renderPartial('entityContent',array(
            'model' => $model,
            'files_content' => $files_content,
            'metals_content' => $metals_content,
            'can_add_enities' => (EntityTypeTree::model()->count("ParentID = :id", array(":id"=>$model->EntityTypeID)) > 0 ? true : false)
        ), false, true);
    }

    /**
     * Выводим попап с формой добавления новой записи в EntitiesMetals
     * @param integer $entityid ID сущности
     */
    public function actionAddEntityMetal($entityid) {
        $model = new EntitiesMetals();
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm-entityadd') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        $metalsTwoDims = array();
        foreach( Metals::model()->findAll() as $obj ) {
            if( $obj->MetalWeightTypeID == MetalWeightTypes::SQUARE_METER )
                $metalsTwoDims[$obj->ID] = 1;
        }
        $model->EntityID = $entityid;
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_entitymetalform',
            'title' => 'Добавление нового изделия металопроката',
            'model' => $model,
            'metalsTwoDims' => $metalsTwoDims
        ), false, true);
    }
    /**
     * Сохраняем новую запись в EntitiesMetals
     */
    public function actionAddEntityMetal_ajax() {
        $model = new EntitiesMetals();
        if (isset($_POST['EntitiesMetals'])) {
            $model->attributes = $_POST['EntitiesMetals'];
            if($model->saveProc()) {
                $res = $this->_outputStatuses($model,true);
                echo json_encode($res);
                Yii::app()->end();
            }
        }
    }

    /**
     * Выводим попап с формой редактирования записи в EntitiesMetals
     * @param integer $entityid ID сущности
     * @param integer $metalid ID метализделия
     */
    public function actionEditEntityMetal($id) {
        $model = EntitiesMetals::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        $model->HiddenMetalID = $model->MetalID;
        $this->renderPartial('_entitymetalform',array(
            'title' => 'Редактирование изделия металопроката',
            'model' => $model
        ), false, true);
    }
    /**
     * Сохраняем новую запись в EntitiesMetals
     */
    public function actionEditEntityMetal_ajax() {
        if (isset($_POST['EntitiesMetals'])) {
            $model = EntitiesMetals::model()->findByPk($_POST['EntitiesMetals']['ID']);
            $model->attributes = $_POST['EntitiesMetals'];
            if($model->updateProc()) {
                $res = $this->_outputStatuses($model,true);
                echo json_encode($res);
                Yii::app()->end();
            }
        }
    }

    /**
     * Получаем список файлов по ID сущности
     * @param integer $id ID сущности
     */
    public function actionEntityMetalsList_ajax($id,$isAll=false) {
        $model = new EntitiesMetals();
        $model->EntityID = $id;
        $model->isAll = $isAll;
        $dp = $model->getEntitiesAsDataProvider($isAll);
        $metalsTwoDims = array();
        foreach( Metals::model()->findAll() as $obj ) {
            if( $obj->MetalWeightTypeID == MetalWeightTypes::SQUARE_METER )
                $metalsTwoDims[$obj->ID] = 1;
        }
        //$entity_model = Entities::model()->findByPk($id);
        $this->renderPartial('EntitiesMetalsList',array(
            'model' => $model,
            'isAll' => $isAll,
            'dp' => $dp,
            'metalsTwoDims'=>$metalsTwoDims,
            //'entity_model' => $entity_model,
        ), false, true);
    }
    /** РАБОТАЕМ СО СПРАВОЧНИКАМИ */

    //Получение списка заказчиков
    public function actionContragents() {
        $model = new Contragents('search');
        $this->pageSizeProccess($model,$_GET);
        if(isset($_GET['Contragents'])) {
            $model->attributes=$_GET['Contragents'];
        }
        $this->render('contragents',array(
            'model' => $model
        ));
    }
    //Работа с новым заказчиком
    public function actionContragentAdd() {
        $model = new Contragents('Reg');
        $contactModel = new Contacts('Reg');
        if (isset($_POST['Contragents']['sameNameFlag']) && $_POST['Contragents']['sameNameFlag'] && isset($_POST['Contragents']['Name']))
            $model->FullName = $_POST['Contragents']['Name'];
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate(array($model, $contactModel));
            Yii::app()->end();
        }
        if (isset($_POST['Contragents'])) {
            $model->attributes = $_POST['Contragents'];
            if($model->saveProc()) {
                $contactModel->attributes = $_POST['Contacts'];
                $contactModel->ContragentID = $model->ID;
                if ($contactModel->saveProc()) {
                    $this->_outputStatuses(array($model, $contactModel));
                    $this->redirect(Yii::app()->createAbsoluteUrl('site/contragents'));
                    Yii::app()->end();
                }
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_contragentform',
            'title' => 'Добавление заказчика',
            'model' => $model,
            'contactModel' => $contactModel,
            'wideModal2' => true,
        ), false, true);
    }
    //Редактирование заказчика
    public function actionContragentEdit($id) {
        $model = Contragents::model()->findByPk($id);
        $model->setScenario('Edit');
        if (isset($_POST['Contragents']['sameNameFlag']) && $_POST['Contragents']['sameNameFlag'] && isset($_POST['Contragents']['Name']))
            $model->FullName = $_POST['Contragents']['Name'];
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Contragents'])) {
            $model->attributes = $_POST['Contragents'];
            if($model->updateProc()) {
                $this->_outputStatuses($model);
                $this->redirect(Yii::app()->createAbsoluteUrl('site/contragents'));
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_contragentform',
            'title' => 'Редактирование заказчика',
            'model' => $model,
            'id' => $id
        ), false, true);
    }

    //Получение списка контактов
    public function actionContacts() {
        $model = new Contacts('search');
        $this->pageSizeProccess($model,$_GET);
        if(isset($_GET['Contacts'])) {
            $model->attributes=$_GET['Contacts'];
        }
        $this->render('contacts',array(
            'model' => $model
        ));
    }

    //Контакты выбранного заказчика
    public function actionContragentContacts($id) {
        $model = new Contacts('search');
        $contragentModel = Contragents::model()->findByPk($id);
        $this->pageSizeProccess($model,$_GET);
        $model->ContragentID = $id;
        $this->render('contacts',array(
            'model' => $model,
            'contragentModel' => $contragentModel,
        ));
    }

    //Работа с новым контактом
    public function actionContactAdd($contragentID = null) {
        $model = new Contacts('Reg');
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Contacts'])) {
            $model->attributes = $_POST['Contacts'];
            if ($contragentID !== null)
                $model->ContragentID = trim($contragentID,"/\\");
            if($model->saveProc()) {
                $this->_outputStatuses($model);
                if ($contragentID !== null)
                    $this->redirect(Yii::app()->createAbsoluteUrl('site/contragentcontacts/'.$model->ContragentID));
                else
                    $this->redirect(Yii::app()->createAbsoluteUrl('site/contacts'));
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_contactform',
            'title' => 'Добавление контакта',
            'model' => $model,
            'contragentID' => $contragentID,
        ), false, true);
    }
    //Редактирование заказчика
    public function actionContactEdit($id) {
        $model = Contacts::model()->findByPk($id);
        $model->setScenario('Edit');
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Contacts'])) {
            $model->attributes = $_POST['Contacts'];
            if($model->updateProc()) {
                $this->_outputStatuses($model);
                $this->redirect(Yii::app()->createAbsoluteUrl('site/contacts'));
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_contactform',
            'title' => 'Редактирование контакта',
            'model' => $model,
            'id' => $id
        ), false, true);
    }

    /**  Получение списка продуктов металлопроката */
    public function actionMetal() {
        $model = new Metals('search');

        $this->pageSizeProccess($model,$_GET); //unset attributes included
        if(isset($_GET['Metals'])) {
            $model->attributes=$_GET['Metals'];
        }
        if (isset($_POST['Metals'])) {
            Yii::app()->session->add("userSets[onlyEnabled]", $_POST['Metals']['onlyEnabled']);
        }
        $model->onlyEnabled = Yii::app()->session->get("userSets[onlyEnabled]");
        if ($model->onlyEnabled) $model->Enabled = $model->onlyEnabled;
        $this->render('metalslist',array(
            'model' => $model
        ));
    }
    /** Возвращает HTML-код c инпутами для свойств метализделия в зависимости от типа
     * @param $id integer ID типа метализделия
     * @throws 404 если не найден тип по переданному ID
     */
    public function actionPropertiesInputsByMetalTypeID($id) {
        //$model=MetalTypesMetalPropertyTypes::model()->findAll("MetalTypeID = :mtid", array(":mtid"=>$id))
        //$model=MetalTypes::model()->findByPk($id);
        $model = new Properties;
        $model->loadData($id);

        $this->renderPartial('mtypeproperties',array(
            'model' => $model
        ), false, false);
    }
    /** Добавление метал изделия */
    public function actionMetalAdd() {
        $model = new Metals();
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            if( $_POST["Metals"]["MetalTypeID"] && isset($_POST["Properties"]["values"])) {
                $prop_model = new Properties();
                $prop_model->loadData($_POST["Metals"]["MetalTypeID"]);
                $prop_model->values = $_POST["Properties"]["values"];
                $mod_arr = array($model,$prop_model);
            }
            else
                $mod_arr = $model;
            echo CActiveForm::validate($mod_arr);
            Yii::app()->end();
        }
        if (isset($_POST['Metals'])) {
            $model->attributes = $_POST['Metals'];
            $model->Properties = $_POST['Properties']['values'];
            if($model->saveProc()) {
                $this->_outputStatuses($model);
                $this->redirect(Yii::app()->createAbsoluteUrl('site/metal'));
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_metalform',
            'title' => 'Добавление металличекого изделия',
            'model' => $model,
            'wideModal2' => true,
        ), false, true);
    }

    /** Редактирование метализделия
     * @param integer $id ID метализделия
     * @throws 404 если не найден тип по переданному ID
     */
    public function actionMetalEdit($id) {
        $model=Metals::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'Не найдено метализделие с ID:'.$id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            if( $_POST["Metals"]["MetalTypeID"] && isset($_POST["Properties"]["values"])) {
                $prop_model = new Properties();
                $prop_model->loadData($_POST["Metals"]["MetalTypeID"]);
                $prop_model->values = $_POST["Properties"]["values"];
                $mod_arr = array($model,$prop_model);
            }
            else
                $mod_arr = $model;
            echo CActiveForm::validate($mod_arr);
            Yii::app()->end();
        }
        if (isset($_POST['Metals'])) {
            $model->attributes = $_POST['Metals'];
            $model->Properties = $_POST['Properties']['values'];
            if($model->updateProc()) {
                $this->_outputStatuses($model);
                $this->redirect(Yii::app()->createAbsoluteUrl('site/metal'));
                Yii::app()->end();
            }
        }
       /* $setPropArr = array();
        //получим установленные свойства
        foreach( $model->metalProperties as $obj ) {
            $setPropArr[$obj->MetalPropertyTypeID] = $obj->Value;
        }*/
        $properties_model = new Properties;
        $properties_model->loadData($model->MetalTypeID);
        $properties_model->loadValues($model);
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_metalform',
            'title' => 'Редактирование металличекого изделия',
            'model' => $model,
            'properties_model' => $properties_model,
            'wideModal2' => true,
            //'setProperties' => $setPropArr,
        ), false, true);
    }

    /**
     * Возвращает набор сущностей определенного типа
     * @param integer $id EntityTypeID.
     * @return array of CActiveRecord
     */
    private function _getEntitiesByTypeID($id) {
        return Entities::model()->findAll("EntityTypeID = :etid",array(":etid"=>$id));
    }

    public function actionProductsDir() {
        $model = new Entities('search');
        $group_model = new EntityGroup();
        $group_model->TypeID = EntityTypes::PRODUCT;
        if (isset($_GET['Entities']))
            $model->attributes = $_GET['Entities'];
        if (isset($_POST['Entities'])) {
            Yii::app()->session->add("userSets[EntitiesOnlyEnabled]", $_POST['Entities']['onlyEnabled']);
        }
        $model->onlyEnabled = Yii::app()->session->get("userSets[EntitiesOnlyEnabled]", true);
        if ($model->onlyEnabled) $model->Enabled = $model->onlyEnabled;
        $this->render('directory_list',array(
            'title' => 'Справочник изделий',
            'model' => $model,
            'group_model' => $group_model,
            'dataProvider' => $model->products()->search(),
            'entityType' => EntityTypes::PRODUCT
        ));
    }

    public function actionAssemblyDir() {
        $model = new Entities('search');
        $group_model = new EntityGroup();
        $group_model->TypeID = EntityTypes::ASSEMBLY;
        if (isset($_GET['Entities']))
            $model->attributes = $_GET['Entities'];
        if (isset($_POST['Entities'])) {
            Yii::app()->session->add("userSets[EntitiesOnlyEnabled]", $_POST['Entities']['onlyEnabled']);
        }
        $model->onlyEnabled = Yii::app()->session->get("userSets[EntitiesOnlyEnabled]", true);
        if ($model->onlyEnabled) $model->Enabled = $model->onlyEnabled;
        $this->render('directory_list',array(
            'title' => 'Справочник сборок',
            'model' => $model,
            'group_model' => $group_model,
            'dataProvider' => $model->assemblies()->search(),
            'entityType' => EntityTypes::ASSEMBLY
        ));
    }

    public function actionShippingDir() {
        $model = new Entities('search');
        $group_model = new EntityGroup();
        $group_model->TypeID = EntityTypes::SHIPPING;
        if (isset($_GET['Entities']))
            $model->attributes = $_GET['Entities'];
        if (isset($_POST['Entities'])) {
            Yii::app()->session->add("userSets[EntitiesOnlyEnabled]", $_POST['Entities']['onlyEnabled']);
        }
        $model->onlyEnabled = Yii::app()->session->get("userSets[EntitiesOnlyEnabled]", true);
        if ($model->onlyEnabled) $model->Enabled = $model->onlyEnabled;
        $this->render('directory_list',array(
            'title' => 'Справочник отгрузочных марок',
            'model' => $model,
            'group_model' => $group_model,
            'dataProvider' => $model->shipmodels()->search(),
            'entityType' => EntityTypes::SHIPPING
        ));
    }

    public function actionComponentsDir() {
        $model = new Entities('search');
        $group_model = new EntityGroup();
        $group_model->TypeID = EntityTypes::COMPONENTS;
        if (isset($_GET['Entities']))
            $model->attributes = $_GET['Entities'];
        if (isset($_POST['Entities'])) {
            Yii::app()->session->add("userSets[EntitiesOnlyEnabled]", $_POST['Entities']['onlyEnabled']);
        }
        $model->onlyEnabled = Yii::app()->session->get("userSets[EntitiesOnlyEnabled]", true);
        if ($model->onlyEnabled) $model->Enabled = $model->onlyEnabled;
        $this->render('directory_list',array(
            'title' => 'Справочник комплектующих',
            'model' => $model,
            'group_model' => $group_model,
            'dataProvider' => $model->components()->search(),
            'entityType' => EntityTypes::COMPONENTS
        ));
    }

    public function actionComponentsSetsDir() {
        $model = new Entities('search');
        $group_model = new EntityGroup();
        $group_model->TypeID = EntityTypes::COMPONENTS_SETS;
        if (isset($_GET['Entities']))
            $model->attributes = $_GET['Entities'];
        if (isset($_POST['Entities'])) {
            Yii::app()->session->add("userSets[EntitiesOnlyEnabled]", $_POST['Entities']['onlyEnabled']);
        }
        $model->onlyEnabled = Yii::app()->session->get("userSets[EntitiesOnlyEnabled]", true);
        if ($model->onlyEnabled) $model->Enabled = $model->onlyEnabled;
        $this->render('directory_list',array(
            'title' => 'Справочник наборов комплектующих',
            'model' => $model,
            'group_model' => $group_model,
            'dataProvider' => $model->compsets()->search(),
            'entityType' => EntityTypes::COMPONENTS_SETS
        ));
    }

    public function actionMetalStandarts() {
        $model = new MetalStandarts('search');
        $this->pageSizeProccess($model,$_GET);
        if (isset($_GET['MetalStandarts']))
            $model->attributes = $_GET['MetalStandarts'];
        $this->render('metalstandarts',array(
            'model' => $model,
        ));
    }

    public function actionMetalStandartAdd() {
        $model = new MetalStandarts();
        $fileModel = new Files('metalstandart_add');
        $fileModel->addToType = 'metalstandart';
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate(array($model,$fileModel));
            Yii::app()->end();
        }
        if (isset($_POST['MetalStandarts'])) {
            $model->attributes = $_POST['MetalStandarts'];
            if($model->saveProc()) {
                if (isset($_POST['Files']['Name'])&&!empty($_POST['Files']['Name'])&&$model->ID) {
                    // Сохранение файла, если указано имя
                    $fileModel->attributes = $_POST['Files'];
                    if($fileModel->saveProc()) {
                        $fileModel->linkToMetalStandart($model->ID);
                        $fileModel->saveFileContentProc(); //пытаемся сохранить контент файла, если он был загружен
                    }
                }
                $this->_outputStatuses(array($model,$fileModel));
                $this->redirect(Yii::app()->createAbsoluteUrl("site/metalstandarts"));
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_metalstandartform',
            'title' => 'Добавление стандарта на профиль',
            'model' => $model,
            'fileModel' => $fileModel,
            'isNewRecord' => true,
        ), false, true);
    }

    public function actionMetalStandartEdit($id) {
        $model = MetalStandarts::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['MetalStandarts'])) {
            $model->attributes = $_POST['MetalStandarts'];
            $back_url = Yii::app()->createAbsoluteUrl("site/metalstandarts");
            if($model->updateProc()) {
                $this->_outputStatuses($model);
                $this->redirect($back_url);
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_metalstandartform',
            'title' => 'Редактирование стандарта на профиль',
            'model' => $model,
        ), false, true);
    }

    // Список марок металлопроката
    public function actionMetalMarkTypes() {
        $model = new MetalMarkTypes();
        $this->pageSizeProccess($model,$_GET);
        if (isset($_GET['MetalMarkTypes']))
            $model->attributes = $_GET['MetalMarkTypes'];
        $this->render('metalmarktypes',array(
            'model' => $model
        ));
    }
    // Создание марки
    public function actionMarkTypeAdd() {
        $model = new MetalMarkTypes();
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['MetalMarkTypes'])) {
            $model->attributes = $_POST['MetalMarkTypes'];
            if($model->saveProc()) {
                $this->_outputStatuses($model);
                $this->redirect(Yii::app()->createAbsoluteUrl('site/metalmarktypes'));
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_metalmarktypeform',
            'title' => 'Добавление марки',
            'model' => $model
        ), false, true);
    }
    // Редактирование марки
    public function actionMarkTypeEdit($id) {
        $model = MetalMarkTypes::model()->findByPk($id);
        $model->setScenario('Edit');
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['MetalMarkTypes'])) {
            $model->attributes = $_POST['MetalMarkTypes'];
            if($model->updateProc()) {
                $this->_outputStatuses($model);
                $this->redirect(Yii::app()->createAbsoluteUrl('site/metalmarktypes'));
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_metalmarktypeform',
            'title' => 'Редактирование марки',
            'model' => $model,
            'id' => $id
        ), false, true);
    }

    public function actionMetalMarkTypeStandarts() {
        $model = new MetalMarkTypeStandarts();
        $this->pageSizeProccess($model,$_GET);
        if (isset($_GET['MetalMarkTypeStandarts']))
            $model->attributes = $_GET['MetalMarkTypeStandarts'];
        $this->render('metalmarktypestandarts',array(
            'model' => $model,
        ));
    }

    public function actionMetalMarkTypeStandartAdd() {
        $model = new MetalMarkTypeStandarts();
        $fileModel = new Files('metalmarktypestandart_add');
        $fileModel->addToType = 'metalmarktypestandart';
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate(array($model,$fileModel));
            Yii::app()->end();
        }
        if (isset($_POST['MetalMarkTypeStandarts'])) {
            $model->attributes = $_POST['MetalMarkTypeStandarts'];
            if($model->saveProc()) {
                if (isset($_POST['Files']['Name'])&&!empty($_POST['Files']['Name'])&&$model->ID) {
                    // Сохранение файла, если указано имя
                    $fileModel->attributes = $_POST['Files'];
                    if($fileModel->saveProc()) {
                        $fileModel->linkToMetalMarkTypeStandart($model->ID);
                        $fileModel->saveFileContentProc(); //пытаемся сохранить контент файла, если он был загружен
                    }
                }
                $this->_outputStatuses(array($model,$fileModel));
                $this->redirect(Yii::app()->createAbsoluteUrl("site/metalmarktypestandarts"));
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_metalmarktypestandartform',
            'title' => 'Добавление стандарта на марку',
            'model' => $model,
            'fileModel' => $fileModel,
            'isNewRecord' => true,
        ), false, true);
    }

    public function actionMetalMarkTypeStandartEdit($id) {
        $model = MetalMarkTypeStandarts::model()->findByPk($id);
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['MetalMarkTypeStandarts'])) {
            $model->attributes = $_POST['MetalMarkTypeStandarts'];
            $back_url = Yii::app()->createAbsoluteUrl("site/metalmarktypestandarts");
            if($model->updateProc()) {
                $this->_outputStatuses($model);
                $this->redirect($back_url);
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_metalmarktypestandartform',
            'title' => 'Редактирование стандарта на марку',
            'model' => $model,
        ), false, true);
    }

    public function actionEntityGroups() {
        $model = new EntityTypes();
        $grp_model = new EntityGroup();
        $grp_model->onlyEnabled = 1;
        $this->render('entitygroups',array(
            'title' => 'Группы объектов',
            'model' => $model,
            'grp_model' => $grp_model,
            'dataProvider' => $model->search(),
        ));
    }

    public function actionEntityGroupAdd($typeID) {
        $model = new EntityGroup();
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['EntityGroup'])) {
            $model->attributes = $_POST['EntityGroup'];
            if($model->saveProc()) {
                $res = $this->_outputStatuses($model,true);
                echo json_encode($res);
                Yii::app()->end();
            }
        }
        $model->TypeID = $typeID;
        $model->Enabled = 1;
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_entitygroupform',
            'title' => 'Добавление группы "'.EntityTypes::model()->findByPk($typeID)->Name.'"',
            'model' => $model,
        ), false, true);
    }

    public function actionEntityGroupEdit($groupID) {
        $model = EntityGroup::model()->findByPk($groupID);
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['EntityGroup'])) {
            $model->attributes = $_POST['EntityGroup'];
            if($model->updateProc()) {
                $res = $this->_outputStatuses($model,true);
                echo json_encode($res);
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_entitygroupform',
            'title' => 'Редактирование группы',
            'model' => $model,
        ), false, true);
    }

    public function actionChangeEntityState($id, $act) {
        $model = Entities::model()->findByPk($id);

        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Entities'])) {
            switch ($act) {
                case 'enable':
                    $model->enableProc();break;
                case 'disable':
                    $model->disableProc();break;
            }
            $back_url = Yii::app()->createAbsoluteUrl("site/".EntityTypes::model()->paths_parts_arr[$model->EntityTypeID]);
            $this->_outputStatuses($model);
            $this->redirect($back_url);
            /*echo json_encode(array(
                "node" => array(
                    "id"=>$id,
                ),
                "result" => $res,
            ));*/
            Yii::app()->end();
        }
        $this->renderPartial('_entitychangestate', array(
            'model'=>$model,
            //'js_button_func' => 'ifCheckAddEntityFormOkSubmit',
        ), false, true);
    }

    public function actionLocations() {
        $model = new Location('search');
        $this->pageSizeProccess($model,$_GET);
        if(isset($_GET['Location'])) {
            $model->attributes=$_GET['Location'];
        }
        $this->render('locations',array(
            'model' => $model
        ));
    }

    public function actionLocationAdd() {
        $model = new Location();
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Location'])) {
            $model->attributes = $_POST['Location'];
            if($model->saveProc()) {
                $this->_outputStatuses($model);
                $this->redirect(Yii::app()->createAbsoluteUrl('site/locations'));
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_locationform',
            'title' => 'Добавление объекта',
            'model' => $model
        ), false, true);
    }

    public function actionLocationEdit($id) {
        $model = Location::model()->findByPk($id);
        $model->setScenario('Edit');
        if(isset($_POST['ajax']) && $_POST['ajax']==='dataForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['Location'])) {
            $model->attributes = $_POST['Location'];
            if($model->updateProc()) {
                $this->_outputStatuses($model);
                $this->redirect(Yii::app()->createAbsoluteUrl('site/locations'));
                Yii::app()->end();
            }
        }
        $this->renderPartial('//layouts/modal_form',array(
            'name'  => '_locationform',
            'title' => 'Редактирование объекта',
            'model' => $model,
            'id' => $id
        ), false, true);
    }
}