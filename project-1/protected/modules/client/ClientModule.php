<?php

class ClientModule extends yupe\components\WebModule
{
    const VERSION = '0.1';

    public function getVersion()
    {
        return self::VERSION;
    }

    public function getIsInstallDefault()
    {
        return false;
    }

    public function getCategory()
    {
        return Yii::t('ClientModule.client', 'Clients');
    }

    public function getName()
    {
        return Yii::t('ClientModule.client', 'Clients');
    }

    public function getDescription()
    {
        return Yii::t('ClientModule.client', 'Custom module Clients');
    }

    public function getAuthor()
    {
        return Yii::t('ClientModule.client', 'Sergey Sladkov');
    }

    public function getAuthorEmail()
    {
        return Yii::t('ClientModule.client', 'me@ssladkov.ru');
    }

    public function getUrl()
    {
        return Yii::t('ClientModule.client', 'http://ssladkov.ru');
    }
    public function getAdminPageLink()
    {
        return '/client/clientBackend/index';
    }
    public function getIcon()
    {
        return "fa fa-fw fa-users";
    }


    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Clients list'),
                'url' => ['/client/clientBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'New client'),
                'url' => ['/client/clientBackend/create']
            ],
            ['label' => Yii::t('ClientModule.client', 'Client requests')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Requests manage'),
                'url' => ['/client/requestBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'New request'),
                'url' => ['/client/requestBackend/create']
            ],
            ['label' => Yii::t('ClientModule.client', 'Feedback forms')],
            [
                'icon' => 'fa fa-fw fa-check-square-o',
                'label' => Yii::t('ClientModule.client', 'Fields manage'),
                'url' => ['/client/FormFieldBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-cogs',
                'label' => Yii::t('ClientModule.client', 'Form constructor'),
                'url' => ['/client/FormConstructorBackend/index']
            ],

        ];
    }

    public function getAuthItems()
    {
        return [
            [
                'name' => 'Client.ClientManager',
                'description' => Yii::t('ClientModule.client', 'Manage clients'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    //clients
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.ClientBackend.Create',
                        'description' => Yii::t('ClientModule.client', 'Creating client')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.ClientBackend.Delete',
                        'description' => Yii::t('ClientModule.client', 'Removing client')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.ClientBackend.Index',
                        'description' => Yii::t('ClientModule.client', 'List of clients')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.ClientBackend.Update',
                        'description' => Yii::t('ClientModule.client', 'Editing client')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.ClientBackend.View',
                        'description' => Yii::t('ClientModule.client', 'Viewing clients')
                    ],
                    //form fields
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormFieldBackend.Create',
                        'description' => Yii::t('ClientModule.client', 'Creating form field')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormFieldBackend.Delete',
                        'description' => Yii::t('ClientModule.client', 'Removing form field')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormFieldBackend.Index',
                        'description' => Yii::t('ClientModule.client', 'List of form fields')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormFieldBackend.Update',
                        'description' => Yii::t('ClientModule.client', 'Editing form field')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormFieldBackend.View',
                        'description' => Yii::t('ClientModule.client', 'Viewing form fields')
                    ],

                    //form constructor
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormConstructorBackend.Create',
                        'description' => Yii::t('ClientModule.client', 'Creating form')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormConstructorBackend.Delete',
                        'description' => Yii::t('ClientModule.client', 'Removing form')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormConstructorBackend.Index',
                        'description' => Yii::t('ClientModule.client', 'List of forms')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormConstructorBackend.Update',
                        'description' => Yii::t('ClientModule.client', 'Editing form')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormConstructorBackend.View',
                        'description' => Yii::t('ClientModule.client', 'Viewing form')
                    ],

                    //form constructor to field
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormConstructorToFieldBackend.Create',
                        'description' => Yii::t('ClientModule.client', 'Creating form constructor field')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormConstructorToFieldBackend.Delete',
                        'description' => Yii::t('ClientModule.client', 'Removing form constructor field')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormConstructorToFieldBackend.Index',
                        'description' => Yii::t('ClientModule.client', 'List of form constructor fields')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormConstructorToFieldBackend.Update',
                        'description' => Yii::t('ClientModule.client', 'Editing form constructor field')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.FormConstructorToFieldBackend.View',
                        'description' => Yii::t('ClientModule.client', 'Viewing form constructor field')
                    ],

                    //request
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.RequestBackend.Create',
                        'description' => Yii::t('ClientModule.client', 'Creating request')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.RequestBackend.Delete',
                        'description' => Yii::t('ClientModule.client', 'Removing request')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.RequestBackend.Index',
                        'description' => Yii::t('ClientModule.client', 'List of requests')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.RequestBackend.Update',
                        'description' => Yii::t('ClientModule.client', 'Editing request')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Client.RequestBackend.View',
                        'description' => Yii::t('ClientModule.client', 'Viewing request')
                    ],

                ]
            ]
        ];
    }

    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'application.modules.client.models.*',
                'application.modules.client.widgets.*'
            ]
        );

        // Если у модуля не задан редактор - спросим у ядра
        if (!$this->editor) {
            $this->editor = Yii::app()->getModule('yupe')->editor;
        }
    }

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
