<?php
/**
 * Отображение для RequestForm/create:
 *
 * @category YupeView
 * @package  client
 * @author   Sladkov Sergey <me@ssladkov.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://ssladkov.ru
 ***/
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><?=$model->formTitle;?></h4>
</div>
<?php
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm',
    [
        'id'                     => 'request-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'afterValidate'=>'js:function(form,data,hasErrors) {
                return requestFormSubmitAjax(form, hasErrors, "'.Yii::app()->createUrl('addrequest/'.$form_code).'", "'.$modal_id.'","main-modal-content" );
            }'
        ),
        'type'                   => 'vertical'
    ]
);?>
<div class="modal-body">
    <div class="alert alert-info">
        <div class="row">
            <div class="col-md-1 col-sm-2 col-xs-2"><span class="glyphicon glyphicon-info-sign font-size-large color-white"></span></div>
            <div class="col-md-11 col-sm-10 col-xs-10">
                <?php if( $model->formTextBefore) {
                    echo $model->formTextBefore;
                } ?>
                <strong>
                    <?php echo Yii::t('ClientModule.client', 'Fields marked with'); ?>
                    <span class="required">*</span>
                    <?php echo Yii::t('ClientModule.client', 'are required.'); ?>
                </strong>
            </div>
        </div>
    </div>
    <?php
    /** создаем форму динамически  */
    foreach($model->getFields() as $fieldName => $fieldData) {
        /** если FormField.code == input || input-email */
        if( $fieldData["type"] == "input" || $fieldData["type"] == "input-email" ) {
            echo $form->textFieldGroup($model, $fieldName, [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName)
                    ]
                ]
            ]);
        }
        /** если FormField.code == input-phone */
        elseif( $fieldData["type"] == "input-phone" ) {
            echo '<div class="form-group">';
            echo $form->labelEx($model, $fieldName, ['class' => 'control-label']);
            $this->widget(
                'CMaskedTextField',
                [
                    'model' => $model,
                    'attribute' => $fieldName,
                    'mask' => $fieldData["mask"],
                    'placeholder' => 'X',
                    'htmlOptions' => [
                        'class' => 'form-control'
                    ]
                ]
            );
            echo $form->error($model,$fieldName);
            echo '</div>';
        }
        /** если FormField.code == textarea  */
        elseif( $fieldData["type"] == "textarea" ) {
            echo $form->textAreaGroup(
                $model,
                $fieldName, [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'rows' => 4,
                            'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName)
                        ]
                    ]
                ]
            );
        }
    }
    ?>
</div>
<div class="modal-footer">
    <?php if( $model->formAddLink ) {
        echo $model->formAddLink;
    } ?>
    <button type="button" class="btn btn-default" data-dismiss="modal"><?=Yii::t('ClientModule.client', 'Close');?></button>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton',
        [
            'buttonType' => 'submit',
            'context'    => 'success',
            'label'      => $model->formSubmitBtnText,
        ]
    ); ?>

</div>
<?php $this->endWidget(); ?>