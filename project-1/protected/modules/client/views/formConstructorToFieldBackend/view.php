<?php
/**
 * Отображение для FormFieldBackend/view:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Form constructor') => ['/client/formConstructorBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('ClientModule.client', 'Forms - view');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Form constructor'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage forms'),
                'url'   => ['/client/formConstructorBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a form'),
                'url'   => ['/client/formConstructorBackend/create']
            ],
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Viewing form'); ?><br/>
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget(
    'bootstrap.widgets.TbDetailView',
    [
        'data'       => $model,
        'attributes' => [
            'id',
            'code',
            'name'
        ],
    ]
); ?>
