<?php
/**
 * Отображение для formConstructorToFieldBackend/index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Form constructor field') => ['/client/formConstructorToFieldBackend/index'],
    Yii::t('ClientModule.client', 'Administration'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Forms - administration');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Form constructor'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage forms'),
                'url'   => ['/client/formConstructorBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a form'),
                'url'   => ['/client/formConstructorBackend/create']
            ],
            'label' => Yii::t('ClientModule.client', 'Form constructor field'),
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage form constructor fields'),
                'url'   => ['/client/formConstructorToFieldBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add form constructor field'),
                'url'   => ['/client/formConstructorToFieldBackend/create']
            ],
        ]
    ],
];
?>

<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Form constructor field'); ?>
        <small><?php echo Yii::t('ClientModule.client', 'Administration'); ?></small>
    </h1>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'form-constructor-fields-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            [
                'name'   => 'form_constructor_id',
                'value'  => '$data->formConstructor->name',
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'form_constructor_id',
                    FormConstructor::model()->getFormsList(),
                    ['encode' => false, 'empty' => '', 'class' => 'form-control']
                )
            ],
            [
                'name'   => 'form_field_id',
                'value'  => '$data->formField->name',
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'form_field_id',
                    FormField::model()->getFieldsList(),
                    ['encode' => false, 'empty' => '', 'class' => 'form-control']
                )
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'label',
                'editable' => [
                    'url'    => $this->createUrl('/client/formConstructorToFieldBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'label', ['class' => 'form-control']),
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'placeholder',
                'editable' => [
                    'url'    => $this->createUrl('/client/formConstructorToFieldBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'placeholder', ['class' => 'form-control']),
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'order',
                'editable' => [
                    'url'    => $this->createUrl('/client/formConstructorToFieldBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'order', ['class' => 'form-control']),
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]
); ?>
