<?php
/**
 * Отображение для FormConstructorToFieldBackend/_form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $form \yupe\widgets\ActiveForm
 * @var $model FormConstructorToField
 * @var $this FormConstructorToFieldBackendController
 ***/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm',
    [
        'id'                     => 'formConstructorToField-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => ['class' => 'well'],
    ]
);

?>
<div class="alert alert-info">
    <?php echo Yii::t('ClientModule.client', 'Fields marked with'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('ClientModule.client', 'are required.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'form_constructor_id',
            [
                'widgetOptions' => [
                    'data'        =>  FormConstructor::model()->getFormsList(),
                    'htmlOptions' => [
                        'empty' => Yii::t('ClientModule.client', '--choose--'),
                    ],
                ]
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'form_field_id',
            [
                'widgetOptions' => [
                    'data'        =>  FormField::model()->getFieldsList(),
                    'htmlOptions' => [
                        'empty' => Yii::t('ClientModule.client', '--choose--'),
                    ],
                ]
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'label'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'placeholder'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'order'); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'maxlength'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'not_empty',
            [
                'widgetOptions' => [
                    'data'        => $model->getYesNoList(),
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-8">
        <?php
        echo $form->textAreaGroup(
            $model,
            'default', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'rows' => 4
                    ]
                ]
            ]
        );
        ?>
    </div>
</div>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => $model->isNewRecord ? Yii::t('ClientModule.client', 'Create form constructor field and continue') : Yii::t(
            'ClientModule.client',
            'Save form constructor field and continue'
        ),
    ]
); ?>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType'  => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label'       => $model->isNewRecord ? Yii::t('ClientModule.client', 'Create form constructor field and close') : Yii::t(
            'ClientModule.client',
            'Save form constructor field and close'
        ),
    ]
); ?>

<?php $this->endWidget(); ?>
