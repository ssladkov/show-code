<html lang="<?php echo Yii::app()->language; ?>">
<head>
	<meta charset="<?php echo Yii::app()->charset; ?>"/>
	<style type="text/css">
		body {
			font-family: "Trebuchet MS",Arial,Tahoma,Verdana,sans-serif;
			font-size: 14px;
			font-weight: normal;
		}
	</style>
</head>
<body>
	<h3>Поступило новое резюме с сайта:</h3>
    <p><strong>Имя:</strong> <?=$data["name"];?></p>
    <p><strong>Email:</strong> <?=$data["email"];?></p>
    <?php if( $data["phone"] ) : ?>
        <p><strong>Телефон:</strong> <?=$data["phone"];?></p>
    <?php endif; ?>
</body>
</html>