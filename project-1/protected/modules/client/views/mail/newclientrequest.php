<html lang="<?php echo Yii::app()->language; ?>">
<head>
	<meta charset="<?php echo Yii::app()->charset; ?>"/>
	<style type="text/css">
		body {
			font-family: "Trebuchet MS",Arial,Tahoma,Verdana,sans-serif;
			font-size: 14px;
			font-weight: normal;
		}
		.b {
			font-weight: bold;
			font-size: 15px;
		}
		.text-right {
			text-align: right;
		}
		table tr th {
			font-size: 18px;
			background: #e3e2e2;
		}
	</style>
</head>
<body>
	<h3>Сообщение с сайта:</h3>
	<table border="0" cellspacing="5" cellpadding="5">
        <tr>
            <th colspan="2">Текст запроса</th>
        </tr>
        <tr>
            <td colspan="2"> <?=$data->text;?></td>
        </tr>
		<tr>
			<th colspan="2">Клиент</th>
		</tr>
		<tr>
			<td class="b text-right">ФИО</td>
			<td><?=$data->client->name;?></td>
		</tr>
        <tr>
			<td class="b text-right">E-mail</td>
			<td><?=$data->client->email;?></td>
		</tr>
        <?php if( $data->client->phone ) : ?>
            <tr>
                <td class="b text-right">Телефон</td>
                <td><?=$data->client->phone;?></td>
            </tr>
        <?php endif; ?>
        <?php if( $data->client->mobile ) : ?>
            <tr>
                <td class="b text-right">Мобильный</td>
                <td><?=$data->client->mobile;?></td>
            </tr>
        <?php endif; ?>
        <?php if( $data->client->comment ) : ?>
            <tr>
                <td class="b text-right">Коментарий</td>
                <td><?=$data->client->comment;?></td>
            </tr>
        <?php endif; ?>
    </table>
</body>
</html>