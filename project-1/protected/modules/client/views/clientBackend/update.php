<?php
/**
 * Отображение для ClientBackend/update:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Clients') => ['/client/clientBackend/index'],
    $model->name                       => ['/client/clientBackend/view', 'id' => $model->id],
    Yii::t('ClientModule.client', 'Edit'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Clients - edit');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Clients'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage clients'),
                'url'   => ['/client/clientBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a client'),
                'url'   => ['/client/clientBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('ClientModule.client', 'Requests'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage requests'),
                'url'   => ['/client/requestBackend/index']
            ],
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Client edit'); ?><br/>
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>
