<?php
/**
 * Отображение для ClientBackend/_form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $form \yupe\widgets\ActiveForm
 * @var $model Client
 * @var $this ClientBackendController
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm',
    [
        'id'                     => 'client-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => ['class' => 'well'],
    ]
);

?>
<div class="alert alert-info">
    <?php echo Yii::t('ClientModule.client', 'Fields marked with'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('ClientModule.client', 'are required.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-4">
        <?php
        echo $form->textFieldGroup(
            $model,
            'name',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('name'),
                        'data-content'        => $model->getAttributeDescription('name'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-3">
        <?php
        echo $form->textFieldGroup(
            $model,
            'email',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('email'),
                        'data-content'        => $model->getAttributeDescription('email'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <?php
        echo $form->textFieldGroup(
            $model,
            'phone',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('phone'),
                        'data-content'        => $model->getAttributeDescription('phone'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-1"></div>
    <div class="col-sm-3">
        <?php
        echo $form->textFieldGroup(
            $model,
            'mobile',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('mobile'),
                        'data-content'        => $model->getAttributeDescription('mobile'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'status',
            [
                'widgetOptions' => [
                    'data'        => $model->getStatusList(),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('status'),
                        'data-content'        => $model->getAttributeDescription('status'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 popover-help" data-original-title='<?php echo $model->getAttributeLabel('comment'); ?>'
         data-content='<?php echo $model->getAttributeDescription('comment'); ?>'>
        <?php echo $form->labelEx($model, 'comment'); ?>
        <?php
        $this->widget(
            $this->module->getVisualEditor(),
            [
                'model'     => $model,
                'attribute' => 'comment',
            ]
        ); ?>
    </div>
</div>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => $model->isNewRecord ? Yii::t('ClientModule.client', 'Create client and continue') : Yii::t(
            'ClientModule.client',
            'Save client and continue'
        ),
    ]
); ?>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType'  => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label'       => $model->isNewRecord ? Yii::t('ClientModule.client', 'Create client and close') : Yii::t(
            'ClientModule.client',
            'Save client and close'
        ),
    ]
); ?>

<?php $this->endWidget(); ?>
