<?php
/**
 * Отображение для formConstructorBackend/clone:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Form constructor') => ['/client/formConstructorBackend/index'],
    $cloneModel->name                       => ['/client/formConstructorBackend/view', 'id' => $cloneModel->id],
    Yii::t('ClientModule.client', 'Cloning'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Forms - clone');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Form constructor'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage forms'),
                'url'   => ['/client/formConstructorBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a form'),
                'url'   => ['/client/formConstructorBackend/create']
            ],
            'label' => Yii::t('ClientModule.client', 'Form constructor field'),
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage form constructor fields'),
                'url'   => ['/client/formConstructorToFieldBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add form constructor field'),
                'url'   => ['/client/formConstructorToFieldBackend/create']
            ],
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Form clone'); ?><br/>
        <small>&laquo;<?php echo $cloneModel->name; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>
