<?php
/**
 * Отображение для formConstructorBackend/index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Form constructor') => ['/client/formConstructorBackend/index'],
    Yii::t('ClientModule.client', 'Administration'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Forms - administration');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Form constructor'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage forms'),
                'url'   => ['/client/formConstructorBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a form'),
                'url'   => ['/client/formConstructorBackend/create']
            ],
            'label' => Yii::t('ClientModule.client', 'Form constructor field'),
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage form constructor fields'),
                'url'   => ['/client/formConstructorToFieldBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add form constructor field'),
                'url'   => ['/client/formConstructorToFieldBackend/create']
            ],
        ]
    ],
];
?>

<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Form constructor'); ?>
        <small><?php echo Yii::t('ClientModule.client', 'Administration'); ?></small>
    </h1>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'forms-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'code',
                'editable' => [
                    'url'    => $this->createUrl('/client/formConstructorBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'code', ['class' => 'form-control']),
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'name',
                'editable' => [
                    'url'    => $this->createUrl('/client/formConstructorBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'name'   => 'responsible_user_id',
                'value'  => 'empty($data->responsibleUser) ? "---" : $data->responsibleUser->fullName',
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'responsible_user_id',
                    User::model()->getActiveUsersList(),
                    ['encode' => false, 'empty' => '', 'class' => 'form-control']
                )
            ],
            [
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'save_request',
                'url'     => $this->createUrl('/client/formConstructorBackend/inline'),
                'source'  => $model->getYesNoList(),
                'options' => [
                    FormConstructor::YES  => ['class' => 'label-success'],
                    FormConstructor::NO => ['class' => 'label-default'],
                ],
            ],
            [
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'send_mail_to_user',
                'url'     => $this->createUrl('/client/formConstructorBackend/inline'),
                'source'  => $model->getYesNoList(),
                'options' => [
                    FormConstructor::YES  => ['class' => 'label-success'],
                    FormConstructor::NO => ['class' => 'label-default'],
                ],
            ],
            [
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'send_mail_to_client',
                'url'     => $this->createUrl('/client/formConstructorBackend/inline'),
                'source'  => $model->getYesNoList(),
                'options' => [
                    FormConstructor::YES  => ['class' => 'label-success'],
                    FormConstructor::NO => ['class' => 'label-default'],
                ],
            ],
            [
                'header' => Yii::t('ClientModule.client', 'Fields'),
                'value'  => 'CHtml::link($data->fieldsCount, array("/client/formConstructorToFieldBackend/index","FormConstructorToField[form_constructor_id]" => $data->id ))',
                'type'   => 'html'
            ],
            [
                'header' => Yii::t('ClientModule.client', 'Requests'),
                'value'  => 'CHtml::link($data->requestsCount, array("/client/requestBackend/index","Request[form_constructor_id]" => $data->id ))',
                'type'   => 'html'
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'template' => '{view} {update} {clone}',
                'buttons'     => [
                    'clone'       => [
                        'icon'  => 'fa fa-fw fa-files-o',
                        'label' => Yii::t('ClientModule.client', 'Clone'),
                        'url'   => 'array("/client/formConstructorBackend/clone", "id" => $data->id)',
                    ],
                ]
            ],

        ],
    ]
); ?>
