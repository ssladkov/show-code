<?php
/**
 * Отображение для formConstructorBackend/create:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Form constructor') => ['/client/formConstructorBackend/index'],
    Yii::t('ClientModule.client', 'Create'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Forms - create');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Form constructor'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage forms'),
                'url'   => ['/client/formConstructorBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a form'),
                'url'   => ['/client/formConstructorBackend/create']
            ],
            'label' => Yii::t('ClientModule.client', 'Form constructor field'),
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage form constructor fields'),
                'url'   => ['/client/formConstructorToFieldBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add form constructor field'),
                'url'   => ['/client/formConstructorToFieldBackend/create']
            ],
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Form constructor'); ?>
        <small><?php echo Yii::t('ClientModule.client', 'Create'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>
