<?php
/**
 * Отображение для FormConstructorBackend/_form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $form \yupe\widgets\ActiveForm
 * @var $model FormConstructor
 * @var $this FormConstructorBackendController
 ***/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm',
    [
        'id'                     => 'formConstructor-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => ['class' => 'well'],
    ]
);

?>
<div class="alert alert-info">
    <?php echo Yii::t('ClientModule.client', 'Fields marked with'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('ClientModule.client', 'are required.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'code'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'title'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'submit_text'); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-8">
        <?php echo $form->textFieldGroup($model, 'links_bottom'); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'success_message'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'error_message'); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'responsible_user_id',
            [
                'widgetOptions' => [
                    'data'        =>  User::model()->getActiveUsersList(),
                    'htmlOptions' => [
                        'empty' => Yii::t('ClientModule.client', '--choose--'),
                    ],
                ]
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'send_mail_to_client',
            [
                'widgetOptions' => [
                    'data'        => $model->getYesNoList(),
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'save_request',
            [
                'widgetOptions' => [
                    'data'        => $model->getYesNoList(),
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'send_mail_to_user',
            [
                'widgetOptions' => [
                    'data'        => $model->getYesNoList(),
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php echo $form->labelEx($model, 'caption_top'); ?>
        <?php
        $this->widget(
            $this->module->getVisualEditor(),
            [
                'model'     => $model,
                'attribute' => 'caption_top',
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php echo $form->labelEx($model, 'caption_bottom'); ?>
        <?php
        $this->widget(
            $this->module->getVisualEditor(),
            [
                'model'     => $model,
                'attribute' => 'caption_bottom',
            ]
        ); ?>
    </div>
</div>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => $model->isNewRecord ? Yii::t('ClientModule.client', 'Create form and continue') : Yii::t(
            'ClientModule.client',
            'Save form and continue'
        ),
    ]
); ?>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType'  => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label'       => $model->isNewRecord ? Yii::t('ClientModule.client', 'Create form and close') : Yii::t(
            'ClientModule.client',
            'Save form and close'
        ),
    ]
); ?>

<?php $this->endWidget(); ?>
