<?php
/**
 * Отображение для clientBackend/_search:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'action'      => Yii::app()->createUrl($this->route),
        'method'      => 'get',
        'type'        => 'vertical',
        'htmlOptions' => ['class' => 'well'],
    ]
);

?>

<fieldset>
    <div class="row">
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'client_id',
                [
                    'widgetOptions' => [
                        'data'        => Client::model()->getNotBlockedClientsList(),
                        'htmlOptions' => [
                            'empty'               => '---',
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('client_id'),
                            'data-content'        => $model->getAttributeDescription('client_id'),
                        ],
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'form_constructor_id',
                [
                    'widgetOptions' => [
                        'data'        => FormConstructor::model()->getFormsList(),
                        'htmlOptions' => [
                            'empty'               => '---',
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('form_constructor_id'),
                            'data-content'        => $model->getAttributeDescription('form_constructor_id'),
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'offer_id',
                [
                    'widgetOptions' => [
                        'data'        => FormConstructor::model()->getFormsList(),
                        'htmlOptions' => [
                            'empty'               => '---',
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('offer_id'),
                            'data-content'        => $model->getAttributeDescription('offer_id'),
                        ],
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'responsible_user_id',
                [
                    'widgetOptions' => [
                        'data'        => User::model()->getActiveUsersList(),
                        'htmlOptions' => [
                            'empty'               => '---',
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('responsible_user_id'),
                            'data-content'        => $model->getAttributeDescription('responsible_user_id'),
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'status',
                [
                    'widgetOptions' => [
                        'data'        => $model->getStatusList(),
                        'htmlOptions' => [
                            'empty'               => '---',
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('status'),
                            'data-content'        => $model->getAttributeDescription('status'),
                        ],
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-4">
            <?php echo $form->textFieldGroup(
                $model,
                'text',
                [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('text'),
                            'data-content'        => $model->getAttributeDescription('text'),
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>
</fieldset>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'context'     => 'primary',
        'encodeLabel' => false,
        'buttonType'  => 'submit',
        'label'       => '<i class="fa fa-search">&nbsp;</i> ' . Yii::t('ClientModule.client', 'Find a request'),
    ]
); ?>

<?php $this->endWidget(); ?>
