<?php
/**
 * Отображение для RequestBackend/create:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Requests') => ['/client/requestBackend/index'],
    Yii::t('ClientModule.client', 'Create'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Requests - create');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Clients'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage clients'),
                'url'   => ['/client/clientBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a client'),
                'url'   => ['/client/clientBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('ClientModule.client', 'Requests'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage requests'),
                'url'   => ['/client/requestBackend/index']
            ],
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Requests'); ?>
        <small><?php echo Yii::t('ClientModule.client', 'Create'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>
