<?php
/**
 * Отображение для requestBackend/_form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $form \yupe\widgets\ActiveForm
 * @var $model Request
 * @var $this requestBackend
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm',
    [
        'id'                     => 'request-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => ['class' => 'well'],
    ]
);

?>
<div class="alert alert-info">
    <?php echo Yii::t('ClientModule.client', 'Fields marked with'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('ClientModule.client', 'are required.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'client_id',
            [
                'widgetOptions' => [
                    'data'        => Client::model()->getNotBlockedClientsList(),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('client_id'),
                        'data-content'        => $model->getAttributeDescription('client_id'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'form_constructor_id',
            [
                'widgetOptions' => [
                    'data'        => FormConstructor::model()->getFormsList(),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('form_constructor_id'),
                        'data-content'        => $model->getAttributeDescription('form_constructor_id'),
                        'empty' => Yii::t('ClientModule.client', '--choose--'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'offer_id',
            [
                'widgetOptions' => [
                    'data'        => Offer::model()->getOffersList(),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('offer_id'),
                        'data-content'        => $model->getAttributeDescription('offer_id'),
                        'empty' => Yii::t('ClientModule.client', '--choose--'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'responsible_user_id',
            [
                'widgetOptions' => [
                    'data'        => User::model()->getActiveUsersList(),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('responsible_user_id'),
                        'data-content'        => $model->getAttributeDescription('responsible_user_id'),
                        'empty' => Yii::t('ClientModule.client', '--choose--'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 popover-help" data-original-title='<?php echo $model->getAttributeLabel('text'); ?>'
         data-content='<?php echo $model->getAttributeDescription('text'); ?>'>
        <?php echo $form->labelEx($model, 'text'); ?>
        <?php
        $this->widget(
            $this->module->getVisualEditor(),
            [
                'model'     => $model,
                'attribute' => 'text',
            ]
        ); ?>
    </div>
</div>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => $model->isNewRecord ? Yii::t('ClientModule.client', 'Create request and continue') : Yii::t(
            'ClientModule.client',
            'Save request and continue'
        ),
    ]
); ?>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType'  => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label'       => $model->isNewRecord ? Yii::t('ClientModule.client', 'Create request and close') : Yii::t(
            'ClientModule.client',
            'Save request and close'
        ),
    ]
); ?>

<?php $this->endWidget(); ?>
