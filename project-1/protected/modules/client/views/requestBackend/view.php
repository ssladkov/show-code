<?php
/**
 * Отображение для requestBackend/view:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Requests') => ['/client/requestBackend/index'],
    $model->id,
];

$this->pageTitle = Yii::t('ClientModule.client', 'Requests - view');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Clients'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage clients'),
                'url'   => ['/client/clientBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a client'),
                'url'   => ['/client/clientBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('ClientModule.client', 'Requests'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage requests'),
                'url'   => ['/client/requestBackend/index']
            ],
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Viewing requests'); ?><br/>
        <small>#<?php echo $model->id; ?></small>
    </h1>
</div>

<?php $this->widget(
    'bootstrap.widgets.TbDetailView',
    [
        'data'       => $model,
        'attributes' => [
            [
                'name' => 'client_id',
                'value' => $model->client->name
            ],
            [
                'name' => 'form_constructor_id',
                'value' => ($model->form_constructor_id?$model->formConstructor->name:"---")
            ],
            [
                'name' => 'offer_id',
                'value' => ($model->offer_id?$model->offer->name:"---")
            ],
            [
                'name' => 'responsible_user_id',
                'value' => ($model->responsible_user_id?$model->responsibleUser->getFullName():"---")
            ],
            [
                'name'  => 'status',
                'value' => $model->getStatus(),
            ],
            [
                'name'  => 'create_time',
                'value' => Yii::app()->getDateFormatter()->formatDateTime($model->create_time, "short", "short"),
            ],
        ],
    ]
); ?>
