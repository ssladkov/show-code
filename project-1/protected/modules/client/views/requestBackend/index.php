<?php
/**
 * Отображение для requestBackend/index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Requests') => ['/client/requestBackend/index'],
    Yii::t('ClientModule.client', 'Administration'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Requests - administration');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Clients'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage clients'),
                'url'   => ['/client/clientBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a client'),
                'url'   => ['/client/clientBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('ClientModule.client', 'Requests'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage requests'),
                'url'   => ['/client/requestBackend/index']
            ],
        ]
    ],
];
?>

<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Requests'); ?>
        <small><?php echo Yii::t('ClientModule.client', 'Administration'); ?></small>
    </h1>
</div>

<a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
    <i class="fa fa-search">&nbsp;</i>
    <?php echo Yii::t('ClientModule.client', 'Find a request'); ?>
    <span class="caret">&nbsp;</span>
</a>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('request-grid', {
            data: $(this).serialize()
        });

        return false;
    });"
    );
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'request-grid',
        'dataProvider' => $model->statusdatesort()->search(),
        'filter'       => $model,
        'columns'      => [
            'id',
            [
                'name' => 'client_id',
                'value' => '$data->client->name',
                'filter' => Client::getNotBlockedClientsList()
            ],
            [
                'name' => 'form_constructor_id',
                'value' => '($data->form_constructor_id?$data->formConstructor->name:"---")',
                'filter' => FormConstructor::getFormsList()
            ],
            [
                'name' => 'offer_id',
                'value' => '($data->offer_id?$data->offer->name:"---")',
                'filter' => Offer::getOffersList()
            ],
            [
                'name' => 'responsible_user_id',
                'value' => '($data->responsible_user_id?$data->responsibleUser->getFullName():"---")',
                'filter' => User::getFullNameList()
            ],

            [
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/client/requestBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Request::STATUS_NEW  => ['class' => 'label-danger'],
                    Request::STATUS_IN_WORK  => ['class' => 'label-success'],
                    Request::STATUS_CLOSED => ['class' => 'label-default'],
                ],
            ],
            [
                'name'   => 'create_time',
                'value'  => 'Yii::app()->getDateFormatter()->formatDateTime($data->create_time, "short", "short")',
                'filter' => false
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
