<?php
/**
 * Отображение для clientBackend/index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Form fields') => ['/client/formFieldBackend/index'],
    Yii::t('ClientModule.client', 'Administration'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Form fields - administration');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Form fields'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage form fields'),
                'url'   => ['/client/formFieldBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a form field'),
                'url'   => ['/client/formFieldBackend/create']
            ],
        ]
    ],
];
?>

<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Form fields'); ?>
        <small><?php echo Yii::t('ClientModule.client', 'Administration'); ?></small>
    </h1>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'form-fields-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'code',
                'editable' => [
                    'url'    => $this->createUrl('/client/formFieldBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'code', ['class' => 'form-control']),
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'name',
                'editable' => [
                    'url'    => $this->createUrl('/client/formFieldBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'mask',
                'editable' => [
                    'url'    => $this->createUrl('/client/formFieldBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'mask', ['class' => 'form-control']),
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'template' => '{update}'
            ],

        ],
    ]
); ?>
