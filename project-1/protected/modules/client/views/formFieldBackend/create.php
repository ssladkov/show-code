<?php
/**
 * Отображение для FormFieldBackend/create:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Form fields') => ['/client/formFieldBackend/index'],
    Yii::t('ClientModule.client', 'Create'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Form fields - create');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Form fields'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage form fields'),
                'url'   => ['/client/formFieldBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a form field'),
                'url'   => ['/client/formFieldBackend/create']
            ],
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Form fields'); ?>
        <small><?php echo Yii::t('ClientModule.client', 'Create'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>
