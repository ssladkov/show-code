<?php
/**
 * Отображение для FormFieldBackend/update:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'Clients') => ['/client/FormFieldBackend/index'],
    $model->name                       => ['/client/FormFieldBackend/view', 'id' => $model->id],
    Yii::t('ClientModule.client', 'Edit'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'Form fields - edit');

$this->menu = [
    [
        'label' => Yii::t('ClientModule.client', 'Form fields'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ClientModule.client', 'Manage form fields'),
                'url'   => ['/client/formFieldBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ClientModule.client', 'Add a form field'),
                'url'   => ['/client/formFieldBackend/create']
            ],
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'Form field edit'); ?><br/>
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>
