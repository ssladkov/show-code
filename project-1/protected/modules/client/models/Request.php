<?php

/**
 * This is the model class for table "{{client_request}}".
 *
 * The followings are the available columns in table '{{client_request}}':
 * @property string $id
 * @property string $client_id
 * @property string $text
 * @property string $create_time
 * @property string $form_constructor_id
 * @property string $offer_id
 * @property string $status
 * @property integer $responsible_user_id
 *
 * The followings are the available model relations:
 * @property ClientClient $client
 * @property ClientFormConstructor $formConstructor
 * @property PurposePurpose $purpose
 * @property ClientRequestFile[] $clientRequestFiles
 */

class Request extends yupe\models\YModel
{
	const STATUS_NEW = 1;
    const STATUS_IN_WORK = 2;
    const STATUS_CLOSED = 3;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{client_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, text', 'required'),
            array('client_id, form_constructor_id, offer_id, responsible_user_id, status', 'numerical', 'integerOnly'=>true),
			array('client_id, form_constructor_id, offer_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, client_id, text, create_time, form_constructor_id, offer_id, status, responsible_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
			'formConstructor' => array(self::BELONGS_TO, 'FormConstructor', 'form_constructor_id'),
			'offer' => array(self::BELONGS_TO, 'Offer', 'offer_id'),
			'responsibleUser' => array(self::BELONGS_TO, 'User', 'responsible_user_id'),
            'clientRequestFiles' => array(self::HAS_MANY, 'RequestFile', 'request_id'),
		);
	}

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'new' => [
                'condition' => 't.status = :status',
                'params'    => [':status' => self::STATUS_NEW],
            ],
            'inwork'    => [
                'condition' => 't.status = :status',
                'params'    => [':status' => self::STATUS_IN_WORK],
            ],
            'closed' => [
                'condition' => 't.status = :status',
                'params'    => [':status' => self::STATUS_CLOSED]
            ],
            'datesort'    => [
                'order' => 'create_time DESC'
            ],
            'statusdatesort'    => [
                'order' => 'status, create_time DESC'
            ]
        ];
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => Yii::t('ClientModule.client', 'Client'),
			'text' => Yii::t('ClientModule.client', 'Request text'),
			'create_time' => Yii::t('ClientModule.client', 'Create date'),
			'form_constructor_id' => Yii::t('ClientModule.client', 'Request form'),
			'offer_id' => Yii::t('ClientModule.client', 'Offer'),
			'responsible_user_id' => Yii::t('ClientModule.client', 'Responsible manager'),
			'status' => Yii::t('ClientModule.client', 'Status'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('create_time',$this->create_time);
		$criteria->compare('form_constructor_id',$this->form_constructor_id);
		$criteria->compare('offer_id',$this->offer_id);
		$criteria->compare('responsible_user_id',$this->responsible_user_id);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    /**
     * @return array customized attribute descriptions (name=>description)
     */
    public function attributeDescriptions()
    {
        return [
            'id'          => Yii::t('ClientModule.client', 'Client id'),
            'client_id'        => Yii::t(
                'ClientModule.client',
                'Please choose client, from who you want to place the request'
            ),
            'form_constructor_id' => Yii::t(
                'ClientModule.client',
                'Please choose which form must be used for this request, it depends on who will be notified after request adding'
            ),
            'offer_id'        => Yii::t(
                'ClientModule.client',
                'Please choose offer to which this request must be added. You should choose this field OR form constructor'
            ),
            'responsible_user_id'        => Yii::t(
                'ClientModule.client',
                'If you specify responsible manager, he will be notified by e-mail and this request receive status "In work", otherwise, no one will be notified and this request receive status "New"'
            ),
            'text'        => Yii::t(
                'ClientModule.client',
                'Please enter some text of the request'
            ),
        ];
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_NEW => Yii::t('ClientModule.client', 'New'),
            self::STATUS_IN_WORK  => Yii::t('ClientModule.client', 'In work'),
            self::STATUS_CLOSED => Yii::t('ClientModule.client', 'Closed'),
        ];
    }
    /**
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('ClientModule.client', '*unknown*');
    }

    /*private function _mailToResponsibleUser() {
        if( $this->responsible_user_id && $this->responsibleUser->email ) {
            $message = new YiiMailMessage;
            $message->setTo(array($this->responsibleUser->email));
            $message->setFrom(array('no-reply@rplus.ru' => 'RPLUS.RU'));
            $message->setSubject(Yii::t('ClientModule.client', '[RPLUS.RU] Request notification: You\'ve been assigned to the new request #{id}', ['id' => $this->id]));
            $message->view = 'requestbackendstatuschange';
            $message->setContentType("text/html");
            $message->setBody(array('data'=>$this, 'text/html'));
            Yii::app()->swiftMail->send($message);
        }
    }*/
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Request the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @inheritdoc
     */
    public function afterSave()
    {
        $isSend = false;
        $from = $this->client->email;
        $from_name = $this->client->name;
        $subject = Yii::app()->params["siteNameForMails"] . Yii::t('ClientModule.client', 'Message from the site #{id}', array('{id}'=>$this->id));
        $template = "newclientrequest";
        $data = $this;
        if( ( $this->scenario == "backend-new-request" && $this->status == Request::STATUS_IN_WORK )
            || $this->scenario == "backend-update-request-responsible"
        ) {
            $isSend = true;
            $toArr[] = $this->responsibleUser->email;
        }
        elseif( $this->scenario == "request-create-with-mail" ) {
            $toArr = [];
            if( $this->responsible_user_id ) {
                $user = User::model()->findByPk($this->responsible_user_id);
                if( $user !== null || $user->email ) {
                    $isSend = true;
                    $toArr[] = $user->email;
                }
            }
            elseif( $this->offer_id && $this->offer->responsible_user_id && ($user_email = $this->offer->responsibleUser->email) ) {
                $isSend = true;
                $toArr[] = $user_email;
                $isSend = true;
                foreach (User::model()->secretary()->findAll() as $user) {
                    if ($user->email)
                        $toArr[] = $user->email;
                }
            }
            else {
                $isSend = true;
                foreach (User::model()->secretary()->findAll() as $user) {
                    if ($user->email)
                        $toArr[] = $user->email;
                }
            }

        }
        if( $isSend ) {
            YiiMailMessage::mailNotification($from,$from_name,$toArr,$subject,$template,$data);
        }

        parent::afterSave();
    }

    public function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->create_time = new CDbExpression('NOW()');
        }

        return parent::beforeSave();
    }
}
