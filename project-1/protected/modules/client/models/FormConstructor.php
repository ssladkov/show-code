<?php

/**
 * This is the model class for table "{{client_form_constructor}}".
 *
 * The followings are the available columns in table '{{client_form_constructor}}':
 * @property string $id
 * @property string $code
 * @property string $name
 * @property string $title
 * @property string $caption_top
 * @property string $caption_bottom
 * @property string $links_bottom
 * @property string $submit_text
 * @property string $success_message
 * @property string $error_message
 * @property integer $save_request
 * @property integer $send_mail_to_user
 * @property integer $send_mail_to_client
 * @property integer $responsible_user_id
 *
 * The followings are the available model relations:
 * @property UserUser $responsibleUser
 * @property ClientFormField[] $yupeClientFormFields
 * @property ClientRequest[] $clientRequests
 */
class FormConstructor extends yupe\models\YModel
{
	const YES = 1;
    const NO = 0;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{client_form_constructor}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, name', 'required'),
			array('code', 'unique'),
			array('save_request, send_mail_to_user, send_mail_to_client, responsible_user_id', 'numerical', 'integerOnly'=>true),
			array('code, name, submit_text, title', 'length', 'max'=>45),
			array('caption_top, caption_bottom, links_bottom, success_message, error_message', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, name, caption_top, caption_bottom, links_bottom, submit_text, success_message, error_message, save_request, send_mail_to_user, send_mail_to_client, responsible_user_id, title', 'safe', 'on'=>'search'),
			array('id, code, name, title', 'unsafe', 'on'=>'clone'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'responsibleUser' => array(self::BELONGS_TO, 'User', 'responsible_user_id'),
			'formFields' => array(self::MANY_MANY, 'FormField', '{{client_form_constructor_to_field}}(form_constructor_id, form_field_id)'),
			'linkedFields' => array(self::HAS_MANY, 'FormConstructorToField', 'form_constructor_id', 'order' => 'linkedFields.order ASC'),
			'requests' => array(self::HAS_MANY, 'Request', 'form_constructor_id'),
            'fieldsCount' => [
                self::STAT,
                'FormConstructorToField',
                'form_constructor_id',
            ],
            'requestsCount' => [
                self::STAT,
                'Request',
                'form_constructor_id',
            ],
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => Yii::t('ClientModule.client', 'Code'),
			'name' => Yii::t('ClientModule.client', 'Title'),
			'title' => Yii::t('ClientModule.client', 'Form title'),
			'caption_top' => Yii::t('ClientModule.client', 'Text on top'),
			'caption_bottom' => Yii::t('ClientModule.client', 'Text on bottom'),
			'links_bottom' => Yii::t('ClientModule.client', 'Links on bottom'),
			'submit_text' => Yii::t('ClientModule.client', 'Submit text'),
			'success_message' => Yii::t('ClientModule.client', 'Success message text'),
			'error_message' => Yii::t('ClientModule.client', 'Error message text'),
			'save_request' => Yii::t('ClientModule.client', 'Save form data to DB'),
			'send_mail_to_user' => Yii::t('ClientModule.client', 'Send mail to user'),
			'send_mail_to_client' => Yii::t('ClientModule.client', 'Send mail to client'),
			'responsible_user_id' => Yii::t('ClientModule.client', 'Responsible user'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('save_request',$this->save_request);
		$criteria->compare('send_mail_to_user',$this->send_mail_to_user);
		$criteria->compare('send_mail_to_client',$this->send_mail_to_client);
		$criteria->compare('responsible_user_id',$this->responsible_user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getUsersList()
    {
        $users = new User('search');
        $users->unsetAttributes();
        $users->status = User::STATUS_ACTIVE;

        return $users;
    }

    /**
     * @return array
     */
    public function getYesNoList()
    {
        return [
            self::YES  => Yii::t('ClientModule.client', 'Yes'),
            self::NO => Yii::t('ClientModule.client', 'No'),
        ];
    }

    public static function getFormsList()
    {
        $forms = [];
        foreach( FormConstructor::model()->findAll() as $form ) {
            $forms[$form->id] = $form->name;
        }
        return $forms;
    }

    public static function isFormPresentByCode($code=null) {
        $res = false;
        if( $code !== null ) {
            if (null !== FormConstructor::model()->cache(Yii::app()->getModule('yupe')->coreCacheTime)->find('code = :code', [':code' => $code]))
                $res = true;
        }
        return $res;
    }
    public function cloneFormFields($sourceId) {
        $cloneModel = FormConstructor::model()->findByPk($sourceId);
        if( $cloneModel->fieldsCount > 0 ) {
            foreach( $cloneModel->linkedFields as $linkModel ) {
                $formToFieldModel = new FormConstructorToField('clone');
                $formToFieldModel->setAttributes($linkModel->getAttributes());
                $formToFieldModel->form_constructor_id = $this->id;
                $formToFieldModel->save();
            }
        }
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormConstructor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
