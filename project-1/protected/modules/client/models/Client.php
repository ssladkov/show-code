<?php

/**
 * This is the model class for table "{{client_client}}".
 *
 * The followings are the available columns in table '{{client_client}}':
 * @property string $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $mobile
 * @property string $comment
 * @property string $create_time
 * @property string $update_time
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property ClientRequest[] $clientRequests
 * @property PurposePurpose[] $purposePurposes
 */
class Client extends yupe\models\YModel
{
    const STATUS_BLOCKED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{client_client}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, email', 'required'),
			array('email', 'unique'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('name, email, phone, mobile', 'length', 'max'=>45),
			array('comment', 'length', 'max'=>255),
			array('update_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, email, phone, mobile, comment, create_time, update_time, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'requests' => array(self::HAS_MANY, 'Request', 'client_id'),
			'offers' => array(self::HAS_MANY, 'Offer', 'client_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('ClientModule.client', 'Name'),
			'email' => 'E-mail',
			'phone' => Yii::t('ClientModule.client', 'Phone'),
			'mobile' => Yii::t('ClientModule.client', 'Mobile'),
			'comment' => Yii::t('ClientModule.client', 'Comment'),
			'create_time' => Yii::t('ClientModule.client', 'Create date'),
			'update_time' => Yii::t('ClientModule.client', 'Update date'),
			'status' => Yii::t('ClientModule.client', 'Status'),
		);
	}

    /**
     * @return array customized attribute descriptions (name=>description)
     */
    public function attributeDescriptions()
    {
        return [
            'id'          => Yii::t('ClientModule.client', 'Client id'),
            'name'        => Yii::t(
                'ClientModule.client',
                'Please enter a full name of the client. For example: <span class="label label-default">James F. Johnes</span>.'
            ),
            'email' => Yii::t(
                'ClientModule.client',
                'Please enter an email of the client. For example: <span class="label label-default">james@gmail.com</span>'
            ),
            'phone'        => Yii::t(
                'ClientModule.client',
                'Please enter a phone of the client in the international format.<br /><br /> For example: <span class="label label-default">+7(495)322-32-32</span>'
            ),
            'mobile'        => Yii::t(
                'ClientModule.client',
                'Please enter a mobile of the client in the international format.<br /><br /> For example: <span class="label label-default">+7(903)322-32-32</span>'
            ),
            'comment'        => Yii::t(
                'ClientModule.client',
                'Please enter a comment of the client if you wish to, it may be some usefull information. <br /><br /> For example: <span class="label label-success">we may call him from 10:00 to 15:00</span>'
            ),
            'status'      => Yii::t(
                'ClientModule.client',
                'Please choose a status of the client:<br /><br /><span class="label label-success">active</span> &ndash; The client will be visible and all his requests will be recorded to DB<br /><br /><span class="label label-warning">blocked</span> &ndash; The client will be visible but all of his requests won\'t be recorded to DB, he will recieve a message.<br /><br /><span class="label label-danger">removed</span> &ndash; The client will be invisible. But if there will be request with this client email, request will be recorded to DB and client become active'
            ),
        ];
    }

    public function scopes()
    {
        return [
            'namesort'    => [
                'order' => 'name ASC'
            ]
        ];
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('create_time',$this->create_time);
		$criteria->compare('update_time',$this->update_time);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
        }

        // Пользователя можно получить только для веб-приложения

        return parent::beforeSave();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_BLOCKED => Yii::t('ClientModule.client', 'Blocked'),
            self::STATUS_ACTIVE  => Yii::t('ClientModule.client', 'Active'),
            self::STATUS_DELETED => Yii::t('ClientModule.client', 'Removed'),
        ];
    }
    /**
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('ClientModule.client', '*unknown*');
    }

    public static function getNotBlockedClientsList()
    {
        $clients = [];
        foreach( Client::model()->namesort()->findAll( 'status != :status', [':status' => Client::STATUS_BLOCKED ] ) as $client ) {
            $clients[$client->id] = $client->name;
        }
        return $clients;
    }
}
