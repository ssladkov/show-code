<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 02.02.2016
 * Time: 17:27
 ****************
 * RequestForm
 * базовый класс для всех форм обратной связи
 * необходимые поля и правила для них подгружаются динамически на основе конструктора форм
 */
Yii::import('application.modules.client.ClientModule');
class RequestForm extends yupe\models\YFormModel {
    private $_fields = array();
    private $_rules = array();
    private $_labels = array();
    private $_dynamicFields = array();
    private $_view = 'create';

    public $formCode;
    public $formTitle;
    public $formName;
    public $formId;
    public $formSubmitBtnText;
    public $formSuccessMsg;
    public $formErrorMsg;
    public $formSaveRequest;
    public $formSendMailToSecretary;
    public $formSendMailToClient;
    public $formResponsibleUserId;
    public $formTextBefore;
    public $formTextAfter;
    public $formAddLink;


    /**
     * Перебиваем геттер.
     * Перебитый сеттер будет класть необъявленные свойства класса в _dynamicFields, а перебитый геттер будет их от туда вытаскивать
     *
     * @param type $name
     * @return type
     */
    public function __get($name) {
        if (isset($this->_dynamicFields[$name])) {
            return $this->_dynamicFields[$name];
        } else {
            return parent::__get($name);
        }
    }

    /**
     * Перебиваем сеттер.
     * Т.к., нам необходимо работать с динамическими полями (свойствами модели), а существующий сеттер выкинет exception, если мы попытаемся через него
     * обратиться к свойству, которое не объявлено, будем складывать эти поля в массив _dynamicFields
     *
     * @param type $name
     * @param type $val
     * @return void
     */
    public function __set($name, $val) {
        if (!empty($this->_fields[$name])) {
            $this->_dynamicFields[$name] = $val;
        } else {
            parent::__set($name, $val);
        }
    }


    public function rules() {
        return $this->_rules;
    }

    public function attributeLabels() {
        return $this->_labels;
    }

    public function getFields() {
        return $this->_fields;
    }

    public function init() {
        $formCode = $this->scenario;
        if($formCode === null)
            throw new CHttpException(500, "Form code doesn't specified!");

        $formConstructorModel = FormConstructor::model()->find("code = :code", [":code" => $formCode]);
        if( $formConstructorModel === null )
            throw new CHttpException(500, "Form with code: $formCode doesn't exist");

        /** Установим данные формы */
        $this->formCode = $formCode;
        $this->formTitle = $formConstructorModel->title ? $formConstructorModel->title : $formConstructorModel->name;
        $this->formName = $formConstructorModel->name;
        $this->formId = $formConstructorModel->id;
        $this->formSubmitBtnText = $formConstructorModel->submit_text ? $formConstructorModel->submit_text : Yii::t('ClientModule.client', 'Send');
        $this->formSuccessMsg = $formConstructorModel->success_message ? $formConstructorModel->success_message : Yii::t('ClientModule.client', 'Operation was successful!');
        $this->formErrorMsg = $formConstructorModel->error_message ? $formConstructorModel->error_message : Yii::t('ClientModule.client', 'There was a error during operation process. Please, try again later');
        $this->formSaveRequest = $formConstructorModel->save_request;
        $this->formSendMailToSecretary = $formConstructorModel->send_mail_to_user;
        $this->formSendMailToClient = $formConstructorModel->send_mail_to_client;
        $this->formTextBefore = $formConstructorModel->caption_top;
        $this->formTextAfter = $formConstructorModel->caption_bottom;
        $this->formAddLink = $formConstructorModel->links_bottom;
        $this->formResponsibleUserId = $formConstructorModel->responsible_user_id;

        $_required_fields = array();
        $_max_length_fields = array();
        $_email_fields = array();
        $_pattern_fields = array();
        $_safe_fields = array();
        foreach( $formConstructorModel->linkedFields as $linkedFieldModel ) {
            $fieldName = $linkedFieldModel->name;
            /** Заполним поля */
            $this->_fields[$fieldName] = array(
                "type"          => $linkedFieldModel->formField->code,
                "mask"          => $linkedFieldModel->formField->mask,
                "placeholder"   => $linkedFieldModel->placeholder
            );
            /** Создаем свойства */
            $this->{$fieldName} = $linkedFieldModel->default ? $linkedFieldModel->default : '';
            /** Заполним названия полей */
            $this->_labels[$fieldName] = $linkedFieldModel->label;
            /** Добавим данные для правил */
            $_safe_fields[] = $fieldName;
            if( $linkedFieldModel->not_empty ) {
                $_required_fields[] = $fieldName;
            }
            if( $maxLength = $linkedFieldModel->maxlength ) {
                $_max_length_fields[$maxLength][] = $fieldName;
            }
            if( $linkedFieldModel->formField->code == "input-email") {
                $_email_fields[] = $fieldName;
            }
            if( $pattern = $linkedFieldModel->formField->pattern ) {
                $_pattern_fields[$fieldName] = array(
                    "pattern" => $pattern,
                    "message" => "Некорректный формат поля {attribute}"
                );
            }
        }
        /** Заполним правила */
        $this->_rules[] = array( join(',',$_required_fields), 'required');
        $this->_rules[] = array( join(',',$_email_fields), 'email');
        foreach( $_pattern_fields as $fName => $rData ) {
            $this->_rules[] = array( $fName, 'match', 'pattern' => $rData['pattern'], 'message' => $rData['message'] );
        }
        foreach( $_max_length_fields as $length => $fArr ) {
            $this->_rules[] = array( join(',',$fArr), 'length', 'max' => $length);
        }
        $this->_rules[] = array( join(',',$_safe_fields), 'safe', 'on' => $formCode);
    }

    public function save() {
        $attaches = [];
        /**  Если надо сохранить сообщение в базу и у нас есть email, name и text */
        if( $this->formSaveRequest
            && isset($this->_dynamicFields["email"])
            && $this->_dynamicFields["email"]
            && isset($this->_dynamicFields["name"])
            && $this->_dynamicFields["name"]
            && isset($this->_dynamicFields["text"])
            && $this->_dynamicFields["text"]
        ) {
            /** Проверим, есть ли в базе клиент с указанным e-mail */
            $client = Client::model()->find("email = :email", [":email" => $this->email ]);
            /** Если нет, добавляем */
            if( $client === null ) {
                $client = new Client('search');
                $client->email = $this->email;
                $client->name = $this->name;
                $client->status = Client::STATUS_ACTIVE;
                if( isset($this->_dynamicFields["phone"]) && $this->_dynamicFields["phone"])
                    $client->phone = $this->phone;
                $client->save();
            }
            /** Создаем запрос */
            $scenario = $this->formSendMailToSecretary ? "request-create-with-mail" : "request-create";
            /** Если нужно, почтовое уведомление будет выслано методом afterSave в модели Request */
            $request = new Request($scenario);
            $request->client_id = $client->id;
            $request->form_constructor_id = $this->formId;
            $request->text = $this->text;
            $request->status = $this->formResponsibleUserId ? Request::STATUS_IN_WORK : Request::STATUS_NEW;
            $request->responsible_user_id = $this->formResponsibleUserId;
            if( isset($this->_dynamicFields["offer_id"]))
                $request->offer_id = $this->offer_id;
            $request->save();
        }
        /** Если надо просто отправить сообщение секретарям без сохранения */
        if( !$this->formSaveRequest && $this->formSendMailToSecretary ) {
            $isSend = true;
            $from = isset($this->_dynamicFields["email"]) ? $this->_dynamicFields["email"] : Yii::app()->params["defaultSender"];
            $from_name = isset($this->_dynamicFields["name"]) ? $this->_dynamicFields["name"] : Yii::app()->params["defaultSenderName"];
            $toArr = [];
            foreach( User::model()->secretary()->findAll() as $user) {
                if($user->email)
                    $toArr[] = $user->email;
            }
            if( isset($this->_dynamicFields["resume"]) ) {
                $template = "resumemessage";
                $subject = Yii::t('ClientModule.client', 'Resume from site');

                if( isset($_FILES['resume']) && $_FILES['resume']['name'] ) {
                    $newPath = '/var/www/rplus/data/www/rplus.ru/www/uploads/files/resume' . mt_rand() . '-' .$this->_dynamicFields["email"]. '-' . $_FILES['resume']['name'];
                    //$newPath = 'F:\WebServers\tmp\!sendmail\resume' . mt_rand() . '-' .$this->_dynamicFields["email"]. '-' . $_FILES['resume']['name'];
                    move_uploaded_file($_FILES['resume']['tmp_name'], $newPath);
                    $attaches[] = $newPath;
                }

                $data = [
                    'name' => $this->_dynamicFields["name"],
                    'email' => $this->_dynamicFields["email"],
                    'phone' => $this->_dynamicFields["phone"],
                ];
            }
            elseif( isset($this->_dynamicFields["text"]) ) {
                $template = "formsimplemessage";
                $subject = isset($this->_dynamicFields["subject"]) && $this->_dynamicFields["subject"]
                    ? $this->_dynamicFields["subject"]
                    : (Yii::app()->params["siteNameForMails"] . Yii::t('ClientModule.client', 'Message from the site'))
                ;
                $data = array( "text" => $this->_dynamicFields["text"] );
            }
            elseif( isset($this->_dynamicFields["phone"] )) {
                $template = "backcallmessage";
                $name = $this->_dynamicFields["name"] ? $this->_dynamicFields["name"] : 'Аноним';
                $subject_text = Yii::t("ClientModule.client", "There is a back call from the site to number: {phone}, {name}", ["{phone}" => $this->_dynamicFields["phone"], "{name}" => $name]);
                $subject = Yii::app()->params["siteNameForMails"] . $subject_text;
                $data = array( "text" => $subject_text );
            }
            else {
                $isSend = false;
            }
            if( $isSend ) {
                YiiMailMessage::mailNotification($from,$from_name,$toArr,$subject,$template,$data,$attaches);
            }
        }
        return true;
    }

    public function getView() {
        return $this->_view;
    }

    public function setView($val) {
        $this->_view = $val;
    }
}