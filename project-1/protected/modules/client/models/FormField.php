<?php

/**
 * This is the model class for table "{{client_form_field}}".
 *
 * The followings are the available columns in table '{{client_form_field}}':
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $mask
 *
 * The followings are the available model relations:
 * @property ClientFormConstructor[] $yupeClientFormConstructors
 */
class FormField extends yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{client_form_field}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, name', 'required'),
			array('code, name, mask', 'length', 'max'=>45),
			array('pattern', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, name, mask, pattern', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'yupeClientFormConstructors' => array(self::MANY_MANY, 'ClientFormConstructor', '{{client_form_constructor_to_field}}(form_field_id, form_constructor_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => Yii::t('ClientModule.client', 'Code'),
			'name' => Yii::t('ClientModule.client', 'Title'),
			'mask' => Yii::t('ClientModule.client', 'Mask'),
			'pattern' => Yii::t('ClientModule.client', 'Pattern'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('mask',$this->mask,true);
		$criteria->compare('pattern',$this->pattern,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function getFieldsList()
    {
        $fields = [];
        foreach( FormField::model()->findAll() as $field ) {
            $fields[$field->id] = $field->name;
        }
        return $fields;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormField the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
