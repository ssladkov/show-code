<?php

/**
 * This is the model class for table "{{client_form_constructor_to_field}}".
 *
 * The followings are the available columns in table '{{client_form_constructor_to_field}}':
 * @property integer $id
 * @property integer $form_constructor_id
 * @property integer $form_field_id
 * @property string $name
 * @property string $label
 * @property string $default
 * @property string $placeholder
 * @property integer $not_empty
 * @property integer $maxlength
 * @property integer $order
 */
class FormConstructorToField extends yupe\models\YModel
{
    const YES = 1;
    const NO = 0;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{client_form_constructor_to_field}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('form_constructor_id, form_field_id, name, label', 'required'),
			array('form_field_id, not_empty, maxlength, order', 'numerical', 'integerOnly'=>true),
			array('form_constructor_id, order', 'length', 'max'=>10),
			array('name, label, placeholder', 'length', 'max'=>45),
			array('default', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, form_constructor_id, form_field_id, name, label, default, placeholder, not_empty, maxlength, order', 'safe', 'on'=>'search'),
			array('id, form_constructor_id', 'unsafe', 'on'=>'clone'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'formConstructor' => array(self::BELONGS_TO, 'FormConstructor', 'form_constructor_id'),
            'formField' => array(self::BELONGS_TO, 'FormField', 'form_field_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'form_constructor_id' => Yii::t('ClientModule.client', 'Form constructor'),
			'form_field_id' => Yii::t('ClientModule.client', 'Form field'),
			'name' => Yii::t('ClientModule.client', 'Field name'),
			'label' => Yii::t('ClientModule.client', 'Label'),
			'default' => Yii::t('ClientModule.client', 'Default'),
			'placeholder' => Yii::t('ClientModule.client', 'Placeholder'),
			'not_empty' => Yii::t('ClientModule.client', 'Not empty'),
			'maxlength' => Yii::t('ClientModule.client', 'Maximum length'),
			'order' => Yii::t('ClientModule.client', 'Order'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('form_constructor_id',$this->form_constructor_id,true);
		$criteria->compare('form_field_id',$this->form_field_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('default',$this->default,true);
		$criteria->compare('placeholder',$this->placeholder,true);
		$criteria->compare('not_empty',$this->not_empty);
		$criteria->compare('maxlength',$this->maxlength);
		$criteria->compare('order',$this->order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @return array
     */
    public function getYesNoList()
    {
        return [
            self::YES  => Yii::t('ClientModule.client', 'Yes'),
            self::NO => Yii::t('ClientModule.client', 'No'),
        ];
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormConstructorToField the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
