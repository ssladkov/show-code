<?php

/**
 * FormConstructorToFieldBackendController контроллер для добавления полей в конструктор форм в панели управления
 *
 * @author Sladkov Sergey <me@ssladkov.ru>
 * @link http://ssladkov.ru
 * @copyright 2016 Sergey Sladkov
 * @package yupe.modules.client.controllers
 * @since 0.1
 *
 */
class FormConstructorToFieldBackendController extends yupe\components\controllers\BackController
{


    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Client.FormConstructorToFieldBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Client.FormConstructorToFieldBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Client.FormConstructorToFieldBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Client.FormConstructorToFieldBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Client.FormConstructorToFieldBackend.Delete']],
            ['deny']
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'FormConstructorToField',
                'validAttributes' => ['label', 'placeholder', 'order']
            ]
        ];
    }


    /**
     * Создает новую связь конструктора форм и поля.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return nothing
     **/
    public function actionCreate()
    {
        $model = new FormConstructorToField();

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('FormConstructorToField') !== null) {

            $model->setAttributes(Yii::app()->getRequest()->getPost('FormConstructorToField'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ClientModule.client', 'Form constructor field was added!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование поля конструктора формы.
     *
     * @param  integer $id Идинтификатор связи
     * @throw CHttpException
     * @return nothing
     **/
    public function actionUpdate($id)
    {
        //$pk = compact( array( "form_constructor_id", "form_field_id" ) );
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('FormConstructorToField') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('FormConstructorToField'));
            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ClientModule.client', 'Form constructor field was updated!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }

        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель поля конструктора формы из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id - идентификатор связи
     *
     * @return nothing
     **/
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            //$pk = compact( array( "form_constructor_id", "form_field_id" ) );
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('ClientModule.client', 'Form constructor field was deleted!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t(
                'ClientModule.client',
                'Wrong request. Please don\'t repeate requests like this anymore!'
            ));
        }
    }

    /**
     * Управление формами.
     *
     * @return nothing
     **/
    public function actionIndex()
    {
        $model = new FormConstructorToField('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('FormConstructorToField') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('FormConstructorToField'));
        }
        $this->render('index', ['model' => $model]);
    }

    public function loadModel($id)
    {
        $model = FormConstructorToField::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('ClientModule.client', 'Page was not found!'));
        }

        return $model;
    }
}
