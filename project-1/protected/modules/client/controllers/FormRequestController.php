<?php
/**
 * Контроллер, отвечающий за вывод и обработку всех форм обратной связи
 *
 * @category ClientModule
 * @package  yupe.modules.client.controllers
 * @author   Sergey Sladkov <me@ssladkov.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.1
 * @link     http://ssladkov.ru
 *
 **/
class FormRequestController extends \yupe\components\controllers\FrontController {
    /**
     * Создает новую модель запроса с форм обратной связи.
     * Если создание прошло успешно - перенаправляет на просмотр.
     * @param  $form_code Идинтификатор формы, с которой будем работать
     * @throws CHttpException
     * @return nothing
     **/
    function actionCreateRequest_ajax($form_code=null) {
        if( !Yii::app()->getRequest()->getIsAjaxRequest() && !Yii::app()->getRequest()->getIsPostRequest())
            throw new CHttpException(400, 'Wrong request!');
        if ($form_code === null) {
            throw new CHttpException(500, 'Form code is empty!');
        }
        $model = new RequestForm($form_code);

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('RequestForm') !== null) {

            $model->setAttributes(Yii::app()->getRequest()->getPost('RequestForm'));

            if ($model->save()) {
                if( isset( $_GET['backUrl']) ) {
                    Yii::app()->user->setFlash("widget-response-success",$model->formSuccessMsg);
                    $this->redirect($_GET['backUrl']);
                    Yii::app()->end();
                }
                echo   '<h3 class="text-success"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> ' . $model->formSuccessMsg . '</h3>
                        <button type="button" class="btn btn-default" data-dismiss="modal">' . Yii::t('ClientModule.client', 'Close') . '</button>';
            }
            else {
                echo   '<h3 class="text-danger"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> ' . $model->formErrorMsg . '</h3>
                        <button type="button" class="btn btn-default" data-dismiss="modal">' . Yii::t('ClientModule.client', 'Close') . '</button>';
            }
            Yii::app()->end();
        }
        Yii::app()->clientscript->scriptMap['jquery.js'] = false;
        Yii::app()->clientscript->scriptMap['jquery.min.js'] = false;
        $this->renderPartial($model->view, ['model' => $model, 'form_code' => $form_code, 'modal_id' => $_POST['modal_id']], false, true);
    }
}