<?php

/**
 * FormFieldBackendController контроллер для управления полями конструктора форм в панели управления
 *
 * @author Sladkov Sergey <me@ssladkov.ru>
 * @link http://ssladkov.ru
 * @copyright 2016 Sergey Sladkov
 * @package yupe.modules.client.controllers
 * @since 0.1
 *
 */
class FormFieldBackendController extends yupe\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Client.FormFieldBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Client.FormFieldBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Client.FormFieldBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Client.FormFieldBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Client.FormFieldBackend.Delete']],
            ['deny']
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'FormField',
                'validAttributes' => ['name', 'code', 'mask']
            ]
        ];
    }

    /**
     * Отображает поле для конструктора формы по указанному идентификатору
     * @throws CHttpException
     * @param  integer $id Идинтификатор поля для отображения
     *
     * @return nothing
     **/
    public function actionView($id)
    {
        if (($model = FormField::model()->find($id)) !== null) {
            $this->render('view', ['model' => $model]);
        } else {
            throw new CHttpException(404, Yii::t('ClientModule.client', 'Page was not found!'));
        }
    }

    /**
     * Создает новую модель поля для конструктора форм.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return nothing
     **/
    public function actionCreate()
    {
        $model = new FormField();

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('FormField') !== null) {

            $model->setAttributes(Yii::app()->getRequest()->getPost('FormField'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ClientModule.client', 'Form field was added!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование поля для констурктора форм.
     *
     * @param  integer $id Идинтификатор поля для редактирования
     * @throw CHttpException
     * @return nothing
     **/
    public function actionUpdate($id)
    {
        if (($model = FormField::model()->findByPk($id)) === null) {
            throw new CHttpException(404, Yii::t('ClientModule.client', 'Page was not found!'));
        }

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('FormField') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('FormField'));
            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ClientModule.client', 'Form field was updated!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }

        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель поля для конструктора форм из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id - идентификатор поля, который нужно удалить
     *
     * @return nothing
     **/
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            // поддерживаем удаление только из POST-запроса
            if (($model = FormField::model()->find($id)) === null) {
                throw new CHttpException(404, Yii::t('ClientModule.client', 'Page was not found!'));
            }

            $model->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('ClientModule.client', 'Form field was deleted!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t(
                'ClientModule.client',
                'Wrong request. Please don\'t repeate requests like this anymore!'
            ));
        }
    }

    /**
     * Управление полями для конструктора форм.
     *
     * @return nothing
     **/
    public function actionIndex()
    {
        $model = new FormField('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('FormField') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('FormField'));
        }
        $this->render('index', ['model' => $model]);
    }
}
