<?php

/**
 * FormConstructorBackendController контроллер для конструктора форм в панели управления
 *
 * @author Sladkov Sergey <me@ssladkov.ru>
 * @link http://ssladkov.ru
 * @copyright 2016 Sergey Sladkov
 * @package yupe.modules.client.controllers
 * @since 0.1
 *
 */
class FormConstructorBackendController extends yupe\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Client.FormConstructorBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Client.FormConstructorBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Client.FormConstructorBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Client.FormConstructorBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Client.FormConstructorBackend.Delete']],
            ['deny']
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'FormConstructor',
                'validAttributes' => ['name', 'code', 'send_mail_to_client', 'send_mail_to_user', 'save_request']
            ]
        ];
    }

    /**
     * Отображает форму по указанному идентификатору
     * @throws CHttpException
     * @param  integer $id Идинтификатор формы для отображения
     *
     * @return nothing
     **/
    public function actionView($id)
    {
        if (($model = FormConstructor::model()->find($id)) !== null) {
            $this->render('view', ['model' => $model]);
        } else {
            throw new CHttpException(404, Yii::t('ClientModule.client', 'Page was not found!'));
        }
    }

    /**
     * Создает новую модель формы.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return nothing
     **/
    public function actionCreate()
    {
        $model = new FormConstructor();

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('FormConstructor') !== null) {

            $model->setAttributes(Yii::app()->getRequest()->getPost('FormConstructor'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ClientModule.client', 'Form was added!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование формы.
     *
     * @param  integer $id Идинтификатор формы для редактирования
     * @throws CHttpException
     * @return nothing
     **/
    public function actionUpdate($id)
    {
        if (($model = FormConstructor::model()->findByPk($id)) === null) {
            throw new CHttpException(404, Yii::t('ClientModule.client', 'Page was not found!'));
        }

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('FormConstructor') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('FormConstructor'));
            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ClientModule.client', 'Form was updated!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }

        $this->render('update', ['model' => $model]);
    }

    /**
     * клонирование формы.
     *
     * @param  integer $id Идинтификатор формы, которую будем клонировать
     * @throws CHttpException
     * @return nothing
     **/
    public function actionClone($id)
    {
        if (($cloneModel = FormConstructor::model()->findByPk($id)) === null) {
            throw new CHttpException(404, Yii::t('ClientModule.client', 'Page was not found!'));
        }

        $model = new FormConstructor();

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('FormConstructor') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('FormConstructor'));
            if ($model->save()) {
                $model->cloneFormFields($id);
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ClientModule.client', 'Form was cloned!')
                );
                $this->redirect(
                    '/backend/client/formConstructorToField?FormConstructorToField%5Bform_constructor_id%5D='.$model->id
                );
            }
        }
        else {
            $model->scenario = "clone";
            $model->setAttributes($cloneModel->getAttributes(),true);
        }

        $this->render('clone', ['model' => $model, 'cloneModel' => $cloneModel ]);
    }

    /**
     * Удаляет модель формы из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id - идентификатор формы, который нужно удалить
     * @throws CHttpException
     * @return nothing
     **/
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            // поддерживаем удаление только из POST-запроса
            if (($model = FormConstructor::model()->find($id)) === null) {
                throw new CHttpException(404, Yii::t('ClientModule.client', 'Page was not found!'));
            }

            $model->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('ClientModule.client', 'Form was deleted!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t(
                'ClientModule.client',
                'Wrong request. Please don\'t repeate requests like this anymore!'
            ));
        }
    }

    /**
     * Управление формами.
     *
     * @return nothing
     **/
    public function actionIndex()
    {
        $model = new FormConstructor('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('FormConstructor') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('FormConstructor'));
        }
        $this->render('index', ['model' => $model]);
    }
}
