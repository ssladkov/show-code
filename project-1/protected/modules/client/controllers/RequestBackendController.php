<?php

/**
 * RequestBackendController контроллер для заявок в панели управления
 *
 * @author Sladkov Sergey <me@ssladkov.ru>
 * @link http://ssladkov.ru
 * @copyright 2016 Sergey Sladkov
 * @package yupe.modules.client.controllers
 * @since 0.1
 *
 */
class RequestBackendController extends yupe\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Client.RequestBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Client.RequestBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Client.RequestBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Client.RequestBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Client.RequestBackend.Delete']],
            ['deny']
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'Request',
                'validAttributes' => ['status']
            ]
        ];
    }

    /**
     * Отображает заявку по указанному идентификатору
     * @throws CHttpException
     * @param  integer $id Идинтификатор заявки для отображения
     *
     * @return nothing
     **/
    public function actionView($id)
    {
        if (($model = Request::model()->find($id)) !== null) {
            $this->render('view', ['model' => $model]);
        } else {
            throw new CHttpException(404, Yii::t('ClientModule.client', 'Page was not found!'));
        }
    }

    /**
     * Создает новую модель заявки.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return nothing
     **/
    public function actionCreate()
    {
        $model = new Request('backend-new-request');

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Request') !== null) {

            $model->setAttributes(Yii::app()->getRequest()->getPost('Request'));
            $model->status = $model->responsible_user_id ? Request::STATUS_IN_WORK : Request::STATUS_NEW;
            $model->form_constructor_id = $model->form_constructor_id == '' ? null : $model->form_constructor_id;
            $model->offer_id = $model->offer_id == '' ? null : $model->offer_id;

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ClientModule.client', 'Request was added!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование заявки.
     *
     * @param  integer $id Идинтификатор заявки для редактирования
     * @throws CHttpException
     * @return nothing
     */
    public function actionUpdate($id)
    {
        if (($model = Request::model()->find($id)) === null) {
            throw new CHttpException(404, Yii::t('ClientModule.client', 'Page was not found!'));
        }

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Request') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Request'));
            $model->form_constructor_id = $model->form_constructor_id == '' ? null : $model->form_constructor_id;
            $model->offer_id = $model->offer_id == '' ? null : $model->offer_id;
            if( $model->responsible_user_id ) {
                $oldModel = Request::model()->findByPk($model->id);
                if( $oldModel->responsible_user_id != $model->responsible_user_id )
                    $model->setScenario("backend-update-request-responsible");
                $model->status = Request::STATUS_IN_WORK;
            }
            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ClientModule.client', 'Request was updated!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }

        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель заявки из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id - идентификатор заявки, который нужно удалить
     *
     * @return nothing
     **/
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            // поддерживаем удаление только из POST-запроса
            if (($model = Request::model()->find($id)) === null) {
                throw new CHttpException(404, Yii::t('ClientModule.client', 'Page was not found!'));
            }

            $model->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('ClientModule.client', 'Request was deleted!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t(
                'ClientModule.client',
                'Wrong request. Please don\'t repeate requests like this anymore!'
            ));
        }
    }

    /**
     * Управление заявками.
     *
     * @return nothing
     **/
    public function actionIndex()
    {
        $model = new Request('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('Request') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('Request'));
        }
        $this->render('index', ['model' => $model]);
    }
}
