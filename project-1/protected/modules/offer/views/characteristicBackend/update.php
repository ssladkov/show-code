<?php
/**
 * Отображение для characteristicBackend/update:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('OfferModule.offer', 'Characteristics') => ['/offer/characteristicBackend/index'],
    $model->name,
    Yii::t('OfferModule.offer', 'Edit'),
];

$this->pageTitle = Yii::t('OfferModule.offer', 'Characteristics - edit');

$this->menu = [
    [
        'label' => Yii::t('OfferModule.offer', 'Offers'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OfferModule.offer', 'Manage offers'),
                'url'   => ['/offer/offerBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add an offer'),
                'url'   => ['/offer/offerBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('OfferModule.offer', 'Object characteristics'),
        'items' => [
            [
                'icon' => 'fa fa-fw fa-check-square-o',
                'label' => Yii::t('OfferModule.offer', 'Offer characteristics list'),
                'url' => ['/offer/offerToCharacteristicBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add characteristic link'),
                'url' => ['/offer/offerToCharacteristicBackend/create']
            ],
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('OfferModule.offer', 'Characteristic edit'); ?><br/>
        <small><?php echo $model->name; ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>
