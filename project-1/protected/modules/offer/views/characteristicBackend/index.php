<?php
/**
 * Отображение для characteristicBackend/index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('OfferModule.offer', 'Characteristics') => ['/offer/characteristicBackend/index'],
    Yii::t('OfferModule.offer', 'Administration'),
];

$this->pageTitle = Yii::t('OfferModule.offer', 'Characteristics - administration');

$this->menu = [
    [
        'label' => Yii::t('OfferModule.offer', 'Offers'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OfferModule.offer', 'Manage offers'),
                'url'   => ['/offer/offerBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add an offer'),
                'url'   => ['/offer/offerBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('OfferModule.offer', 'Object characteristics'),
        'items' => [
            [
                'icon' => 'fa fa-fw fa-check-square-o',
                'label' => Yii::t('OfferModule.offer', 'Offer characteristics list'),
                'url' => ['/offer/offerToCharacteristicBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add characteristic link'),
                'url' => ['/offer/offerToCharacteristicBackend/create']
            ],
        ]
    ],
];
?>

<div class="page-header">
    <h1>
        <?php echo Yii::t('OfferModule.offer', 'Characteristics'); ?>
        <small><?php echo Yii::t('OfferModule.offer', 'Administration'); ?></small>
    </h1>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'offer-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            'id',
            [
                'header' => Yii::t('OfferModule.offer', 'Image'),
                'value'  => '($data->icon_code?$data->icon_code:"")',
                'type'   => 'html'
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'name',
                'editable' => [
                    'url'    => $this->createUrl('/offer/characteristicBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'code',
                'editable' => [
                    'url'    => $this->createUrl('/offer/characteristicBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'code', ['class' => 'form-control']),
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'icon_code',
                'editable' => [
                    'url'    => $this->createUrl('/offer/characteristicBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'icon_code', ['class' => 'form-control']),
            ],

            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]
); ?>
