<?php
/**
 * Отображение для clientBackend/_search:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'action'      => Yii::app()->createUrl($this->route),
        'method'      => 'get',
        'type'        => 'vertical',
        'htmlOptions' => ['class' => 'well'],
    ]
);

?>

<fieldset>
    <div class="row">
        <div class="col-sm-4">
            <?php echo $form->textFieldGroup($model,'name');?>
        </div>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'offer_type_id',
                [
                    'widgetOptions' => [
                        'data'        => Category::model()->getParentChildrenList($model->offer_type_root_cat_id),
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'object_type_id',
                [
                    'widgetOptions' => [
                        'data'        => Category::model()->getParentChildrenList($model->object_type_root_cat_id),
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'city_id',
                [
                    'widgetOptions' => [
                        'data'        => Category::model()->getParentChildrenList($model->location_root_cat_id),
                        'htmlOptions' => [
                            'ajax' => array(
                                'type'   => 'POST',
                                'url'    => $this->createUrl('/offer/offerBackend/changeCity'),
                                'update' => '#'.CHtml::activeId($model,'district_id'),
                            ),
                            'empty' => Yii::t('OfferModule.offer', '--any--'),
                        ],
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'district_id',
                [
                    'widgetOptions' => [
                        'data'        => [],
                        'htmlOptions' => [
                            'empty' => Yii::t('OfferModule.offer', '--choose city--'),
                        ],
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-4">
            <?php echo $form->textFieldGroup($model,'address');?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?php echo $form->textFieldGroup($model,'price');?>
        </div>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'responsible_user_id',
                [
                    'widgetOptions' => [
                        'data'        => User::model()->getActiveUsersList(),
                        'htmlOptions' => [
                            'empty' => Yii::t('OfferModule.offer', '--any--'),
                        ],
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'client_id',
                [
                    'widgetOptions' => [
                        'data'        => Client::model()->getNotBlockedClientsList(),
                        'htmlOptions' => [
                            'empty' => Yii::t('OfferModule.offer', '--any--'),
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?php echo $form->textFieldGroup($model,'rooms');?>
        </div>
        <div class="col-sm-3">
            <?php echo $form->textFieldGroup($model,'floor');?>
        </div>
        <div class="col-sm-3">
            <?php echo $form->textFieldGroup($model,'area');?>
        </div>
        <div class="col-sm-3">
            <?php echo $form->textFieldGroup($model,'acres');?>
        </div>
    </div>
</fieldset>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'context'     => 'primary',
        'encodeLabel' => false,
        'buttonType'  => 'submit',
        'label'       => '<i class="fa fa-search">&nbsp;</i> ' . Yii::t('OfferModule.offer', 'Find an offer'),
    ]
); ?>

<?php $this->endWidget(); ?>
