<?php
/**
 * Отображение для offerBackend/_form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $form \yupe\widgets\ActiveForm
 * @var $model Offer
 * @var $this offerBackend
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm',
    [
        'id'                     => 'offer-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => ['class' => 'well'],
    ]
);

?>
<div class="alert alert-info">
    <?php echo Yii::t('OfferModule.offer', 'Fields marked with'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('OfferModule.offer', 'are required.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup(
            $model,
            'name',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('name'),
                        'data-content'        => $model->getAttributeDescription('name'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->slugFieldGroup(
            $model,
            'alias',
            [
                'sourceAttribute' => 'name',
                'widgetOptions'   => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('alias'),
                        'data-content'        => $model->getAttributeDescription('alias'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'offer_type_id',
            [
                'widgetOptions' => [
                    'data'        => Category::model()->getParentChildrenList($model->offer_type_root_cat_id),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('offer_type_id'),
                        'data-content'        => $model->getAttributeDescription('offer_type_id'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'object_type_id',
            [
                'widgetOptions' => [
                    'data'        => Category::model()->getParentChildrenList($model->object_type_root_cat_id),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('object_type_id'),
                        'data-content'        => $model->getAttributeDescription('object_type_id'),
						'ajax' => array(
                            'type'   => 'POST',
                            'url'    => $this->createUrl('/offer/offerBackend/changeObjectType'),
                            'update' => '#'.CHtml::activeId($model,'object_subtype_id'),
                        ),
                    ],
                ],
            ]
        ); ?>
    </div>    
    <?php if( !$model->isNewRecord && $model->object_type_id ) : ?>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'object_subtype_id',
                [
                    'widgetOptions' => [
                        'data'        => Category::model()->getParentChildrenList($model->object_type_id),
                        'htmlOptions' => [
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('object_type_id'),
                            'data-content'        => $model->getAttributeDescription('object_type_id'),
                        ],
                    ],
                ]
            ); ?>
        </div>
    <?php else : ?>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'object_subtype_id',
                [
                    'widgetOptions' => [
                        'data'        => [],
                        'htmlOptions' => [
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('object_type_id'),
                            'data-content'        => $model->getAttributeDescription('object_type_id'),
                            'empty' => Yii::t('OfferModule.offer', '--choose object type--'),
                        ],
                    ],
                ]
            ); ?>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'city_id',
            [
                'widgetOptions' => [
                    'data'        => Category::model()->getParentChildrenList($model->location_root_cat_id),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('city_id'),
                        'data-content'        => $model->getAttributeDescription('city_id'),
                        'ajax' => array(
                            'type'   => 'POST',
                            'url'    => $this->createUrl('/offer/offerBackend/changeCity'),
                            'update' => '#'.CHtml::activeId($model,'district_id'),
                        ),
                        'empty' => Yii::t('OfferModule.offer', '--choose--'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <?php if( !$model->isNewRecord && $model->city_id ) : ?>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'district_id',
                [
                    'widgetOptions' => [
                        'data'        => Category::model()->getParentChildrenList($model->city_id),
                        'htmlOptions' => [
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('district_id'),
                            'data-content'        => $model->getAttributeDescription('district_id'),
                        ],
                    ],
                ]
            ); ?>
        </div>
    <?php else : ?>
        <div class="col-sm-4">
            <?php echo $form->dropDownListGroup(
                $model,
                'district_id',
                [
                    'widgetOptions' => [
                        'data'        => [],
                        'htmlOptions' => [
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('district_id'),
                            'data-content'        => $model->getAttributeDescription('district_id'),
                            'empty' => Yii::t('OfferModule.offer', '--choose city--'),
                        ],
                    ],
                ]
            ); ?>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup(
            $model,
            'address',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('address'),
                        'data-content'        => $model->getAttributeDescription('address'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup(
            $model,
            'price',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('price'),
                        'data-content'        => $model->getAttributeDescription('price'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'responsible_user_id',
            [
                'widgetOptions' => [
                    'data'        => User::model()->getActiveUsersList(),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('responsible_user_id'),
                        'data-content'        => $model->getAttributeDescription('responsible_user_id'),
                        'empty' => Yii::t('OfferModule.offer', '--choose--'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'client_id',
            [
                'widgetOptions' => [
                    'data'        => Client::model()->getNotBlockedClientsList(),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('client_id'),
                        'data-content'        => $model->getAttributeDescription('client_id'),
                        'empty' => Yii::t('OfferModule.offer', '--choose--'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup(
            $model,
            'rooms',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('rooms'),
                        'data-content'        => $model->getAttributeDescription('rooms'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup(
            $model,
            'floor',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('floor'),
                        'data-content'        => $model->getAttributeDescription('floor'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup(
            $model,
            'area',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('area'),
                        'data-content'        => $model->getAttributeDescription('area'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup(
            $model,
            'acres',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('acres'),
                        'data-content'        => $model->getAttributeDescription('acres'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-8 popover-help" data-original-title='<?php echo $model->getAttributeLabel('description'); ?>'
         data-content='<?php echo $model->getAttributeDescription('description'); ?>'>
        <?php echo $form->labelEx($model, 'description'); ?>
        <?php
        $this->widget(
            $this->module->getVisualEditor(),
            [
                'model'     => $model,
                'attribute' => 'description',
            ]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-8">
        <?php echo $form->textFieldGroup(
            $model,
            'meta_keywords',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('meta_keywords'),
                        'data-content'        => $model->getAttributeDescription('meta_keywords'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-8">
        <?php echo $form->textFieldGroup(
            $model,
            'meta_description',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('meta_description'),
                        'data-content'        => $model->getAttributeDescription('meta_description'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'gallery_id',
            [
                'widgetOptions' => [
                    'data'        => Gallery::model()->getPublicGalleriesList(),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('gallery_id'),
                        'data-content'        => $model->getAttributeDescription('gallery_id'),
                        'empty' => Yii::t('OfferModule.offer', '--choose--'),
                    ],
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'status',
            [
                'widgetOptions' => [
                    'data'        => $model->getStatusList(),
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('client_id'),
                        'data-content'        => $model->getAttributeDescription('client_id'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
	<div class="col-sm-8">
		<?php echo $form->checkboxListGroup(
			$model,
			'export_templates',
			array(
				'widgetOptions' => array(
					'data' => ExportTemplate::getList($model->export_templates,($model->isNewRecord?false:$model)),
				),
			)
		); ?>
	</div>
</div>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => $model->isNewRecord ? Yii::t('OfferModule.offer', 'Create offer and continue') : Yii::t(
            'OfferModule.offer',
            'Save offer and continue'
        ),
    ]
); ?>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType'  => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label'       => $model->isNewRecord ? Yii::t('OfferModule.offer', 'Create offer and close') : Yii::t(
            'OfferModule.offer',
            'Save offer and close'
        ),
    ]
); ?>

<?php $this->endWidget(); ?>
