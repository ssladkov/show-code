<?php
/**
 * Отображение для offerBackend/index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('OfferModule.offer', 'Offers') => ['/offer/offerBackend/index'],
    Yii::t('OfferModule.offer', 'Administration'),
];

$this->pageTitle = Yii::t('OfferModule.offer', 'Offers - administration');

$this->menu = [
    [
        'label' => Yii::t('OfferModule.offer', 'Offers'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OfferModule.offer', 'Manage offers'),
                'url'   => ['/offer/offerBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add an offer'),
                'url'   => ['/offer/offerBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('OfferModule.offer', 'Object characteristics'),
        'items' => [
            [
                'icon' => 'fa fa-fw fa-check-square-o',
                'label' => Yii::t('OfferModule.offer', 'Offer characteristics list'),
                'url' => ['/offer/offerToCharacteristicBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add characteristic link'),
                'url' => ['/offer/offerToCharacteristicBackend/create']
            ],
        ]
    ],
];
?>

<div class="page-header">
    <h1>
        <?php echo Yii::t('OfferModule.offer', 'Offers'); ?>
        <small><?php echo Yii::t('OfferModule.offer', 'Administration'); ?></small>
    </h1>
</div>

<a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
    <i class="fa fa-search">&nbsp;</i>
    <?php echo Yii::t('OfferModule.offer', 'Find an offer'); ?>
    <span class="caret">&nbsp;</span>
</a>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('offer-grid', {
            data: $(this).serialize()
        });

        return false;
    });"
    );
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'offer-grid',
        'dataProvider' => $model->datesort()->search(),
        'filter'       => $model,
        'columns'      => [
           /* [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'name',
                'editable' => [
                    'url'    => $this->createUrl('/offer/offerBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],*/
            [
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'city_id',
                'value'    => '($data->city_id?$data->city->name:"---")',
                'url'     => $this->createUrl('/offer/offerBackend/inline'),
                'source'  => Category::model()->getParentChildrenList($model->location_root_cat_id),

            ],
            [
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'district_id',
                'value'    => '($data->district_id?$data->district->name:"---")',
                'url'     => $this->createUrl('/offer/offerBackend/inline'),
                'source'  => Category::model()->getAllParentsChildrenList($model->location_root_cat_id),

            ],
            [
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'offer_type_id',
                'value'    => '($data->offer_type_id?$data->offerType->name:"---")',
                'url'     => $this->createUrl('/offer/offerBackend/inline'),
                'source'  => Category::model()->getParentChildrenList($model->offer_type_root_cat_id),

            ],
            [
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'object_type_id',
                'value'    => '($data->object_type_id?$data->objectType->name:"---")',
                'url'     => $this->createUrl('/offer/offerBackend/inline'),
                'source'  => Category::model()->getParentChildrenList($model->object_type_root_cat_id),

            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'address',
                'editable' => [
                    'url'    => $this->createUrl('/offer/offerBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'address', ['class' => 'form-control']),
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'price',
                'editable' => [
                    'url'    => $this->createUrl('/offer/offerBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'price', ['class' => 'form-control']),
            ],

            [
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'responsible_user_id',
                'value'    => '($data->responsible_user_id?$data->responsibleUser->getFullName():"---")',
                'url'     => $this->createUrl('/offer/offerBackend/inline'),
                'source'  => User::getFullNameList(),

            ],
            [
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/offer/offerBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Offer::STATUS_DRAFT => ['class' => 'label-default'],
                    Offer::STATUS_ACTIVE  => ['class' => 'label-success'],
                    Offer::STATUS_REMOVED => ['class' => 'label-danger'],
                ],
            ],
            [
                'name'   => 'create_time',
                'value'  => 'Yii::app()->getDateFormatter()->formatDateTime($data->create_time, "short", "short")',
                'filter' => false
            ],
            [
                'header' => Yii::t('OfferModule.offer', 'Chars'),
                'value'  => 'CHtml::link($data->charsCount, array("/offer/offerToCharacteristicBackend/index","OfferToCharacteristic[offer_id]" => $data->id ))',
                'type'   => 'html'
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'template' => '{update} {view}'
            ],
        ],
    ]
); ?>
