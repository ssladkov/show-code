<?php
/**
 * Отображение для offerBackend/view:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('OfferModule.offer', 'Offers') => ['/offer/offerBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('OfferModule.offer', 'Offers - view');

$this->menu = [
    [
        'label' => Yii::t('OfferModule.offer', 'Offers'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OfferModule.offer', 'Manage offers'),
                'url'   => ['/offer/offerBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add an offer'),
                'url'   => ['/offer/offerBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('OfferModule.offer', 'Object characteristics'),
        'items' => [
            [
                'icon' => 'fa fa-fw fa-check-square-o',
                'label' => Yii::t('OfferModule.offer', 'Offer characteristics list'),
                'url' => ['/offer/offerToCharacteristicBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add characteristic link'),
                'url' => ['/offer/offerToCharacteristicBackend/create']
            ],
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('OfferModule.offer', 'Viewing offer'); ?><br/>
        <small><?php echo $model->name; ?></small>
    </h1>
</div>

<?php

$client_info = [];
if($model->client_id ) {
    $client_info[] = [
        'title' => Yii::t('OfferModule.offer', 'Client email'),
        'value' => $model->client->email
    ];
    if( $model->client->phone ) {
        $client_info[] = [
            'title' => Yii::t('OfferModule.offer', 'Client phone'),
            'value' => $model->client->phone
        ];
    }
    if( $model->client->mobile ) {
        $client_info[] = [
            'title' => Yii::t('OfferModule.offer', 'Client mobile'),
            'value' => $model->client->mobile
        ];
    }
}
$this->widget(
'bootstrap.widgets.TbDetailView',
[
    'data'       => $model,
    'attributes' => [
        [
            'name' => 'name',
            'value' => $model->name
        ],
        [
            'name' => 'alias',
            'value' => $model->alias
        ],
        [
            'name' => 'offer_type_id',
            'value' => ($model->offer_type_id?$model->offerType->name:"---")
        ],
        [
            'name' => 'object_type_id',
            'value' => ($model->object_type_id?$model->objectType->name:"---")
        ],
        [
            'name' => 'city_id',
            'value' => ($model->city_id?$model->city->name:"---")
        ],
        [
            'name' => 'district_id',
            'value' => ($model->district_id?$model->district->name:"---")
        ],
        [
            'name' => 'address',
            'value' => $model->address
        ],
        [
            'name' => 'price',
            'value' => $model->price
        ],
        'rooms',
        'floor',
        'area',
        'acres',
        'description',
        'meta_keywords',
        'meta_description',
        [
            'name' => 'gallery_id',
            'value' => ($model->gallery_id?$model->gallery->name:"---")
        ],
        [
            'name' => 'responsible_user_id',
            'value' => ($model->responsible_user_id?$model->responsibleUser->getFullName():"---")
        ],
        [
            'name' => 'client_id',
            'value' => ($model->client_id?$model->client->name:"---")
        ],
        $client_info,
        [
            'name'  => 'status',
            'value' => $model->getStatus(),
        ],
        [
            'name'  => 'create_time',
            'value' => Yii::app()->getDateFormatter()->formatDateTime($model->create_time, "short", "short"),
        ],
        [
            'name'  => 'update_time',
            'value' => Yii::app()->getDateFormatter()->formatDateTime($model->update_time, "short", "short"),
        ],
    ],
]
); ?>
