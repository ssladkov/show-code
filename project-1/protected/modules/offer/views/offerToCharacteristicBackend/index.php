<?php
/**
 * Отображение для OfferToCharacteristicBackend/index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('OfferModule.offer', 'Offer to characteristic links') => ['/offer/offerToCharacteristicBackend/index'],
    Yii::t('OfferModule.offer', 'Administration'),
];

$this->pageTitle = Yii::t('OfferModule.offer', 'Offer to characteristic link - administration');

$this->menu = [
    [
        'label' => Yii::t('OfferModule.offer', 'Offers'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OfferModule.offer', 'Manage offers'),
                'url'   => ['/offer/offerBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add an offer'),
                'url'   => ['/offer/offerBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('OfferModule.offer', 'Object characteristics'),
        'items' => [
            [
                'icon' => 'fa fa-fw fa-check-square-o',
                'label' => Yii::t('OfferModule.offer', 'Offer characteristics list'),
                'url' => ['/offer/offerToCharacteristicBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add characteristic link'),
                'url' => ['/offer/offerToCharacteristicBackend/create']
            ],
        ]
    ],
];
?>

<div class="page-header">
    <h1>
        <?php echo Yii::t('OfferModule.offer', 'Offer to characteristic links'); ?>
        <small><?php echo Yii::t('OfferModule.offer', 'Administration'); ?></small>
    </h1>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'offer-to-characteristic-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            [
                'name' => 'offer_id',
                'value' => '$data->offer->name',
                'filter' => Offer::getOffersList()
            ],
            [
                'header' => Yii::t('OfferModule.offer', 'Icon'),
                'value'  => '($data->characteristic->icon_code?$data->characteristic->icon_code:"")',
                'type'   => 'html'
            ],
            [
                'name' => 'characteristic_id',
                'value' => '$data->characteristic->name',
                'filter' => Characteristic::getCharacteristicsList()
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'value',
                'editable' => [
                    'url'    => $this->createUrl('/offer/offerToCharacteristicBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'value', ['class' => 'form-control']),
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'template' => '{delete}',
                'deleteButtonUrl' => function($data) {
                    return Yii::app()->createUrl('/offer/offerToCharacteristicBackend/delete', ['offer_id' => $data->offer_id, 'characteristic_id' => $data->characteristic_id]);
                }
            ],
        ],
    ]
); ?>
