<?php
/**
 * Отображение для OfferToCharacteristicBackend/_form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $form \yupe\widgets\ActiveForm
 * @var $model OfferToCharacteristic
 * @var $this OfferToCharacteristicBackendController
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm',
    [
        'id'                     => 'offer-to-characteristic-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => ['class' => 'well'],
    ]
);

?>
<div class="alert alert-info">
    <?php echo Yii::t('OfferModule.offer', 'Fields marked with'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('OfferModule.offer', 'are required.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'offer_id',
            [
                'widgetOptions' => [
                    'data'        => Offer::model()->getOffersList(),
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->dropDownListGroup(
            $model,
            'characteristic_id',
            [
                'widgetOptions' => [
                    'data'        => Characteristic::model()->getCharacteristicsList(),
                ],
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?php echo $form->textFieldGroup($model, 'value'); ?>
    </div>
</div>


<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => $model->isNewRecord ? Yii::t('OfferModule.offer', 'Create link and continue') : Yii::t(
            'OfferModule.offer',
            'Save link and continue'
        ),
    ]
); ?>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType'  => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label'       => $model->isNewRecord ? Yii::t('OfferModule.offer', 'Create link and close') : Yii::t(
            'OfferModule.offer',
            'Save link and close'
        ),
    ]
); ?>

<?php $this->endWidget(); ?>
