<?php
/**
 * Отображение для OfferToCharacteristicBackend/create:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('OfferModule.offer', 'Offer to characteristic link') => ['/offer/offerToCharacteristicBackend/index'],
    Yii::t('OfferModule.offer', 'Create'),
];

$this->pageTitle = Yii::t('OfferModule.offer', 'Offer to characteristic link - create');

$this->menu = [
    [
        'label' => Yii::t('OfferModule.offer', 'Offers'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OfferModule.offer', 'Manage offers'),
                'url'   => ['/offer/offerBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add an offer'),
                'url'   => ['/offer/offerBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('OfferModule.offer', 'Object characteristics'),
        'items' => [
            [
                'icon' => 'fa fa-fw fa-check-square-o',
                'label' => Yii::t('OfferModule.offer', 'Offer characteristics list'),
                'url' => ['/offer/offerToCharacteristicBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add characteristic link'),
                'url' => ['/offer/offerToCharacteristicBackend/create']
            ],
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('OfferModule.offer', 'Offer to characteristic links'); ?>
        <small><?php echo Yii::t('OfferModule.offer', 'Create'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>
