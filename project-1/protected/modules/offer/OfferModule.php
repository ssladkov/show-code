<?php

class OfferModule extends yupe\components\WebModule
{
    const VERSION = '0.1';

    public function getVersion()
    {
        return self::VERSION;
    }

    public function getIsInstallDefault()
    {
        return false;
    }

    public function getCategory()
    {
        return Yii::t('OfferModule.offer', 'Offers');
    }

    public function getName()
    {
        return Yii::t('OfferModule.offer', 'Offers');
    }

    public function getDescription()
    {
        return Yii::t('OfferModule.offer', 'Custom module Offers');
    }

    public function getAuthor()
    {
        return Yii::t('OfferModule.offer', 'Sergey Sladkov');
    }

    public function getAuthorEmail()
    {
        return Yii::t('OfferModule.offer', 'me@ssladkov.ru');
    }

    public function getUrl()
    {
        return Yii::t('OfferModule.offer', 'http://ssladkov.ru');
    }

    public function getIcon()
    {
        return "fa fa-fw fa-building";
    }

    public function getAdminPageLink()
    {
        return '/offer/offerBackend/index';
    }

    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-camera',
                'label' => Yii::t('OfferModule.offer', 'Images gallery'),
                'url' => ['/gallery/galleryBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OfferModule.offer', 'Offer list'),
                'url' => ['/offer/offerBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'New offer'),
                'url' => ['/offer/offerBackend/create']
            ],
            ['label' => Yii::t('OfferModule.offer', 'Object characteristics')],
            [
                'icon' => 'fa fa-fw fa-check-square-o',
                'label' => Yii::t('OfferModule.offer', 'Offer characteristics list'),
                'url' => ['/offer/offerToCharacteristicBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Add characteristic link'),
                'url' => ['/offer/offerToCharacteristicBackend/create']
            ],
            [
                'icon' => 'fa fa-fw fa-bed',
                'label' => Yii::t('OfferModule.offer', 'Characteristic manage'),
                'url' => ['/offer/characteristicBackend/index']
            ],

        ];
    }

    public function getAuthItems()
    {
        return [
            [
                'name' => 'Offer.OfferManager',
                'description' => Yii::t('OfferModule.offer', 'Manage offers'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    //offers
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferBackend.Create',
                        'description' => Yii::t('OfferModule.offer', 'Creating offer')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferBackend.Delete',
                        'description' => Yii::t('OfferModule.offer', 'Removing offer')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferBackend.Index',
                        'description' => Yii::t('OfferModule.offer', 'List of offers')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferBackend.Update',
                        'description' => Yii::t('OfferModule.offer', 'Editing offer')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferBackend.View',
                        'description' => Yii::t('OfferModule.offer', 'Viewing offers')
                    ],
                    //characteristics
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.CharacteristicBackend.Create',
                        'description' => Yii::t('OfferModule.offer', 'Creating characteristic')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.CharacteristicBackend.Delete',
                        'description' => Yii::t('OfferModule.offer', 'Removing characteristic')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.CharacteristicBackend.Index',
                        'description' => Yii::t('OfferModule.offer', 'List of characteristics')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.CharacteristicBackend.Update',
                        'description' => Yii::t('OfferModule.offer', 'Editing characteristic')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.CharacteristicBackend.View',
                        'description' => Yii::t('OfferModule.offer', 'Viewing characteristic')
                    ],
                    //offersToCharacteristics
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferToCharacteristicBackend.Create',
                        'description' => Yii::t('OfferModule.offer', 'Creating link offer-characteristic')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferToCharacteristicBackend.Delete',
                        'description' => Yii::t('OfferModule.offer', 'Removing link offer-characteristic')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferToCharacteristicBackend.Index',
                        'description' => Yii::t('OfferModule.offer', 'List of links offers-characteristics')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferToCharacteristicBackend.Update',
                        'description' => Yii::t('OfferModule.offer', 'Editing link offer-characteristic')
                    ],

                ]
            ]
        ];
    }
	public function init()
	{
        parent::init();

        $this->setImport(
            [
                'application.modules.offer.models.*',
                'application.modules.offer.widgets.*',
                'application.modules.gallery.models.*'
            ]
        );

        // Если у модуля не задан редактор - спросим у ядра
        if (!$this->editor) {
            $this->editor = Yii::app()->getModule('yupe')->editor;
        }
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
