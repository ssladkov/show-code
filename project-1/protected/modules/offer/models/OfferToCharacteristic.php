<?php

/**
 * This is the model class for table "{{offer_offer_to_characteristic}}".
 *
 * The followings are the available columns in table '{{offer_offer_to_characteristic}}':
 * @property string $offer_id
 * @property string $characteristic_id
 * @property string $value
 */
class OfferToCharacteristic extends yupe\models\YModel
{
    public function primaryKey() {
        return array('offer_id','characteristic_id');
    }
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{offer_offer_to_characteristic}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('offer_id, characteristic_id, value', 'required'),
			array('offer_id, characteristic_id', 'length', 'max'=>10),
			array('value', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('offer_id, characteristic_id, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'offer' => array(self::BELONGS_TO, 'Offer', 'offer_id'),
            'characteristic' => array(self::BELONGS_TO, 'Characteristic', 'characteristic_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'offer_id' => Yii::t('OfferModule.offer', 'Offer'),
			'characteristic_id' => Yii::t('OfferModule.offer', 'Characteristic'),
			'value' => Yii::t('OfferModule.offer', 'Value'),
		);
	}

    public static function getOfferValue($offer_id,$characteristic_id) {
        $pk = compact( array( "offer_id", "characteristic_id" ) );
        $model = OfferToCharacteristic::model()->cache(Yii::app()->getModule('yupe')->coreCacheTime)->findByPk($pk);
        return ($model === null ? null : $model->value);
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('offer_id',$this->offer_id);
		$criteria->compare('characteristic_id',$this->characteristic_id);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OfferToCharacteristic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
