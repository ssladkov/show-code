<?php

/**
 * This is the model class for table "{{offer_characteristic}}".
 *
 * The followings are the available columns in table '{{offer_characteristic}}':
 * @property string $id
 * @property string $code
 * @property string $name
 * @property string $icon_code
 *
 * The followings are the available model relations:
 * @property OfferOffer[] $yupeOfferOffers
 */
class Characteristic extends yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{offer_characteristic}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, name', 'required'),
			array('code, name', 'length', 'max'=>45),
			array('icon_code', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, name, icon_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'offers' => array(self::MANY_MANY, 'OfferOffer', '{{offer_offer_to_characteristic}}(charateristic_id, offer_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => Yii::t('OfferModule.offer', 'Code'),
			'name' => Yii::t('OfferModule.offer', 'Name'),
			'icon_code' => Yii::t('OfferModule.offer', 'Icon code'),
		);
	}
    public static function getCharacteristicsList()
    {
        $chars = [];
        foreach( Characteristic::model()->findAll() as $characteristic ) {
            $chars[$characteristic->id] = $characteristic->name;
        }
        return $chars;
    }

    public function getByCode($code) {
        return self::model()->cache(Yii::app()->getModule('yupe')->coreCacheTime)->find(
            'code = :code',
            [
                ':code' => $code
            ]
        );
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('icon_code',$this->icon_code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Characteristic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
