<?php

/**
 * This is the model class for table "{{offer_offer}}".
 *
 * The followings are the available columns in table '{{offer_offer}}':
 * @property string $id
 * @property string $name
 * @property string $alias
 * @property string $address
 * @property integer $price
 * @property string $description
 * @property integer $gallery_id
 * @property integer $create_user_id
 * @property integer $responsible_user_id
 * @property integer $offer_type_id
 * @property integer $object_type_id
 * @property string $client_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $status
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $city_id
 * @property integer $district_id
 * @property integer $rooms
 * @property integer $floor
 * @property integer $area
 * @property integer $acres
 *
 * The followings are the available model relations:
 * @property ClientRequest[] $clientRequests
 * @property CategoryCategory $objectType
 * @property GalleryGallery $gallery
 * @property UserUser $createUser
 * @property UserUser $responsibleUser
 * @property ClientClient $client
 * @property CategoryCategory $city
 * @property CategoryCategory $district
 * @property CategoryCategory $offerType
 * @property OfferCharacteristic[] $yupeOfferCharacteristics
 */
Yii::import('application.modules.offer.OfferModule');
Yii::import('application.modules.gallery.models.Gallery');
Yii::import('application.modules.dictionary.models.DictionaryData');
Yii::import('application.modules.offersexport.models.ExportTemplate');
Yii::import('application.modules.offersexport.models.ExportTemplateToOffer');

class Offer extends yupe\models\YModel
{
	const BLOCK_IMAGE_HEIGHT = 195;
	const BLOCK_IMAGE_WIDTH = 265;
    const LIST_IMAGE_HEIGHT = 100;
    const LIST_IMAGE_WIDTH = 130;
    const THUMB_IMAGE_HEIGHT = 72;
    const THUMB_IMAGE_WIDTH = 96;
    const FULL_IMAGE_HEIGHT = 400;
    const FULL_IMAGE_WIDTH = 700;

    const SHORT_DESCRIPTION_LENGTH = 150;

    const ROOT_PATH = '/predlozheniya/';

    const STATUS_ACTIVE = 1;
    const STATUS_DRAFT = 0;
    const STATUS_REMOVED = 2;

    /** Эти данные используются для поиска данных в справочниках. НЕ МЕНЯЙТЕ ИХ! Если не уверены в том, что делаете */
    const OFFER_TYPE_ROOT_CATEGORY_ALIAS = 'deal-types';
    const OBJECT_TYPE_ROOT_CATEGORY_ALIAS = 'purpose-types';
    const LOCATION_ROOT_CATEGORY_ALIAS = 'location';

    const HOUSE_DIRECTION_PATH_CHAR_CODE = 'road';

    const OBJECT_TYPE_HOUSES_CODE = 'zagorodnaya-nedvizhimost';

    const OFFER_TYPE_RENT_CODE = 'arenda';
    const OFFER_TYPE_SELL_CODE = 'prodazha';
	const OFFER_SUBTYPE_ROOM = 'komnata';
	const OFFER_SUBTYPE_ROOM_OLD = 'komnata-old';

    const SORT_ORDER_PRICE_DESC = 'price_desc';
    const SORT_ORDER_PRICE_ASC = 'price';
    const SORT_ORDER_DATE_DESC = 'date_desc';
    const SORT_ORDER_DATE_ASC = 'date';

    const LIST_VIEW_TYPE_GRID = 'grid';
    const LIST_VIEW_TYPE_LIST = 'list';

    public $offer_type_root_cat_id;
    public $object_type_root_cat_id;
    public $location_root_cat_id;
    public $house_direction_char_id;

    public $all_but_rent = false;

    /** Дополнительные поля для фильтра */
    public $price_low_limit;
    public $price_upper_limit;
    public $area_low_limit;
    public $area_upper_limit;
    public $sort_order = 'date_desc';
    public $view_type = 'grid';
    public $offer_subtype_room;
    public $offer_subtype_room_old;
	
	public $export_templates = [];

    private $_characteristics = [];

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{offer_offer}}';
	}

    public function init() {
        $catModel = new Category();
        $charModel = new Characteristic();
        $this->offer_type_root_cat_id = $catModel->getByAlias(self::OFFER_TYPE_ROOT_CATEGORY_ALIAS)->id;
        $this->offer_subtype_room = $catModel->getByAlias(self::OFFER_SUBTYPE_ROOM)->id;
        $this->offer_subtype_room_old = $catModel->getByAlias(self::OFFER_SUBTYPE_ROOM_OLD)->id;
        $this->object_type_root_cat_id = $catModel->getByAlias(self::OBJECT_TYPE_ROOT_CATEGORY_ALIAS)->id;
        $this->location_root_cat_id = $catModel->getByAlias(self::LOCATION_ROOT_CATEGORY_ALIAS)->id;
        $this->house_direction_char_id = $charModel->getByCode(self::HOUSE_DIRECTION_PATH_CHAR_CODE)->id;
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, alias, price, offer_type_id, object_type_id, object_subtype_id, status', 'required', 'on' => 'search'),
            array('name, alias', 'unique', 'on' => 'search'),
            array(
                'alias',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('OfferModule.offer', 'Illegal characters in {attribute}')
            ),
			array('price, gallery_id, create_user_id, responsible_user_id, offer_type_id, object_type_id, object_subtype_id, status, city_id, district_id, rooms, floor, area, acres', 'numerical', 'integerOnly'=>true),
			array('name, alias, address, meta_keywords, meta_description', 'length', 'max'=>255),
			array('client_id', 'length', 'max'=>10),
			array('description, update_time, export_templates', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, alias, address, price, description, gallery_id, create_user_id, responsible_user_id, offer_type_id, object_type_id, object_subtype_id, client_id, create_time, update_time, status, meta_keywords, meta_description, city_id, district_id, rooms, floor, area, acres', 'safe', 'on'=>'search'),
			array('offer_type_id, object_type_id, city_id, district_id, rooms, floor, area_upper_limit, area_low_limit, price_upper_limit, price_low_limit, sort_order, view_type', 'safe', 'on'=>'frontend-search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clientRequests' => array(self::HAS_MANY, 'Request', 'offer_id'),
			'objectType' => array(self::BELONGS_TO, 'Category', 'object_type_id'),
			'gallery' => array(self::BELONGS_TO, 'Gallery', 'gallery_id'),
			'createUser' => array(self::BELONGS_TO, 'User', 'create_user_id'),
			'responsibleUser' => array(self::BELONGS_TO, 'User', 'responsible_user_id'),
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
			'city' => array(self::BELONGS_TO, 'Category', 'city_id'),
			'district' => array(self::BELONGS_TO, 'Category', 'district_id'),
			'offerType' => array(self::BELONGS_TO, 'Category', 'offer_type_id'),
			'characteristics' => array(self::HAS_MANY, 'OfferToCharacteristic', 'offer_id'),
			'offerCharacteristics' => array(self::MANY_MANY, 'Characteristic', '{{offer_offer_to_characteristic}}(offer_id, charateristic_id)'),
            'charsCount'   => [
                self::STAT,
                'OfferToCharacteristic',
                'offer_id',
            ],
			'exportTemplates'      => [self::HAS_MANY, 'ExportTemplateToOffer', 'offer_id'],
		);
	}

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'datesort'    => [
                'order' => 'create_time DESC'
            ],            
			'updatesort'    => [
                'order' => 'update_time DESC'
            ],
            'active' => [
                'condition' => 't.status = :status',
                'params' => [
                    ':status' => self::STATUS_ACTIVE
                ],
            ],
        ];
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('OfferModule.offer', 'Name'),
			'alias' => Yii::t('OfferModule.offer', 'Alias'),
			'address' => Yii::t('OfferModule.offer', 'Address'),
			'price' => Yii::t('OfferModule.offer', 'Price'),
			'description' => Yii::t('OfferModule.offer', 'Description'),
			'gallery_id' => Yii::t('OfferModule.offer', 'Gallery'),
			'create_user_id' => Yii::t('OfferModule.offer', 'Create user'),
			'responsible_user_id' => Yii::t('OfferModule.offer', 'Responsible user'),
			'offer_type_id' => Yii::t('OfferModule.offer', 'Offer type'),
			'object_type_id' => Yii::t('OfferModule.offer', 'Object type'),
			'object_subtype_id' => Yii::t('OfferModule.offer', 'Object subtype'),
			'client_id' => Yii::t('OfferModule.offer', 'Owner'),
			'create_time' => Yii::t('OfferModule.offer', 'Create date'),
			'update_time' => Yii::t('OfferModule.offer', 'Update date'),
			'status' => Yii::t('OfferModule.offer', 'Status'),
			'meta_keywords' => Yii::t('OfferModule.offer', 'Meta keywords'),
			'meta_description' => Yii::t('OfferModule.offer', 'Meta description'),
			'city_id' => Yii::t('OfferModule.offer', 'City'),
			'district_id' => Yii::t('OfferModule.offer', 'District'),
			'rooms' => Yii::t('OfferModule.offer', 'Rooms'),
			'floor' => Yii::t('OfferModule.offer', 'Floor'),
			'area' => Yii::t('OfferModule.offer', 'Area'),
			'acres' => Yii::t('OfferModule.offer', 'Acres'),
			'price_low_limit' => Yii::t('OfferModule.offer', 'Minimum'),
			'price_upper_limit' => Yii::t('OfferModule.offer', 'Maximum'),
            'area_low_limit' => Yii::t('OfferModule.offer', 'Minimum'),
			'area_upper_limit' => Yii::t('OfferModule.offer', 'Maximum'),
			'export_templates' => Yii::t('OfferModule.offer', 'Export templates'),
		);
	}

    /**
     * @return array customized attribute descriptions (name=>description)
     */
    public function attributeDescriptions()
    {
        return [
            'name'        => Yii::t(
                'OfferModule.offer',
                'Please enter a name of the offer. It will be visible on the site in page title and used for inner identification. For example: <span class="label label-default">Buy flat in Moscow, Tverskaya, 11</span>. Use unique names'
            ),
            'alias' => Yii::t(
                'OfferModule.offer',
                'Please enter an URL-friendly name for the offer.<br /><br /> For example: <pre>http://rplus.ru/offers/<span class="label label-default">kupit-kvartiru-1552</span>/</pre> If you don\'t know how to fill this field you can leave it empty.'
            ),
            'address'        => Yii::t(
                'OfferModule.offer',
                'Please enter an address of the offer.<br /><br /> For example: <span class="label label-default">Moscow, Tverskaja, 11</span>'
            ),
            'price'        => Yii::t(
                'OfferModule.offer',
                'Please enter a price of the offer'
            ),
            'description'        => Yii::t(
                'OfferModule.offer',
                'Please enter a description of the offer'
            ),
            'gallery_id'        => Yii::t(
                'OfferModule.offer',
                'Please choose a gallery, from which will be taken photos for the offer'
            ),
            'responsible_user_id'        => Yii::t(
                'OfferModule.offer',
                'Please choose a manager, who will be responsible for the offer. This manager will be notified, if user send a request for this offer'
            ),
            'offer_type_id'        => Yii::t(
                'OfferModule.offer',
                'Please choose an offer type, it is used on the purposes filter'
            ),
            'object_type_id'        => Yii::t(
                'OfferModule.offer',
                'Please choose an object type, it is used on the purposes filter'
            ),
            'client_id'        => Yii::t(
                'OfferModule.offer',
                'Please choose a client, who is an owner of the object'
            ),
            'meta_keywords'        => Yii::t(
                'OfferModule.offer',
                'SEO keywords separated by comma. For example, if your post is about your seaside vacation keyword would be: <pre>buy a flat, 3 rooms, zelenograd, 15 microdistrict, etc.</pre>'
            ),
            'meta_description'        => Yii::t(
                'OfferModule.offer',
                'SEO description.'
            ),
            'city_id'        => Yii::t(
                'OfferModule.offer',
                'Please choose a city, where object is situated, it is used in the purposes filter'
            ),
            'district_id'        => Yii::t(
                'OfferModule.offer',
                'Please choose a district, where object is situated, it is used in the purposes filter'
            ),
            'rooms'        => Yii::t(
                'OfferModule.offer',
                'Please enter a rooms number of the object, it is need only in flats'
            ),
            'floor'        => Yii::t(
                'OfferModule.offer',
                'Please enter a floor number of the object, it is need only in flats. In object details use additional characteristic'
            ),
            'area'        => Yii::t(
                'OfferModule.offer',
                'Please enter a total area of the object. In object details use additional characteristic'
            ),
            'acres'        => Yii::t(
                'OfferModule.offer',
                'Please enter a total acres of the object, it is need only in country estate objects'
            ),
            'status'      => Yii::t(
                'OfferModule.offer',
                'Please choose a status of the offer:<br /><br /><span class="label label-success">active</span> &ndash; The offer will be visible on the site<br /><br /><span class="label label-default">draft</span> &ndash; The offer will not be visible on the site, this status is used, when editing or approoving requreied.<br /><br /><span class="label label-danger">removed</span> &ndash; The offer is no relevant more. It will be invisible on the site and in some lists in the site backend'
            ),
        ];
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('gallery_id',$this->gallery_id);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('responsible_user_id',$this->responsible_user_id);
		$criteria->compare('offer_type_id',$this->offer_type_id);
		$criteria->compare('object_type_id',$this->object_type_id);
		$criteria->compare('object_subtype_id',$this->object_subtype_id);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('district_id',$this->district_id);
		$criteria->compare('rooms',$this->rooms);
		$criteria->compare('floor',$this->floor);
		$criteria->compare('area',$this->area);
		$criteria->compare('acres',$this->acres);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /** 
	* Метод для поиска (фильтра) предложений на фронтенде
	* @return CActiveDataProvider
	*/
	public function frontendSearch()
	{

		$criteria=new CDbCriteria;

        if (isset($this->price_low_limit) && isset($this->price_upper_limit)) {
            $criteria->addBetweenCondition(
                'price',
                $this->price_low_limit,
                $this->price_upper_limit,
                'AND'
            );
        }

        if (isset($this->area_low_limit) && isset($this->area_upper_limit)) {
            $criteria->addBetweenCondition(
                'area',
                $this->area_low_limit,
                $this->area_upper_limit,
                'AND'
            );
        }

        $sort_field = $this->sort_order == self::SORT_ORDER_PRICE_DESC || $this->sort_order == self::SORT_ORDER_PRICE_ASC ? 'price' : 'create_time';
        $sort_order = $this->sort_order == self::SORT_ORDER_PRICE_DESC || $this->sort_order == self::SORT_ORDER_DATE_DESC ? CSort::SORT_DESC : CSort::SORT_ASC;

        if( $this->all_but_rent !== false ) {
            $criteria->compare('offer_type_id',$this->all_but_rent,false,'<>');
        }
        else
            $criteria->compare('offer_type_id',$this->offer_type_id);

        $criteria->compare('status',self::STATUS_ACTIVE);

		$criteria->compare('object_type_id',$this->object_type_id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('district_id',$this->district_id);
		if( $this->rooms == 99 ) {
			$criteria->addInCondition('object_subtype_id', [$this->offer_subtype_room, $this->offer_subtype_room_old]);
		}
		else {
			$criteria->addNotInCondition('object_subtype_id', [$this->offer_subtype_room, $this->offer_subtype_room_old]);
			$criteria->compare('rooms',$this->rooms);
		}
		$criteria->compare('floor',$this->floor);
		$criteria->compare('area',$this->area);


        $dependecy = new CDbCacheDependency('SELECT MAX(create_time) FROM yupe_offer_offer');
        return new CActiveDataProvider(self::model()->cache(Yii::app()->getModule('yupe')->coreCacheTime, $dependecy, 2), array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>12,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    $sort_field => $sort_order
                )
            )
		));
	}
	
    /** 
	* Получение конкретного предложение по его алиасу
	* @return Offer|null
	*/
    public static function getModelByAlias($alias=null) {
        if( $alias === null )
            return null;
        return Offer::model()->cache(Yii::app()->getModule('yupe')->coreCacheTime)->find('alias = :alias',[':alias' => $alias]);
    }
	
	/** 
	* Получение отсортированного списка всех предложений
	* @return array Массив предложений в формате (Offer->id=>Offer->name)
	*/
    public static function getOffersList()
    {
        $offers = [];
        foreach( Offer::model()->datesort()->findAll() as $offer ) {
            $offers[$offer->id] = $offer->name;
        }
        return $offers;
    }

	/** 
	* Получение массива со всевозможным количеством комнат для фильтра предложений (99 - комната) исходя из максимально и минимально возможных значений
	* @return array (number=>number)
	*/
    public static function getFilterRoomsList()
    {
        $min = DictionaryData::model()->getByCode('min-rooms')->value;
        $max = DictionaryData::model()->getByCode('max-rooms')->value;
        $list = [];
		$list[99] =  Yii::t('OfferModule.offer', 'room');
        for($i = $min; $i <= $max; $i++) {
            $list[$i] = $i;
        }
        return $list;
    }

	/** 
	* Получение массива со всевозможным этажности зданий для фильтра предложений, исходя из максимально и минимально возможных значений
	* @return array (number=>number)
	*/
    public static function getFilterFloorsList()
    {
        $min = DictionaryData::model()->getByCode('min-floor')->value;
        $max = DictionaryData::model()->getByCode('max-floor')->value;
        $list = [];
        for($i = $min; $i <= $max; $i++) {
            $list[$i] = $i;
        }
        return $list;
    }

	/** 
	* Получение значения справочника по коду
	* @return string|null 
	*/
    public function getDictValueByCode($code) {
        $dictionary =  DictionaryData::model()->getByCode($code);
        return ($dictionary === null ? null : $dictionary->value);
    }

    /**
     * @return array Массив возможных статусов предложения: черновик, активное, удалено (number=>string)
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('OfferModule.offer', 'Draft'),
            self::STATUS_ACTIVE  => Yii::t('OfferModule.offer', 'Active'),
            self::STATUS_REMOVED => Yii::t('OfferModule.offer', 'Removed'),
        ];
    }
    /**
     * Получение статуса конкретного предложения или dafault-строки для нового предложения
	 * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('OfferModule.offer', '*unknown*');
    }
	
	/** 
	* Получение html-кода списка для выбора способа сортировки в фильтре предложений
	* @param string $form_id id формы фильтра
	* @param string $listview_id id контейтера gridView
	* @param string $field_name свойство модели для применения фильтра
	* @return string (html)
	*/
    private function _getSortInterface($form_id,$listview_id,$field_name) {
        $data = [
            self::SORT_ORDER_DATE_ASC => '',
            self::SORT_ORDER_DATE_DESC => '',
            self::SORT_ORDER_PRICE_ASC => '',
            self::SORT_ORDER_PRICE_DESC => '',
        ];
        $data[$this->sort_order] = 'selected="selected"';
        $output = '
            <label for="sort_selector">'.Yii::t('OfferModule.offer', 'Sort by').': </label>
            <select style="padding: 0 5px;" class="form-control event-change-listview-sort" data-form-id="'.$form_id.'" data-listview-id="'.$listview_id.'" data-field-id="'.$field_name.'" id="sort_selector">
                <option value="'.self::SORT_ORDER_DATE_ASC.'" '.$data[self::SORT_ORDER_DATE_ASC].'>'.Yii::t('OfferModule.offer', 'date ascending').'</option>
                <option value="'.self::SORT_ORDER_DATE_DESC.'" '.$data[self::SORT_ORDER_DATE_DESC].'>'.Yii::t('OfferModule.offer', 'date descending').'</option>
                <option value="'.self::SORT_ORDER_PRICE_ASC.'" '.$data[self::SORT_ORDER_PRICE_ASC].'>'.Yii::t('OfferModule.offer', 'increasing price').'</option>
                <option value="'.self::SORT_ORDER_PRICE_DESC.'" '.$data[self::SORT_ORDER_PRICE_DESC].'>'.Yii::t('OfferModule.offer', 'price reduction').'</option>
            </select>';
        return $output;
    }

	/** 
	* Получение html-кода списка для выбора способа отображения предложений в фильтре предложений
	* @param string $form_id id формы фильтра
	* @param string $listview_id id контейтера gridView
	* @param string $field_name свойство модели для применения фильтра
	* @return string (html)
	*/
    private function _getChangeViewInterface($form_id,$listview_id,$field_name) {
        $output = '<label>'.Yii::t('OfferModule.offer', 'View').':</label> <div class="btn-group event-change-listview-view" data-toggle="buttons" data-form-id="'.$form_id.'" data-listview-id="'.$listview_id.'" data-field-id="'.$field_name.'">
              <label class="'.($this->view_type == self::LIST_VIEW_TYPE_GRID?'btn btn-success':'btn btn-default').'">
                <input type="radio" value="grid" name="view_types" id="grid" autocomplete="off" '.($this->view_type == self::LIST_VIEW_TYPE_GRID?'checked="checked"':'').'><span class="glyphicon glyphicon-th" aria-hidden="true"></span>
              </label>
              <label class="'.($this->view_type == self::LIST_VIEW_TYPE_LIST?'btn btn-success':'btn btn-default').'">
                <input type="radio" value="list" name="view_types" id="list" autocomplete="off" '.($this->view_type == self::LIST_VIEW_TYPE_LIST?'checked="checked"':'').'><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
              </label>
            </div>';
        return $output;
    }
	
	/** 
	* Получение строки адреса с первым (основным) изображением конкретного предложения для способа отображения "плитка" 
	* @return string
	*/
    public function getImageBlockURL() {
        $imgUrl = "/uploads/dummy/nophoto-offer-block.jpg";
        if( $this->gallery_id ) {
            $imgUrl = $this->gallery->cache(Yii::app()->getModule('yupe')->coreCacheTime)->images[0]->getImageUrl( self::BLOCK_IMAGE_WIDTH, self::BLOCK_IMAGE_HEIGHT, true);
        }
        return $imgUrl;
    }

	/** 
	* Получение строки адреса с первым (основным) изображением конкретного предложения для способа отображения "список" 
	* @return string
	*/
    public function getImageListURL() {
        $imgUrl = "/uploads/dummy/nophoto-offer-block.jpg";
        if( $this->gallery_id ) {
            $imgUrl = $this->gallery->cache(Yii::app()->getModule('yupe')->coreCacheTime)->images[0]->getImageUrl( self::LIST_IMAGE_WIDTH, self::LIST_IMAGE_HEIGHT, true);
        }
        return $imgUrl;
    }


	/** 
	* Получение массива адресов со всеми изображениями и миниатюрами конкретного предложения
	* @return array (['big' => ['alt' => string, 'url' => string], 'thumb' => string])
	*/	
    public function getAllOfferImages() {
        $imgArr[] = [
            'big' => [
                'alt' => Yii::t('OfferModule.offer', 'No photo'),
                'url' => "/uploads/service/no-image-big.png"
            ],
            'thumb' => ''
        ];
        if( $this->gallery_id ) {
            $imgArr = [];
            $images = $this->gallery->images;
            foreach( $images as $image ) {
                $imgArr[] = [
                    'big' => [
                        'alt' => $image->name,
                        'url' => $image->getImageURL(self::FULL_IMAGE_WIDTH, self::FULL_IMAGE_HEIGHT, false, null, true, $this->gallery->watermarkPath)
                    ],
                    'thumb' => $image->getImageURL(self::THUMB_IMAGE_WIDTH, self::THUMB_IMAGE_HEIGHT, true)
                ];
            }
        }
        return $imgArr;
    }

	/** 
	* Получение массива со списком всех характеристик конкретного предложения
	* @return array ([string (char code) => ['icon_code' => string, 'name' => string, 'value' => string]])
	*/	
    public function getAllCharacteristics() {
        $cacheName = "OfferCharacteristics{$this->id}";
        $data = Yii::app()->cache->get($cacheName);
        if( $data === false ) {
            $charArr = [];
            $charArr['price'] = [
                'icon_code' => '<img src="/uploads/char-icons/price.png"/>',
                'name' => Yii::t('OfferModule.offer', 'Price'),
                'value' => '<span class="label label-success">'.$this->formattedPrice.'</span>'
            ];
            $charArr['address'] = [
                'icon_code' => '<img src="/uploads/char-icons/address.png"/>',
                'name' => Yii::t('OfferModule.offer', 'Address '),
                'value' => $this->charAddress
            ];
            if( $this->rooms ) {
                $charArr['rooms'] = [
                    'icon_code' => '<img src="/uploads/char-icons/rooms.png"/>',
                    'name' => Yii::t('OfferModule.offer', 'Rooms '),
                    'value' => $this->rooms
                ];
            }
            if( $this->acres ) {
                $charArr['acres'] = [
                    'icon_code' => '<img src="/uploads/char-icons/acres.png"/>',
                    'name' => Yii::t('OfferModule.offer', 'Land square, acres'),
                    'value' => $this->acres
                ];
            }
            foreach( $this->characteristics as $offerChar ) {
                $charArr[$offerChar->characteristic->code] = [
                    'icon_code' => $offerChar->characteristic->icon_code,
                    'name' => $offerChar->characteristic->name,
                    'value' => $offerChar->value
                ];
            }
            $data = $charArr;
            Yii::app()->cache->set($cacheName, $data);
        }
        return $data;
    }

	/** 
	* Получение массива адресов со всеми миниатюрами конкретного предложения
	* @return array (key => string)
	*/	
    public function getAllThumbs() {
        $imagesArr = [];
        if( $this->gallery_id ) {
            foreach( $this->gallery->cache(Yii::app()->getModule('yupe')->coreCacheTime)->images as $image ) {
                $imagesArr[] = $image->getImageURL(self::THUMB_IMAGE_WIDTH, self::THUMB_IMAGE_HEIGHT, true);
            }
        }
        return $imagesArr;
    }

	/** 
	* Получение строки персонального URL конкретного предложения
	* @return string
	*/	
    public function getURL() {
        return self::ROOT_PATH . $this->objectType->cache(Yii::app()->getModule('yupe')->coreCacheTime)->slug . '/' . $this->alias;
    }

	/** 
	* Получение строки названия типа объекта конкретного предложения
	* @return string
	*/	
    public function getObjectTypeName() {
        return $this->objectType->cache(Yii::app()->getModule('yupe')->coreCacheTime)->name;
    }

	/** 
	* Получение короткого описания конкретного предложения
	* @return string
	*/	
    public function getShortDescription(){
        $text=strip_tags($this->description);
        $length = self::SHORT_DESCRIPTION_LENGTH;
        if( mb_strlen($text, 'UTF-8') > $length )
        {
            $pos = mb_strpos($text, ' ', $length, 'UTF-8');
            $text = mb_substr($text, 0, $pos, 'UTF-8');
            $text = $text.'...';
        }
        return str_replace("\r\n", ' ', $text);
    }

	/** 
	* Получение отформатированной стоимости конкретного предложения
	* @param bool $withCurrency добавлять ли к стоимости валюту
	* @return string
	*/	
    public function getFormattedPrice($withCurrency=true) {
        $price = Yii::t('OfferModule.offer', 'not specified');
        if( $this->price ) {
            $tail = Yii::t('OfferModule.offer', 'rub') . ($this->offerType->cache(Yii::app()->getModule('yupe')->coreCacheTime)->slug == self::OFFER_TYPE_RENT_CODE ? Yii::t('OfferModule.offer', '/mth') : '');
            $price = number_format( $this->price, 0, ',', ' ') . ($withCurrency ? ' '. $tail : '');
        }
        return $price;
    }

	/** 
	* Получение полного адреса конкретного предложения
	* @param string $separator соединитель для элементов строки адреса
	* @return string
	*/	
    public function getFullAddress($separator=', ') {
        $address = $this->address;

        if( $this->district_id ) {
            //$address = '<span class="text-nowrap">' . $this->district->name . '</span>' . $separator. $address;
            $address = $this->district->name . $separator. $address;
        }
        if( $this->city_id ) {
            $city_name = $this->city->slug == 'mosk-oblast' ? 'МО' : $this->city->name;
            $address = '<span class="color-blue">' . $city_name . '</span>' . $separator . $address;
        }
        return $address;
    }

	/** 
	* Получение строки адреса конкретного предложения отформатированного для показа на интерактивной яндекс.карте
	* @param string $separator соединитель для элементов строки адреса
	* @return string
	*/	
    public function getMapAddress($separator=', ') {
        $address = $this->address;
        //надо добавить в адрес поиска по карте район, если сам адрес пуст, или если это загородная недвижка (кол-во комнат незадано)
        if( $address == '' || !$this->rooms ) {
            $address = ( $this->district_id ? $this->district->name . $separator : '' ) . $address;
        }
        if( $this->city_id ) {
            $address = $this->city->name . $separator . $address;
        }
        return $address;
    }
	
	/** 
	* Получение строки сокращенного адреса конкретного предложения для показа в таблице характеристик
	* @param string $separator соединитель для элементов строки адреса
	* @return string
	*/	
    public function getCharAddress($separator=', ') {
        $address = $this->address;
        if( $address == '' ) {
            $address = $this->district_id ? $this->district->name : '';
        }
        if( $this->city_id ) {
            $address = $this->city->name . $separator . $address;
        }
        return $address;
    }

	/** 
	* Получение отформатированного названия конкретного предложения
	* @param string $container тег-обертка для части названия
	* @return string
	*/	
    public function getFormattedTitle($container='div') {
        //подразумевается, что количество слов "купить дом", "снять квартиру" и т.д., всегда = 2, и нам их надо выделять
        $nameArr = explode(' ', $this->name, 3);
        //убираем возможные запятые во втором слове
        $nameArr[1] = str_replace(',', '', $nameArr[1]);
        //убираем вторую часть в контейнер
        $nameArr[2] = '<' . $container . '>' . $nameArr[2] . '</' . $container . '>';
        return implode(' ',$nameArr);
    }

	/** 
	* Получение html-кода для отображения основных характеристик конкретного предложения (для показа в списках)
	* @return string html
	*/	
    public function getMetaIconsHTML() {
        $output = '';
        if( $this->objectType->slug != self::OBJECT_TYPE_HOUSES_CODE ) {
            $output.= '<div class="ct-icons">';
            if( $this->rooms ) {
                $output.= '
                    <span data-toggle="tooltip" data-placement="top" title="'.Yii::t('OfferModule.offer', 'Rooms number').'">
                        <i class="fa fa-bed"></i> ' . $this->rooms . '
                    </span>
                ';
            }
            if( $this->floor ) {
                $output.= '
                    <span data-toggle="tooltip" data-placement="top" title="'.Yii::t('OfferModule.offer', 'Floor').'">
                        <i class="fa fa-building"></i> ' . $this->floor . '
                    </span>
                ';
            }
            $output.= '</div>';
            if( $this->area ) {
                $output.= '
                    <div class="ct-text">
                        <span> ' . Yii::t('OfferModule.offer', 'Total area') . ': <span>' . $this->area . ' ' .Yii::t('OfferModule.offer', 'm<sup>2</sup>') . '</span></span>
                    </div>
                ';
            }
        }
        else {
            $output.= '<div class="ct-icons">';
            if( $this->area ) {
                $output.= '
                    <span data-toggle="tooltip" data-placement="top" title="'.Yii::t('OfferModule.offer', 'House area').'">
                        <i class="fa fa-home"></i> ' . $this->area . ' ' .Yii::t('OfferModule.offer', 'm<sup>2</sup>') . '</span></span>
                    </span>
                ';
            }
            $output.= '</div>';
            if( $this->acres ) {
                $output.= '
                    <div class="ct-text">
                        <span> ' . Yii::t('OfferModule.offer', 'Site') . ': <span>' . $this->acres . ' ' .Yii::t('OfferModule.offer', 'acres') . '</span></span>
                    </div>
                ';
            }
        }
        return $output;
    }

	/** 
	* Получение html-строки шаблона для вывода списка предложений gridView cо списками вида списка и типа сортировки
	* @param string $form_id id формы фильтра
	* @param string $listview_id id контейтера gridView
	* @param string $view_field_id свойство модели вида списка
	* @param string $sort_field_id свойство модели сортировки списка
	* @return string
	*/	
    public function getListViewTemplate($form_id=null,$listview_id=null,$sort_field_id=null,$view_field_id=null) {
        $output = "{summary}\n{items}\n<div class='text-center'>{pager}</div>";
        if( $form_id && $listview_id && $sort_field_id && $view_field_id && $this->frontendSearch()->totalItemCount > 0) {
            $output = '<div class="pull-left">{summary}</div>
            <div class="pull-right"><div class="form-group form-inline">'.$this->_getChangeViewInterface($form_id, $listview_id,$view_field_id).'&nbsp;&nbsp;&nbsp;&nbsp;'. $this->_getSortInterface($form_id, $listview_id,$sort_field_id) . '</div></div>
            <div class="clearfix"></div>
            {items}
            <div class="text-center">{pager}</div>
            ';
        }
        return $output;
    }

	/** 
	* Получение самого последнего времени изменения любого предложения
	* @return string
	*/		
	public function getLastOfferUpdate() {
		$row = $this->find()->updatesort();
		return $row->update_time;
	}

    public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
            $this->create_user_id = Yii::app()->hasComponent('user') ? Yii::app()->getUser()->getId() : null;
        }

        return parent::beforeSave();
    }    
	
	public function afterSave()
    {
        if ($this->isNewRecord) {
			if( count($this->export_templates) > 0 ) {
				foreach( $this->export_templates as $templateId ) {
					$templateToOfferModel = new ExportTemplateToOffer();
					$templateToOfferModel->unsetAttributes();
					$templateToOfferModel->offer_id = $this->id;
					$templateToOfferModel->export_template_id = $templateId;
					$templateToOfferModel->save();
				}
			}
        }
		else {
			$oldTemplates = [];
			$delArr = [];
			$newArr = [];
			foreach( $this->exportTemplates as $linkTemplate ) {
				$oldTemplates[] = $linkTemplate->export_template_id;
			}
			$newArr = array_diff( $this->export_templates, $oldTemplates );
			$delArr = array_diff( $oldTemplates, $this->export_templates );
			if( count( $newArr ) > 0 ) {
				foreach( $newArr as $addTemplateId ) {
					$templateToOfferModel = new ExportTemplateToOffer();
					$templateToOfferModel->unsetAttributes();
					$templateToOfferModel->offer_id = $this->id;
					$templateToOfferModel->export_template_id = $addTemplateId;
					$templateToOfferModel->save();					
				}
			}
			if( count( $delArr ) > 0 ) {
				foreach( $delArr as $delTemplateId ) {
					$templateToOfferModel = ExportTemplateToOffer::model()->findByPk([ 'export_template_id' => $delTemplateId, 'offer_id' => $this->id ]);
					$templateToOfferModel->delete();		
				}
			}
		}

        return parent::afterSave();
    }
	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Offer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
