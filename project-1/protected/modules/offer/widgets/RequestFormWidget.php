<?php
/**
 * Виджет для отрисовки формы RequestForm
 *
 * @category YupeWidgets
 * @package  yupe.modules.offer.widgets
 * @author   Sladkov Sergey <me@ssladkov.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @link     http://ssladkov.ru
 *
 **/
//Yii::import('application.modules.client.models.RequestForm');
Yii::import('application.modules.offer.OfferModule');

class RequestFormWidget extends yupe\widgets\YWidget
{
    public $view = 'calculator';
    public $model = null;
    public $userModel = null;

    public function run()
    {

        if( $this->model !== null )
            $this->render($this->view, ['model' => $this->model, 'userModel' => $this->userModel]);
    }
}
