<?php
/**
 * Виджет для отрисовки последний предложений
 *
 * @category YupeWidgets
 * @package  yupe.modules.offer.widgets
 * @author   Sladkov Sergey <me@ssladkov.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @link     http://ssladkov.ru
 *
 **/
Yii::import('application.modules.offer.models.Offer');
Yii::import('application.modules.gallery.models.Gallery');
Yii::import('application.modules.offer.OfferModule');

class LastOffersWidget extends yupe\widgets\YWidget
{
    public $view = 'lastoffers';

    public function run()
    {

        $offers = Offer::model()->cache($this->cacheTime)->active()->datesort()->with('gallery','gallery.images','offerType','objectType','district')->findAll(['limit' => 4]);

        $this->render($this->view, ['offers' => $offers]);
    }
}
