<?php

use yupe\components\Event;

/**
 * Class OfferSitemapGeneratorListener
 */
class OfferSitemapGeneratorListener
{
    /**
     * @param Event $event
     */
    public static function onGenerate(Event $event)
    {
        $generator = $event->getGenerator();
		
		$generator->addItem(
			Yii::app()->createAbsoluteUrl('/predlozheniya/'),
			strtotime(Offer::model()->lastOfferUpdate),
			SitemapHelper::FREQUENCY_WEEKLY,
			0.5
		);

        $offersProvider = new CActiveDataProvider(Offer::model()->active());

        foreach (new CDataProviderIterator($offersProvider) as $offer) {
            $generator->addItem(
                Yii::app()->createAbsoluteUrl('/predlozheniya/'. $offer->objectType->slug . '/' . $offer->alias),
                strtotime($offer->update_time),
                SitemapHelper::FREQUENCY_WEEKLY,
                0.5
            );
        }

    }
} 
