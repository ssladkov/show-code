<?php

/**
 * characteristicBackendController контроллер для предложений в панели управления
 *
 * @author Sladkov Sergey <me@ssladkov.ru>
 * @link http://ssladkov.ru
 * @copyright 2016 Sergey Sladkov
 * @package yupe.modules.offer.controllers
 * @since 0.1
 *
 */
class CharacteristicBackendController extends yupe\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Offer.CharacteristicBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Offer.CharacteristicBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Offer.CharacteristicBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Offer.CharacteristicBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Offer.CharacteristicBackend.Delete']],
            ['deny']
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'Characteristic',
                'validAttributes' => ['name', 'code', 'icon_code']
            ]
        ];
    }

    /**
     * Отображает характеристику по указанному идентификатору
     * @throws CHttpException
     * @param  integer $id Идинтификатор характеристики для отображения
     *
     * @return nothing
     **/
    public function actionView($id)
    {
        if (($model = Characteristic::model()->find($id)) !== null) {
            $this->render('view', ['model' => $model]);
        } else {
            throw new CHttpException(404, Yii::t('OfferModule.offer', 'Page was not found!'));
        }
    }

    /**
     * Создает новую модель характеристики.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return nothing
     **/
    public function actionCreate()
    {
        $model = new Characteristic();

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Characteristic') !== null) {

            $model->setAttributes(Yii::app()->getRequest()->getPost('Characteristic'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('OfferModule.offer', 'Characteristic was added!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование характеристики.
     *
     * @param  integer $id Идинтификатор характеристики для редактирования
     * @throw CHttpException
     * @return nothing
     **/
    public function actionUpdate($id)
    {
        if (($model = Characteristic::model()->findByPk($id)) === null) {
            throw new CHttpException(404, Yii::t('OfferModule.offer', 'Page was not found!'));
        }

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Characteristic') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Characteristic'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('OfferModule.offer', 'Characteristic was updated!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }

        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель характеристики из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id - идентификатор характеристики, который нужно удалить
     *
     * @return nothing
     **/
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            // поддерживаем удаление только из POST-запроса
            if (($model = Characteristic::model()->findByPk($id)) === null) {
                throw new CHttpException(404, Yii::t('OfferModule.offer', 'Page was not found!'));
            }

            $model->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('OfferModule.offer', 'Characteristic was deleted!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t(
                'OfferModule.offer',
                'Wrong request. Please don\'t repeate requests like this anymore!'
            ));
        }
    }

    /**
     * Управление предложениями.
     *
     * @return nothing
     **/
    public function actionIndex()
    {
        $model = new Characteristic('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('Characteristic') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('Characteristic'));
        }
        $this->render('index', ['model' => $model]);
    }

}
