<?php

/**
 * OfferBackendController контроллер для предложений в панели управления
 *
 * @author Sladkov Sergey <me@ssladkov.ru>
 * @link http://ssladkov.ru
 * @copyright 2016 Sergey Sladkov
 * @package yupe.modules.offer.controllers
 * @since 0.1
 *
 */
class OfferBackendController extends yupe\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Offer.OfferBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Offer.OfferBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Offer.OfferBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Offer.OfferBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Offer.OfferBackend.Delete']],
            ['deny']
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'Offer',
                'validAttributes' => ['name', 'price', 'responsible_user_id', 'client_id', 'status', 'address', 'district_id']
            ]
        ];
    }

    /**
     * Отображает предложение по указанному идентификатору
     * @throws CHttpException
     * @param  integer $id Идинтификатор предложения для отображения
     *
     * @return nothing
     **/
    public function actionView($id)
    {
        if (($model = Offer::model()->findByPk($id)) !== null) {
            $this->render('view', ['model' => $model]);
        } else {
            throw new CHttpException(404, Yii::t('OfferModule.offer', 'Page was not found!'));
        }
    }

    /**
     * Создает новую модель предложения.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return nothing
     **/
    public function actionCreate()
    {
        $model = new Offer();
        $model->status = Offer::STATUS_DRAFT;
        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Offer') !== null) {

            $model->setAttributes(Yii::app()->getRequest()->getPost('Offer'));

            $model->gallery_id = $model->gallery_id == '' ? null : $model->gallery_id;
            $model->responsible_user_id = $model->responsible_user_id == '' ? null : $model->responsible_user_id;
            $model->client_id = $model->client_id == '' ? null : $model->client_id;
            $model->city_id = $model->city_id == '' ? null : $model->city_id;
            $model->district_id = $model->district_id == '' ? null : $model->district_id;
            $model->area = $model->area == '' ? 0 : $model->area;

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('OfferModule.offer', 'Offer was added!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование предложения.
     *
     * @param  integer $id Идинтификатор предложения для редактирования
     * @throw CHttpException
     * @return nothing
     **/
    public function actionUpdate($id)
    {
        if (($model = Offer::model()->findByPk($id)) === null) {
            throw new CHttpException(404, Yii::t('OfferModule.offer', 'Page was not found!'));
        }

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Offer') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Offer'));

            $model->gallery_id = $model->gallery_id == '' ? null : $model->gallery_id;
            $model->responsible_user_id = $model->responsible_user_id == '' ? null : $model->responsible_user_id;
            $model->client_id = $model->client_id == '' ? null : $model->client_id;
            $model->city_id = $model->city_id == '' ? null : $model->city_id;
            $model->district_id = $model->district_id == '' ? null : $model->district_id;

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('OfferModule.offer', 'Offer was updated!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель предложения из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id - идентификатор прдложения, который нужно удалить
     *
     * @return nothing
     **/
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            // поддерживаем удаление только из POST-запроса
            if (($model = Offer::model()->find($id)) === null) {
                throw new CHttpException(404, Yii::t('OfferModule.offer', 'Page was not found!'));
            }

            $model->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('OfferModule.offer', 'Offer was deleted!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t(
                'OfferModule.offer',
                'Wrong request. Please don\'t repeate requests like this anymore!'
            ));
        }
    }

    /**
     * Управление предложениями.
     *
     * @return nothing
     **/
    public function actionIndex()
    {
        $model = new Offer('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('Offer') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('Offer'));
        }
        $this->render('index', ['model' => $model]);
    }

    public function actionChangeCity(){

        $selectedCity = $_POST['Offer']['city_id'];

        $data = Category::model()->getParentChildrenList($selectedCity);
        echo CHtml::tag('option', array('value'=>''),Yii::t('OfferModule.offer', '--not specified--'),true);
        if( count($data) > 0 ) {
            foreach ($data as $value => $district) {
                echo CHtml::tag
                ('option', array('value' => $value), CHtml::encode($district), true);
            }
        }

    }    
	
	public function actionChangeObjectType(){

        $selectedType = $_POST['Offer']['object_type_id'];

        $data = Category::model()->getParentChildrenList($selectedType);
        echo CHtml::tag('option', array('value'=>''),Yii::t('OfferModule.offer', '--not specified--'),true);
        if( count($data) > 0 ) {
            foreach ($data as $value => $subtype) {
                echo CHtml::tag
                ('option', array('value' => $value), CHtml::encode($subtype), true);
            }
        }

    }
}
