<?php

/**
 * OfferToCharacteristicBackendController контроллер для связи предложений и дополнительных характеристик в панели управления
 *
 * @author Sladkov Sergey <me@ssladkov.ru>
 * @link http://ssladkov.ru
 * @copyright 2016 Sergey Sladkov
 * @package yupe.modules.offer.controllers
 * @since 0.1
 *
 */
class OfferToCharacteristicBackendController extends yupe\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Offer.OfferToCharacteristicBackend.Index']],
            ['allow', 'actions' => ['create'], 'roles' => ['Offer.OfferToCharacteristicBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Offer.OfferToCharacteristicBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Offer.OfferToCharacteristicBackend.Delete']],
            ['deny']
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'OfferToCharacteristic',
                'validAttributes' => ['value']
            ]
        ];
    }


    /**
     * Создает новую модель связи предложение-характеристика.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return nothing
     **/
    public function actionCreate()
    {
        $model = new OfferToCharacteristic();

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('OfferToCharacteristic') !== null) {

            $model->setAttributes(Yii::app()->getRequest()->getPost('OfferToCharacteristic'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('OfferModule.offer', 'Characteristic to offer link was added!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование связи предложение-характеристика.
     *
     * @param  integer $offer_id Идинтификатор предложения для редактирования
     * @param  integer $characteristic_id Идинтификатор характеристики для редактирования
     * @throw CHttpException
     * @return nothing
     **/
   /* public function actionUpdate($offer_id,$characteristic_id)
    {
        $pk = compact( array( "offer_id", "characteristic_id" ) );
        $model = $this->loadModel($pk);

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('OfferToCharacteristic') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('OfferToCharacteristic'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('OfferModule.offer', 'Characteristic to offer link was updated!')
                );
                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'offer_id' => $model->offer_id,
                            'characteristic_id' => $model->characteristic_id,
                        ]
                    )
                );
            }
        }

        $this->render('update', ['model' => $model]);
    }*/

    /**
     * Удаляет модель связи предложение-характеристика из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param  integer $offer_id Идинтификатор предложения для редактирования
     * @param  integer $characteristic_id Идинтификатор характеристики для редактирования
     *
     * @return nothing
     **/
    public function actionDelete($offer_id,$characteristic_id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $pk = compact( array( "offer_id", "characteristic_id" ) );
            $this->loadModel($pk)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('OfferModule.offer', 'Characteristic to offer link was deleted!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t(
                'OfferModule.offer',
                'Wrong request. Please don\'t repeate requests like this anymore!'
            ));
        }
    }

    /**
     * Управление связями предложение-характеристика.
     *
     * @return nothing
     **/
    public function actionIndex()
    {
        $model = new OfferToCharacteristic('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('OfferToCharacteristic') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('OfferToCharacteristic'));
        }
        $this->render('index', ['model' => $model]);
    }

    public function loadModel($id)
    {
        $model = OfferToCharacteristic::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('OfferModule.offer', 'Page was not found!'));
        }

        return $model;
    }

}
