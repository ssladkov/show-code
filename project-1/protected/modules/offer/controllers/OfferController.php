<?php

/**
 * OfferController контроллер для предложений в панели управления
 *
 * @author Sladkov Sergey <me@ssladkov.ru>
 * @link http://ssladkov.ru
 * @copyright 2016 Sergey Sladkov
 * @package yupe.modules.offer.controllers
 * @since 0.1
 *
 */
class OfferController extends \yupe\components\controllers\FrontController
{

    /**
     * Отображает предложение по указанному идентификатору
     * @throws CHttpException
     * @param  integer $id Идинтификатор предложения для отображения
     *
     * @return nothing
     **/
    public function actionView($category,$alias)
    {
        if (($model = Offer::getModelByAlias($alias)) !== null && ($catModel = Category::model()->getByAlias($category)) !== null) {
            $this->breadcrumbs = array(
                Yii::t('OfferModule.offer', 'Offers') => Offer::ROOT_PATH,
                $catModel->name => Offer::ROOT_PATH.$catModel->slug,
                $model->name
            );
            $reqFormModel = null;

            if( isset($model->allCharacteristics['is-mortgage']) ) {
                $reqFormModel = new RequestForm('mortgage-calculation');
                $reqFormModel->cost = $model->price;
            }

            $reqOfferFormModel = new RequestForm('offer-form');
            $reqOfferFormModel->offer_id = $model->id;
            $formText = $reqOfferFormModel->text;
            $searchArr = ['{address}', '{district}', '{floor}', '{room}'];
            $replaceArr = [
                $model->address,
                ($model->district_id?$model->district->name:''),
                ($model->floor?', '.Yii::t('OfferModule.offer', 'floor').': '.$model->floor:''),
                ($model->rooms?', '.Yii::t('OfferModule.offer', 'rooms').': '.$model->rooms:''),
            ];
            $reqOfferFormModel->text = str_replace($searchArr,$replaceArr,$formText);

            $this->render('view', ['model' => $model,'category' => $catModel, 'reqFormModel' => $reqFormModel, 'reqOfferFormModel' => $reqOfferFormModel ]);
        } else {
            throw new CHttpException(404, Yii::t('OfferModule.offer', 'Page was not found!'));
        }
    }


    /**
     * Управление предложениями.
     * @param string $category
     *
     * @return nothing
     **/
    public function actionIndex($category=null)
    {
        $model = new Offer('frontend-search');

        $breadcrumbs = array( Yii::t('OfferModule.offer', 'Offers') );
        $model->unsetAttributes(); // clear any default values

        if( isset($_GET['offer_type']) && ( $offerType = $_GET['offer_type'] ) ) {
            if( $offerType == "all_but_rent" ) {
                $model->all_but_rent = Category::model()->getByAlias(Offer::OFFER_TYPE_RENT_CODE)->id;
            }
            elseif( $catModel = Category::model()->getByAlias($offerType)) {
                $model->offer_type_id = $catModel->id;
            }
        }
        if( isset($_GET['object_type']) && $_GET['object_type'] == 'land' ) {
            $model->area = 0;
        }

        if( $category ) {
            if( $catModel2 = Category::model()->getByAlias($category)) {
                $model->object_type_id = $catModel2->id;
                $breadcrumbs = array(
                    Yii::t('OfferModule.offer', 'Offers') => Offer::ROOT_PATH,
                    $catModel2->name
                );
            }
        }

        $this->breadcrumbs = $breadcrumbs;

        if (Yii::app()->getRequest()->getParam('Offer') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('Offer'));
        }


        $model->price_low_limit = empty($model->price_low_limit) ? $model->getDictValueByCode('min-price') : $model->price_low_limit;
        $model->price_upper_limit = empty($model->price_upper_limit) ? $model->getDictValueByCode('max-price') : $model->price_upper_limit;
        $model->area_low_limit = empty($model->area_low_limit) ? $model->getDictValueByCode('min-area') : $model->area_low_limit;
        $model->area_upper_limit = empty($model->area_upper_limit) ? $model->getDictValueByCode('max-area') : $model->area_upper_limit;

        $this->render('index', ['model' => $model, 'category' => $category]);
    }

    public function actionChangeCity(){
        $selectedCity = $_POST['Offer']['city_id'];
        echo CHtml::tag('option', array('value'=>''),Yii::t('OfferModule.offer', 'Any'),true);
        if( $selectedCity ) {
            $data = Category::model()->getParentChildrenList($selectedCity);
            if( count($data) > 0 ) {
                foreach ($data as $value => $district) {
                    echo CHtml::tag
                    ('option', array('value' => $value), CHtml::encode($district), true);
                }
            }
        }

    }
}
