<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 12.02.2016
 * Time: 10:19
 *
 * @var $model Offer
 * @var $this OfferController
 */
$this->title = [$model->name, Yii::app()->getModule('yupe')->siteName];
//$this->description = Yii::app()->getModule('yupe')->siteDescription;
$this->description = $model->ShortDescription;
$this->keywords = Yii::app()->getModule('yupe')->siteKeyWords;

//Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/offer.owl.carousel.init.js', CClientScript::POS_BEGIN);
Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/royalslider.css');
Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/rs-universal.css');
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.royalslider.min.js', CClientScript::POS_BEGIN);
Yii::app()->getClientScript()->registerScript('gallery-offer-'.$model->id,"
    $('#offer-gallery').royalSlider({
        controlNavigation: '".(count($model->allOfferImages)>1?'thumbnails':'none')."',
        thumbs: {
          spacing: 9,
          appendSpan: true
        },
        transitionType:'fade',
        autoScaleSlider: true,
        autoScaleSliderWidth: 7,
        autoScaleSliderHeight: 4,

        loop: true,
        arrowsNav: false,
        keyboardNavEnabled: true,

    });
", CClientScript::POS_READY);
?>
<h1 id="offer_title" class="text-center"><?=$model->getFormattedTitle();?></h1>
<section>
    <div class="container">
        <div class="ct-section--products">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <!-- Gallery -->
                    <?php /*
                    <div class="ct-gallery ct-u-marginBottom30">
                        <div class="ct-owl-controls--type2" data-single="true" data-pagination="false" data-navigation="false" id="sync1">
                            <?php foreach( $model->allImages as $alt => $image ) : ?>
                            <div class="item">
                                <img src="<?=$image['url'];?>" alt="<?=$image['alt'];?>">
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <?php if( count($model->allImages) > 1 ) : ?>
                        <div class="ct-navigationThumbnail">
                            <div class="ct-owl-controls--type3" data-single="false" data-items="5" data-pagination="false" id="sync2">
                               <?php foreach( $model->allThumbs as $alt => $thumbUrl ) : ?>
                                <div class="item">
                                    <img src="<?=$thumbUrl;?>" alt="">
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div> */ ?>
                    <div id="offer-gallery" class="royalSlider rsUni ct-u-marginBottom30" style="width:100%">
                        <?php foreach( $model->allOfferImages as $key => $image ) : ?>
                            <a class="rsImg" <?=($key==0?'id="first_img"':'');?> href="<?=$image['big']['url'];?>" data-rsw="<?=Offer::FULL_IMAGE_WIDTH;?>" data-rsh="<?=Offer::FULL_IMAGE_HEIGHT;?>">
                                <?php if($image['thumb']) : ?>
                                    <img width="<?=Offer::THUMB_IMAGE_WIDTH;?>" height="<?=Offer::THUMB_IMAGE_HEIGHT;?>" class="rsTmb" src="<?=$image['thumb'];?>">
                                <?php endif; ?>
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <!-- Gallery END -->


                    <div class="row">
                        <!-- Characteristics -->
                        <div class="col-md-5">
                            <div class="offer-heading ct-u-marginBottom20">
                                <h3 class="text-uppercase"><?=Yii::t('OfferModule.offer', 'Object  characteristics');?></h3>
                            </div>
                            <div class="characteristics">
                                <?php foreach( $model->allCharacteristics as $char ) :?>
                                    <div class="char-item">
                                        <div class="icon"><?=$char['icon_code'];?></div>
                                        <div class="name"><?=$char['name'];?></div>
                                        <div class="value"><?=$char['value'];?></div>
                                    </div>
                                <?php endforeach;?>
                            </div>
                            <div class="clearfix"></div>
                            <div id="share_container" class="text-center" style="margin-top:20px; margin-bottom: 20px;"></div>
                        </div>
                        <!-- Characteristics END-->

                        <div class="col-md-7">
                            <!-- Description -->
                            <div class="offer-heading ct-u-marginBottom20">
                                <h3 class="text-uppercase"><?=Yii::t('OfferModule.offer', 'Object description');?></h3>
                                <div id='offer_description' class="text-justify"><?=$model->description;?></div>
                            </div>
                            <!-- Description END -->

                            <!-- YMap -->
                            <div class="offer-heading ct-u-marginBottom20">
                                <h3 class="text-uppercase"><?=Yii::t('OfferModule.offer', 'Object map');?></h3>
                                <div id="offer-ymap-container" class="show-ymap" style="height:400px" data-address="<?=$model->mapAddress;?>" data-icon="/uploads/service/house-pin.png"></div>
                            </div>
                            <!-- YMap END -->
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3">
                    <div class="ct-js-sidebar">
                        <div class="row">
                            <div class="col-sm-6 col-md-12">
                                <?php
                                if( $reqFormModel !== null)
                                    $this->widget('application.modules.offer.widgets.RequestFormWidget', ['model' => $reqFormModel]);
                                else {
                                    echo '<div style="margin-right:7px;" class="hidden-md hidden-xs hidden-sm">';
                                    $this->widget('application.modules.contentblock.widgets.BannerCarouselWidget', array('category' => 'banners'));
                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="col-sm-6 col-md-12">
                                <?php $this->widget('application.modules.offer.widgets.RequestFormWidget', [
                                    'model' => $reqOfferFormModel,
                                    'view' => 'offer-form',
                                    'userModel' => ($model->responsible_user_id ? $model->responsibleUser : null)
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="https://yastatic.net/share2/share.js"></script>
<script>
    $(function() {
        Ya.share2('share_container', {
            content: {
                url: window.location.href,
                title: $('#offer_title').text(),
                description: '<?=$model->ShortDescription;?>',
                image: '<?=$model->imageBlockURL;?>'
            },
            theme: {
                services: 'vkontakte,facebook,twitter,odnoklassniki,moimir',
                counter: true,
                size: 'm',
                bare: false
            }
        });
    })
</script>

