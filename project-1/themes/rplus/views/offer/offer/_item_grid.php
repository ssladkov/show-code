<?php
/**
 * Отображение для gallery/_item:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $this GalleryController
 * @var $data Gallery
 **/
?>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 mt20">
    <div class="ct-itemProducts ct-u-marginBottom30 ct-hover products-grid">
        <label class="control-label object-type">
            <?=$data->objectTypeName;?>
        </label>
        <a href="<?=$data->URL;?>" target="_blank">
            <div class="ct-main-content">
                <div class="ct-imageBox">
                    <img src="<?=$data->imageBlockURL;?>" style="opacity: 1;"><i class="fa fa-eye"></i>
                </div>
                <div class="ct-main-text">
                    <div class="ct-product--tilte">
                        <?=$data->fullAddress;?>
                    </div>
                    <div class="ct-product--price">
                        <span><?=$data->formattedPrice;?></span>
                    </div>
                    <div class="ct-product--description text-justify">
                        <?=$data->shortDescription;?>
                    </div>
                </div>
            </div>
            <div class="ct-product--meta">
                <?=$data->metaIconsHTML;?>
            </div>
        </a>
    </div>
</div>
