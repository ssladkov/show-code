<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 12.02.2016
 * Time: 11:12
 */

Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/select2/select2.min.js', CClientScript::POS_BEGIN);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/bootstrap-slider.js', CClientScript::POS_BEGIN);
Yii::app()->getClientScript()->registerScript('filter-init','
        var select2 = $(".ct-js-select");
        if(select2.length > 0){
            select2.select2();
        }

        $(".slider").slider({});
        var $sliderAmount = $(".ct-sliderAmount");
        jQuery.each($sliderAmount, function(){
            var $this = $(this);
            var $slidermin = $this.find(".ct-js-slider-min");
            var $slidermax = $this.find(".ct-js-slider-max");
            var $sliderrange = $this.find(".ct-js-sliderAmount");

            $sliderrange.on("slide", function(){
                var newvalue = $sliderrange.data("slider").getValue();

                $slidermin.val(newvalue[0]);  //Add value on inputs
                $slidermax.val(newvalue[1]);

            });
        });

        $("#offers-filter").submit(function () {
            $.fn.yiiListView.update("offers-list", {
                data: $(this).serialize()
            });
            return false;
        });

        ', CClientScript::POS_READY);
?>
<div class="col-sm-6 col-md-12">
    <div class="widget">
        <div class="widget-inner">
            <?php
            $form = $this->beginWidget(
                'bootstrap.widgets.TbActiveForm',
                [
                    'id'          => 'offers-filter',
                    'action'      => Yii::app()->createUrl($this->route),
                    'htmlOptions' => ['class' => 'ct-formSearch--extended','role' => 'form'],
                ]
            );
            echo $form->hiddenField($model,'sort_order');
            echo $form->hiddenField($model,'view_type');
            ?>

                <div class="ct-form--label--type3">
                    <div class="ct-u-displayTableVertical">
                        <div class="ct-u-displayTableCell">
                            <div class="ct-input-group-btn">
                                <button class="btn btn-primary">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="ct-u-displayTableCell text-center">
                            <span class="text-uppercase"><?=Yii::t('OfferModule.offer', 'Search for properties');?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="ct-form--item ct-u-marginBottom10">
                        <?=$form->labelEx($model, 'city_id'); ?>
                        <?php echo $form->dropDownList(
                            $model,
                            'city_id',
                            Category::model()->getParentChildrenList($model->location_root_cat_id),
                            [
                                'empty' => Yii::t('OfferModule.offer', 'Any'),
                                'class' => 'ct-js-select ct-select-lg',
                                'ajax' => array(
                                    'type'   => 'POST',
                                    'url'    => $this->createUrl('/predlozheniya/operation/ChangeCity'),
                                    'update' => '#'.CHtml::activeId($model,'district_id'),
                                ),
                            ]
                        ); ?>
                    </div>
                    <div class="ct-form--item ct-u-marginBottom10">
                        <?php
                        echo $form->labelEx($model, 'district_id');
                        if( $model->city_id ) {
                            echo $form->dropDownList(
                                $model,
                                'district_id',
                                Category::model()->getParentChildrenList($model->city_id),
                                [
                                    'empty' => Yii::t('OfferModule.offer', 'Any'),
                                    'class' => 'ct-js-select ct-select-lg'
                                ]
                            );
                        }
                        else {
                            echo $form->dropDownList(
                                $model,
                                'district_id',
                                [],
                                [
                                    'empty' => Yii::t('OfferModule.offer', 'Any'),
                                    'class' => 'ct-js-select ct-select-lg'
                                ]
                            );
                        }
                        ?>
                    </div>

                    <div class="ct-form--item ct-u-marginBottom10">
                        <?=$form->labelEx($model, 'offer_type_id'); ?>
                        <?php echo $form->dropDownList(
                            $model,
                            'offer_type_id',
                            Category::model()->getParentChildrenList($model->offer_type_root_cat_id),
                            [
                                'empty' => Yii::t('OfferModule.offer', 'Any '),
                                'class' => 'ct-js-select ct-select-lg',
                            ]
                        ); ?>
                    </div>
                    <div class="ct-form--item ct-u-marginBottom10">
                        <?php
                        $disabled = [];
                        if( $category )
                            $disabled = ["disabled" => "disabled"];
                        ?>
                        <?=$form->labelEx($model, 'object_type_id'); ?>
                        <?php echo $form->dropDownList(
                            $model,
                            'object_type_id',
                            Category::model()->getParentChildrenList($model->object_type_root_cat_id),
                            array_merge([
                                'empty' => Yii::t('OfferModule.offer', 'Any'),
                                'class' => 'ct-js-select ct-select-lg',
                            ],$disabled)
                        ); ?>
                    </div>
                    <div class="ct-u-displayTableVertical ct-slider--row ct-u-marginBottom10">
                        <div class="ct-u-displayTableCell">
                            <div class="ct-form--item">
                                <?=$form->labelEx($model, 'rooms'); ?>
                                <?php echo $form->dropDownList(
                                    $model,
                                    'rooms',
                                    $model->filterRoomsList,
                                    [
                                        'empty' => Yii::t('OfferModule.offer', 'Any  '),
                                        'class' => 'ct-js-select ct-select-lg',
                                        'style' => 'min-width:70px;'
                                    ]
                                ); ?>
                            </div>
                        </div>
                        <div class="ct-u-displayTableCell">
                            <div class="ct-form--item">
                                <?=$form->labelEx($model, 'floor'); ?>
                                <?php echo $form->dropDownList(
                                    $model,
                                    'floor',
                                    $model->filterFloorsList,
                                    [
                                        'empty' => Yii::t('OfferModule.offer', 'Any'),
                                        'class' => 'ct-js-select ct-select-lg',
                                    ]
                                ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="ct-form--item ct-sliderAmount">
                        <label><?=Yii::t('OfferModule.offer', 'Price (rub.)');?></label>
                        <input type="text"
                           class="slider ct-js-sliderAmount"
                           value="" data-slider-tooltip="hide"
                           data-slider-handle="square"
                           data-slider-min="<?=$model->getDictValueByCode('min-price');?>"
                           data-slider-max="<?=$model->getDictValueByCode('max-price');?>"
                           data-slider-step="<?=$model->getDictValueByCode('price-step');?>"
                           data-slider-value="[<?=($model->price_low_limit.','.$model->price_upper_limit);?>]"
                        >
                        <div class="ct-u-displayTableVertical ct-slider--row ct-u-marginBottom10">
                            <div class="ct-u-displayTableCell">
                                <div class="ct-form--item">
                                    <?=$form->labelEx($model, 'price_low_limit'); ?>
                                    <?=$form->textField(
                                        $model,
                                        'price_low_limit',
                                        [
                                            'class' => 'form-control input-lg ct-js-slider-min'
                                        ]
                                    );?>
                                </div>
                            </div>
                            <div class="ct-u-displayTableCell">
                                <div class="ct-form--item">
                                    <?=$form->labelEx($model, 'price_upper_limit', ['class' => 'pull-right']); ?>
                                    <?=$form->textField(
                                        $model,
                                        'price_upper_limit',
                                        [
                                            'class' => 'form-control input-lg ct-js-slider-max'
                                        ]
                                    );?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ct-form--item ct-sliderAmount">
                        <label><?=Yii::t('OfferModule.offer', 'Area (m<sup>2</sup>)');?></label>
                        <input type="text"
                           class="slider ct-js-sliderAmount"
                           value="" data-slider-tooltip="hide"
                           data-slider-handle="square"
                           data-slider-min="<?=$model->getDictValueByCode('min-area');?>"
                           data-slider-max="<?=$model->getDictValueByCode('max-area');?>"
                           data-slider-step="<?=$model->getDictValueByCode('area-step');?>"
                           data-slider-value="[<?=($model->area_low_limit.','.$model->area_upper_limit);?>]"
                        >
                        <div class="ct-u-displayTableVertical ct-slider--row ct-u-marginBottom10">
                            <div class="ct-u-displayTableCell">
                                <div class="ct-form--item">
                                    <?=$form->labelEx($model, 'area_low_limit'); ?>
                                    <?=$form->textField(
                                        $model,
                                        'area_low_limit',
                                        [
                                            'class' => 'form-control input-lg ct-js-slider-min'
                                        ]
                                    );?>
                                </div>
                            </div>
                            <div class="ct-u-displayTableCell">
                                <div class="ct-form--item">
                                    <?=$form->labelEx($model, 'area_upper_limit', ['class' => 'pull-right']); ?>
                                    <?=$form->textField(
                                        $model,
                                        'area_upper_limit',
                                        [
                                            'class' => 'form-control input-lg ct-js-slider-max'
                                        ]
                                    );?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button type="submit" class="btn btn-success" data-loading-text="<?=Yii::t('OfferModule.offer', 'Loading...');?>"><?=Yii::t('OfferModule.offer', 'Apply filter');?></button>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>