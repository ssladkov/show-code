<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 12.02.2016
 * Time: 10:19
 *
 * @var $model Offer
 * @var $this OfferController
 */
$title = Yii::t('OfferModule.offer', 'Offers');
$this->title = [$title, Yii::app()->getModule('yupe')->siteName];
$this->description = Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = Yii::app()->getModule('yupe')->siteKeyWords;
?>
<h1 class="text-center"><?=$title;?></h1>

<div class="col-md-4 col-lg-3">
    <div class="ct-js-sidebar">
        <div class="row">
            <?php $this->renderPartial('_search', ['model' => $model, 'category' => $category]); ?>
        </div>
    </div>
</div>
<div class="col-md-8 col-lg-9">
    <?php $this->widget(
        'bootstrap.widgets.TbListView',
        [
            'id' => 'offers-list',
            'dataProvider'      => $model->frontendSearch(),
            'itemView'          => ($model->view_type==Offer::LIST_VIEW_TYPE_GRID ?'_item_grid' : '_item_list'),
            'template'          => $model->getListViewTemplate('offers-filter','offers-list','Offer_sort_order','Offer_view_type'),
            'itemsCssClass'     => ($model->view_type==Offer::LIST_VIEW_TYPE_GRID ? 'row' : 'row ct-js-search-results ct-showProducts--list'),
            'itemsTagName'      => 'div',
            'beforeAjaxUpdate'  => 'onSpinLoader',
            'afterAjaxUpdate'   => 'function(id, data) {
											hyphenateTextInContainer(".ct-product--description");
											offSpinLoader(id);
										}',
            'loadingCssClass'   => 'loading',
            'summaryText'       => Yii::t('OfferModule.offer', 'Found offers: {count}'),
            'summaryCssClass'   => 'custom',
            'emptyText'         => Yii::t('OfferModule.offer', 'No offers found. Try to change search criteria'),
            'emptyCssClass'     => 'custom'
        ]
    ); ?>
    <div class="text-center" style="margin-bottom:20px;">
        <?=Yii::t('OfferModule.offer', 'Look for a specific object? Here you can use the service legal object inspection');?>
        &nbsp;<a href="javascript:void(0)" class="btn btn-success event-show-modal" data-form-code="i-jurist-check" data-modal-id="main-modal"><?=Yii::t('OfferModule.offer', 'Check an object');?></a>
    </div>
</div>
