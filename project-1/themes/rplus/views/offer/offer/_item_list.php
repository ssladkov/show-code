<?php
/**
 * Отображение для gallery/_item:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $this GalleryController
 * @var $data Gallery
 **/
?>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
    <div class="ct-itemProducts ct-u-marginBottom30 ct-hover">
        <label class="control-label object-type" style="max-width:130px">
            <?=$data->objectTypeName;?>
        </label>
        <a href="<?=$data->URL;?>" target="_blank">
            <div class="ct-main-content col-lg-6">
                <div class="ct-imageBox">
                    <img src="<?=$data->imageListURL;?>" style="opacity: 1;"><i class="fa fa-eye"></i>
                </div>
                <div class="ct-main-text">
                    <div class="ct-product--tilte">
                        <?=$data->fullAddress;?>
                    </div>
                    <div class="ct-product--price">
                        <span><?=$data->formattedPrice;?></span>
                    </div>
                </div>
            </div>
            <div class="ct-product--meta">
                <?=$data->metaIconsHTML;?>
            </div>
            <div class="ct-product--description text-justify">
                <?=$data->shortDescription;?>
            </div>
        </a>
    </div>
</div>
