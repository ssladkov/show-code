<?php
/**
 * Отображение для виджета RequestFormWidget:
 *
 * @category YupeView
 * @package  client
 * @author   Sladkov Sergey <me@ssladkov.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://ssladkov.ru
 * @param $model RequestForm
 **/

$successMsg = Yii::app()->user->getFlash("widget-response-success");
$errorMsg = Yii::app()->user->getFlash("widget-response-error");
?>
<div class="widget">
    <div id="response-success-container" class="widget-response success mt20" style="display: none;">
        <?php if( $successMsg ) :?>
            <p><span><?=$successMsg;?></span></p>
        <?php endif;?>
    </div>
    <div id="response-error-container" class="widget-response error mt20" style="display: none;">
        <?php if( $errorMsg ) :?>
            <p><span><?=$errorMsg;?></span></p>
        <?php endif;?></div>
    <div class="widget-inner" id="widget-common-inline-form">

    <div class="user-card">

        <div class="position"><?=$model->formTitle;?></div>
        <div class="about">
            <?=$model->formTextBefore;?>
        </div>

    <?php
    $form = $this->beginWidget(
        'yupe\widgets\ActiveForm',
        [
            'id'                     => 'common-inline-form',
            'action' => Yii::app()->createUrl('addrequest/'.$model->formCode.'/?backUrl='. Yii::app()->request->requestUri ),
            'enableAjaxValidation'   => false,
            'enableClientValidation' => true,
            'clientOptions'          => array(
                'validateOnSubmit'=>true,
                'beforeValidate'=> 'js:function(form) {
                    commonSpinOn("common-inline-form");
                    return true;
                }',
                'afterValidate'=>'js:function(form,data,hasErrors) {
                    commonSpinOff("common-inline-form");
                    return (hasErrors ? false : true);
                }'
            ),
            'type'                   => 'vertical',
            'htmlOptions'            => [
                'class' => 'offer-form',
                'enctype' => 'multipart/form-data'
            ]
        ]
    );?>

        <?php
        /** создаем форму динамически  */
        foreach($model->getFields() as $fieldName => $fieldData) {
            /** если FormField.code == input || input-email */
            if( $fieldData["type"] == "input" || $fieldData["type"] == "input-email" ) {
                echo $form->textFieldGroup($model, $fieldName, [
                    'groupOptions' => [
                        'class' => 'ct-form--item'
                    ],
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName)
                        ]
                    ]
                ]);
            }
            elseif( $fieldData["type"] == "input-disabled" ) {
                echo $form->textFieldGroup($model, $fieldName, [
                    'groupOptions' => [
                        'class' => 'ct-form--item'
                    ],
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName),
                            'disabled' => 'disabled'
                        ]
                    ]
                ]);
            }
            /** если FormField.code == input-phone */
            elseif( $fieldData["type"] == "input-phone" ) {
                echo '<div class="form-group ct-form--item">';
                echo $form->labelEx($model, $fieldName, ['class' => 'control-label']);
                $this->widget(
                    'CMaskedTextField',
                    [
                        'model' => $model,
                        'attribute' => $fieldName,
                        'mask' => $fieldData["mask"],
                        'placeholder' => 'X',
                        'htmlOptions' => [
                            'class' => 'form-control'
                        ]
                    ]
                );
                echo $form->error($model,$fieldName);
                echo '</div>';
            }
            /** если FormField.code == textarea  */
            elseif( $fieldData["type"] == "textarea" ) {
                echo $form->textAreaGroup(
                    $model,
                    $fieldName, [
                        'widgetOptions' => [
                            'htmlOptions' => [
                                'rows' => 4,
                                'class' => 'ct-form--item',
                                'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName)
                            ]
                        ]
                    ]
                );
            }
            /** если FormField.code == input-hidden  */
            elseif( $fieldData["type"] == "input-hidden" ) {
                echo $form->hiddenField(
                    $model,
                    $fieldName
                );
            }
            /** если FormField.code == input-file  */
            elseif( $fieldData["type"] == "input-file" ) { ?>
            <div class="form-group">
                <label class="control-label"><?=$model->getAttributeLabel($fieldName);?></label>
                <div class="btn-group info">
                    <div class="fileform">
                        <div class="fileformlabel" id="<?=$fieldName;?>-label"></div>
                        <div class="selectbutton"><?=Yii::t('OfferModule.offer', 'Add ');?></div>
                        <input id="upload" type="file" name="<?=$fieldName;?>" onChange="writeFilename(this);"/>
                    </div>
                </div>
            </div>
           <?php }
        }
        ?>
        <div class="form-group">
            <?php
            $this->widget(
                'bootstrap.widgets.TbButton',
                [
                    'buttonType' => 'submit',
                    'context'    => 'success',
                    'label'      => $model->formSubmitBtnText,
                    'htmlOptions'=> [
                        'class' => 'btn-block'
                    ],
                ]
            ); ?>
        </div>
    <?php $this->endWidget(); ?>
    </div>
</div>
<?php if( $successMsg ) : ?>
<script>
    $(document).ready( function() {
        var widgetContainer = $("#widget-common-inline-form");
        var responseContainer = $("#response-success-container");
        responseContainer.height(widgetContainer.height());
        widgetContainer.hide();
        responseContainer.show();
    })
</script>
<?php endif; ?>