<?php
/**
 * Отображение для виджета RequestFormWidget:
 *
 * @category YupeView
 * @package  client
 * @author   Sladkov Sergey <me@ssladkov.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://ssladkov.ru
 * @param $model RequestForm
 **/
?>
<div class="widget">
    <div id="contacts-response-success-container" class="widget-response success mt20" style="display: none;">
        <p><span><?=$model->formSuccessMsg;?></span></p>
    </div>
    <div id="contacts-response-error-container" class="widget-response error mt20" style="display: none">
        <p><span><?=$model->formErrorMsg;?></span></p>
    </div>
    <div class="widget-inner" id="widget-contacts-form">

    <div class="user-card">
        <div class="position"><?=$model->formTitle;?></div>
        <?php if($model->formTextBefore) : ?>
        <div class="about">
            <?=$model->formTextBefore;?>
        </div>
        <?php endif; ?>

    <?php
    $form = $this->beginWidget(
        'yupe\widgets\ActiveForm',
        [
            'id'                     => 'contacts-form',
            'enableAjaxValidation'   => false,
            'enableClientValidation' => true,
            'clientOptions'          => array(
                'validateOnSubmit'=>true,
                'beforeValidate'=> 'js:function(form) {
                    commonSpinOn("widget-contacts-form");
                    return true;
                }',
                'afterValidate'=>'js:function(form,data,hasErrors) {
                    return requestInlineFormSubmitAjax(form, hasErrors, "'.Yii::app()->createUrl('addrequest').'/", "main-inline", "widget-contacts-form", "contacts-response-success-container");
                }'
            ),
            'type'                   => 'vertical',
            'htmlOptions'            => [
                'class' => 'offer-form no-border-top'
            ]
        ]
    );?>

        <?php
        /** создаем форму динамически  */
        foreach($model->getFields() as $fieldName => $fieldData) {
            /** если FormField.code == input || input-email */
            if( $fieldData["type"] == "input" || $fieldData["type"] == "input-email" ) {
                echo $form->textFieldGroup($model, $fieldName, [
                    'groupOptions' => [
                        'class' => 'ct-form--item'
                    ],
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName)
                        ]
                    ]
                ]);
            }
            elseif( $fieldData["type"] == "input-disabled" ) {
                echo $form->textFieldGroup($model, $fieldName, [
                    'groupOptions' => [
                        'class' => 'ct-form--item'
                    ],
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName),
                            'disabled' => 'disabled'
                        ]
                    ]
                ]);
            }
            /** если FormField.code == input-phone */
            elseif( $fieldData["type"] == "input-phone" ) {
                echo '<div class="form-group ct-form--item">';
                echo $form->labelEx($model, $fieldName, ['class' => 'control-label']);
                $this->widget(
                    'CMaskedTextField',
                    [
                        'model' => $model,
                        'attribute' => $fieldName,
                        'mask' => $fieldData["mask"],
                        'placeholder' => 'X',
                        'htmlOptions' => [
                            'class' => 'form-control'
                        ]
                    ]
                );
                echo $form->error($model,$fieldName);
                echo '</div>';
            }
            /** если FormField.code == textarea  */
            elseif( $fieldData["type"] == "textarea" ) {
                echo $form->textAreaGroup(
                    $model,
                    $fieldName, [
                        'widgetOptions' => [
                            'htmlOptions' => [
                                'rows' => 4,
                                'class' => 'ct-form--item',
                                'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName)
                            ]
                        ]
                    ]
                );
            }
            elseif( $fieldData["type"] == "input-hidden" ) {
                echo $form->hiddenField(
                    $model,
                    $fieldName
                );
            }
        }
        ?>
        <div class="form-group">
            <?php
            $this->widget(
                'bootstrap.widgets.TbButton',
                [
                    'buttonType' => 'submit',
                    'context'    => 'success',
                    'label'      => $model->formSubmitBtnText,
                    'htmlOptions'=> [
                        'class' => 'btn-block'
                    ],
                ]
            ); ?>
        </div>
    <?php $this->endWidget(); ?>
    </div>
</div>