<?php
/**
 * Отображение для виджета RequestFormWidget:
 *
 * @category YupeView
 * @package  client
 * @author   Sladkov Sergey <me@ssladkov.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://ssladkov.ru
 * @param $model RequestForm
 **/
?>
<div class="widget ct-widget--calculator">
    <div id="calculator-response-success-container" class="widget-response success calculator" style="display: none;">
        <p><span><?=$model->formSuccessMsg;?></span></p>
    </div>
    <div id="calculator-response-error-container" class="widget-response error calculator" style="display: none">
        <p><span><?=$model->formErrorMsg;?></span></p>
    </div>
    <div class="widget-inner" id="widget-calculator">
    <?php
    $form = $this->beginWidget(
        'yupe\widgets\ActiveForm',
        [
            'id'                     => 'calculator-form',
            'enableAjaxValidation'   => false,
            'enableClientValidation' => true,
            'clientOptions'          => array(
                'validateOnSubmit'=>true,
                'beforeValidate'=> 'js:function(form) {
                    commonSpinOn("widget-calculator");
                    return true;
                }',
                'afterValidate'=>'js:function(form,data,hasErrors) {
                    return requestFormCalculatorSubmitAjax(form, hasErrors, "'.Yii::app()->createUrl('addrequest/mortgage-calculation').'");
                }'
            ),
            'type'                   => 'vertical',
            'htmlOptions'            => [
                'class' => 'ct-form--darkStyle'
            ]
        ]
    );?>
        <div class="ct-form--label--type3">
            <div class="ct-u-displayTableVertical">
                <div class="ct-u-displayTableCell">
                    <div class="ct-input-group-btn">
                        <button class="btn btn-primary">
                            <i class="fa fa-calculator"></i>
                        </button>
                    </div>
                </div>
                <div class="ct-u-displayTableCell text-center">
                    <span class="text-uppercase"><?=Yii::t('OfferModule.offer', 'Calculate mortgage');?></span>
                </div>
            </div>
        </div>

        <?php
        /** создаем форму динамически  */
        foreach($model->getFields() as $fieldName => $fieldData) {
            /** если FormField.code == input || input-email */
            if( $fieldData["type"] == "input" || $fieldData["type"] == "input-email" ) {
                echo $form->textFieldGroup($model, $fieldName, [
                    'groupOptions' => [
                        'class' => 'ct-form--item'
                    ],
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName)
                        ]
                    ]
                ]);
            }
            elseif( $fieldData["type"] == "input-disabled" ) {
                echo $form->textFieldGroup($model, $fieldName, [
                    'groupOptions' => [
                        'class' => 'ct-form--item'
                    ],
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName),
                            'disabled' => 'disabled'
                        ]
                    ]
                ]);
            }
            /** если FormField.code == input-phone */
            elseif( $fieldData["type"] == "input-phone" ) {
                echo '<div class="form-group ct-form--item">';
                echo $form->labelEx($model, $fieldName, ['class' => 'control-label']);
                $this->widget(
                    'CMaskedTextField',
                    [
                        'model' => $model,
                        'attribute' => $fieldName,
                        'mask' => $fieldData["mask"],
                        'placeholder' => 'X',
                        'htmlOptions' => [
                            'class' => 'form-control'
                        ]
                    ]
                );
                echo $form->error($model,$fieldName);
                echo '</div>';
            }
            /** если FormField.code == textarea  */
            elseif( $fieldData["type"] == "textarea" ) {
                echo $form->textAreaGroup(
                    $model,
                    $fieldName, [
                        'widgetOptions' => [
                            'htmlOptions' => [
                                'rows' => 4,
                                'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName)
                            ]
                        ]
                    ]
                );
            }
            elseif( $fieldData["type"] == "input-hidden" ) {
                echo $form->hiddenField(
                    $model,
                    $fieldName
                );
            }
        }
        ?>
        <div class="ct-form--item">
            <?php
            $this->widget(
                'bootstrap.widgets.TbButton',
                [
                    'buttonType' => 'submit',
                    'context'    => 'success',
                    'label'      => $model->formSubmitBtnText,
                ]
            ); ?>
        </div>
    <?php $this->endWidget(); ?>
    </div>
</div>