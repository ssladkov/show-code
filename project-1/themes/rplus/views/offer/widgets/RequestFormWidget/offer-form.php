<?php
/**
 * Отображение для виджета RequestFormWidget:
 *
 * @category YupeView
 * @package  client
 * @author   Sladkov Sergey <me@ssladkov.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://ssladkov.ru
 * @param $model RequestForm
 **/
?>
<div class="widget">
    <div id="offer-response-success-container" class="widget-response success mt20" style="display: none;">
        <p><span><?=$model->formSuccessMsg;?></span></p>
    </div>
    <div id="offer-response-error-container" class="widget-response error mt20" style="display: none">
        <p><span><?=$model->formErrorMsg;?></span></p>
    </div>
    <div class="widget-inner" id="widget-offer-form">

    <div class="user-card">
        <?php if( $userModel !== null ) : ?>
            <div class="position"><?=Yii::t('OfferModule.offer', 'Responsible user');?></div>
            <?php /*<div class="photo">
                <img src="<?=$userModel->getAvatar(200);?>" alt="<?=$userModel->fullName;?>" />
            </div>*/ ?>
            <div class="fio">
                <?=$userModel->fullName;?>
            </div>
            <?php if( $userModel->phone || $userModel->mobile ) : ?>
                <div class="contacts">
                    <?php if($userModel->phone) : ?>
                        <div class="phone"><?=$userModel->phone;?></div>
                    <?php endif;?>
                    <?php if($userModel->mobile) : ?>
                        <div class="mobile"><?=$userModel->mobile;?></div>
                    <?php endif;?>
                </div>
            <?php endif; ?>
        <?php else : ?>
            <div class="position"><?=$model->formTitle;?></div>
            <div class="about">
                <?=$model->formTextBefore;?>
            </div>
        <?php endif; ?>

    <?php
    $form = $this->beginWidget(
        'yupe\widgets\ActiveForm',
        [
            'id'                     => 'offer-form',
            'enableAjaxValidation'   => false,
            'enableClientValidation' => true,
            'clientOptions'          => array(
                'validateOnSubmit'=>true,
                'beforeValidate'=> 'js:function(form) {
                    commonSpinOn("widget-offer-form");
                    return true;
                }',
                'afterValidate'=>'js:function(form,data,hasErrors) {
                    return requestOfferFormSubmitAjax(form, hasErrors, "'.Yii::app()->createUrl('addrequest/offer-form').'");
                }'
            ),
            'type'                   => 'vertical',
            'htmlOptions'            => [
                'class' => 'offer-form'
            ]
        ]
    );?>

        <?php
        /** создаем форму динамически  */
        foreach($model->getFields() as $fieldName => $fieldData) {
            /** если FormField.code == input || input-email */
            if( $fieldData["type"] == "input" || $fieldData["type"] == "input-email" ) {
                echo $form->textFieldGroup($model, $fieldName, [
                    'groupOptions' => [
                        'class' => 'ct-form--item'
                    ],
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName)
                        ]
                    ]
                ]);
            }
            elseif( $fieldData["type"] == "input-disabled" ) {
                echo $form->textFieldGroup($model, $fieldName, [
                    'groupOptions' => [
                        'class' => 'ct-form--item'
                    ],
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName),
                            'disabled' => 'disabled'
                        ]
                    ]
                ]);
            }
            /** если FormField.code == input-phone */
            elseif( $fieldData["type"] == "input-phone" ) {
                echo '<div class="form-group ct-form--item">';
                echo $form->labelEx($model, $fieldName, ['class' => 'control-label']);
                $this->widget(
                    'CMaskedTextField',
                    [
                        'model' => $model,
                        'attribute' => $fieldName,
                        'mask' => $fieldData["mask"],
                        'placeholder' => 'X',
                        'htmlOptions' => [
                            'class' => 'form-control'
                        ]
                    ]
                );
                echo $form->error($model,$fieldName);
                echo '</div>';
            }
            /** если FormField.code == textarea  */
            elseif( $fieldData["type"] == "textarea" ) {
                echo $form->textAreaGroup(
                    $model,
                    $fieldName, [
                        'widgetOptions' => [
                            'htmlOptions' => [
                                'rows' => 4,
                                'class' => 'ct-form--item',
                                'placeholder' => $fieldData["placeholder"] ? $fieldData["placeholder"] : $model->getAttributeLabel($fieldName)
                            ]
                        ]
                    ]
                );
            }
            elseif( $fieldData["type"] == "input-hidden" ) {
                echo $form->hiddenField(
                    $model,
                    $fieldName
                );
            }
        }
        ?>
        <div class="form-group">
            <?php
            $this->widget(
                'bootstrap.widgets.TbButton',
                [
                    'buttonType' => 'submit',
                    'context'    => 'success',
                    'label'      => $model->formSubmitBtnText,
                ]
            ); ?>
        </div>
    <?php $this->endWidget(); ?>
    </div>
</div>