<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 07.02.2016
 * Time: 17:38
 * @param Offer $offers
 */

?>
<div class="header-containter-with-line">
    <h2 class="pull-left"><?=Yii::t('OfferModule.offer', 'New realty offers');?></h2>
    <div class="pull-right hidden-xs">
        <a href="/predlozheniya/" class="btn btn-success"><?=Yii::t('OfferModule.offer', 'See all offers');?></a>
    </div>
</div>
<div class="row last-offers-widget">
<?php foreach( $offers as $offer ) : ?>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 mt20">
        <div class="ct-itemProducts ct-u-marginBottom30 ct-hover">
            <label class="control-label object-type">
                <?=$offer->objectTypeName;?>
            </label>
            <?php /*if( $offer->offer_type_id && $offer_type = $offer->offerType ) : ?>
                <label class="control-label offer-type">
                    <?=$offer_type->name;?>
                </label>
            <?php endif; */ ?>
            <a href="<?=$offer->URL;?>">
                <div class="ct-main-content">
                    <div class="ct-imageBox">
                        <img src="<?=$offer->imageBlockURL;?>" style="opacity: 1;"><i class="fa fa-eye"></i>
                    </div>
                    <div class="ct-main-text">
                        <div class="ct-product--tilte">
                            <?=$offer->fullAddress;?>
                        </div>
                        <div class="ct-product--price">
                            <span><?=$offer->formattedPrice;?></span>
                        </div>
                        <div class="ct-product--description text-justify">
                            <?=$offer->shortDescription;?>
                        </div>
                    </div>
                </div>
                <div class="ct-product--meta">
                    <?=$offer->metaIconsHTML;?>
                </div>
            </a>
        </div>
    </div>
<?php endforeach; ?>
</div>
<style>
    .ct-itemProducts .control-label.offer-type {
        background-color: #d9534f;
        top: 50px;
    }
    .ct-itemProducts .control-label.offer-type:before {
        border-color: transparent #d9534f transparent transparent;
    }
</style>
